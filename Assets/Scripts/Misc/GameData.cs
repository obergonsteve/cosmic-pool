using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "GameData")]

// asset containing data shared by several scripts
// persists between scene changes
// reset on each build
// encapsulates data that is saved to / loaded from file

public class GameData : ScriptableObject
{
    [Header("Game Data saved to File")]
    public SaveData saveData = new();      // stats saved to file - eg. Hi-Score

    [Space]
    public List<Color> BallColours;

    public Color ErrorColour = Color.red;
    public Color UpgradeColour = Color.white;

    [Header("Balls")]
    public int StartBalls = 4;

    public float MaxBallVelocity = 20f;
    public float BallScaleTime = 0.33f;
    public float BallVanishTime = 1f;

    [Header("Ripples")]
    public float RippleLifeTime = 3.0f;

    [Header("Lives")]
    public int StartLives = 9;
    public int LivesWarning = 2;            // warn when only a few lives left

    public bool ShowMenu { get; private set; } = true;          // eg. not if replay button clicked


    // a ScriptableObject cannot be in a Scene (it's an Asset!)
    // so Start() and Update() never called...


    private void OnEnable()
    {
        // a ScriptableObject can subscribe to events!
        GameEvents.OnNewHiScore += OnNewHiScore;
        GameEvents.OnGameStart += OnGameStart;
        GameEvents.OnRestartGame += OnRestartGame;
    }

    private void OnDisable()
    {
        GameEvents.OnNewHiScore -= OnNewHiScore;
        GameEvents.OnGameStart -= OnGameStart;
        GameEvents.OnRestartGame -= OnRestartGame;
    }


    public Color GetBallColour(BallColours.BallColour colour)
    {
        return BallColours[(int)colour]; 
    }

    public Color GetRandomBallColour()
    {
        return GetBallColour(Utility.RandomEnum<BallColours.BallColour>());
    }

    private void OnNewHiScore(int score, bool flash)
    {
        if (score <= saveData.HiScore)
            return;

        saveData.HiScore = score;
        GameEvents.OnSaveGameData?.Invoke(saveData);     // hi-score is saved to file, along with other player stats
    }

    private void OnGameStart()
    {
        saveData.PlayCount++;
        ShowMenu = true;                  // assume menu shown, unless eg. replaying (see below)
    }

    private void OnRestartGame()
    {
        ShowMenu = false;                  // no need (annoying) if replaying!
    }
}