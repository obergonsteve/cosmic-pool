using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{
    public GameData gameData;       // scriptable object

    public int Score { get; private set; }              // points from upgrades
    public float LivesLeft { get; private set; }        // lost on ball 'error', gained on large ball upgrade

    // increase lives added for key 'success' according to beat timing of touch and release
    public float AccuracyLivesFactor = 6f;              // factor for added lives on key 'success' based on touch accuracy
    public float AccuracyScoreFactor = 10f;             // factor for upgrade ball score based on touch accuracy
    private float touchAccuracy = 0f;                   // cumulative average of touch/release beat timing (0-1)
    private int touchCount = 0;                         // number of touches since last life gained from bar 'success'
    private float AverageTouchAccuracy => touchCount > 0 ? touchAccuracy / touchCount : 0f;   // average of touch/release beat timing (0-1) since key 'success'

    private Projection grabbedBall;

    // game cycles through these states
    // loops from WaitingForGame back to WaitingForPlayer until GameOver
    public enum GameState
    {
        WaitingToStart,         // menu, waiting for player to start game. skipped if scene reloaded on play again
        BallSetup,              // initial balls - can't grab ball or spawn ripple
        WaitingForPlayer,       // waiting for player to grab ball or spawn ripple
        Projecting,             // while dragging ball/ripple. time limit before ball will be thrown or ripple released
        WaitingForGame,         // after throw or ripple, back to WaitingForPlayer after cooldown
        GameOver                // the end. reload scene from here
    };

    public GameState CurrentState { get; private set; }


    private void OnEnable()
    {
        GameEvents.OnGameStart += OnGameStart;
        GameEvents.OnBallsSpawning += OnBallsSpawning;

        GameEvents.OnCooldownTimerExpired += OnCooldownTimerExpired;

        GameEvents.OnGrab += OnGrab;
        GameEvents.OnThrow += OnThrow;
        GameEvents.OnRippleSpawned += OnRippleSpawned;
        GameEvents.OnRippleActivated += OnRippleActivated;

        GameEvents.OnLoseLife += OnLoseLife;
        GameEvents.OnGainLife += OnGainLife;
        GameEvents.OnAddScore += OnAddScore;

        GameEvents.OnTouchBeatTiming += OnTouchBeatTiming;
        GameEvents.OnReleaseBeatTiming += OnReleaseBeatTiming;
        GameEvents.OnProgressionSuccess += OnProgressionSuccess;
        GameEvents.OnProgressionFail += OnProgressionFail;

        GameEvents.OnRestartGame += OnRestartGame;
    }

    private void OnDisable()
    {
        GameEvents.OnGameStart -= OnGameStart;
        GameEvents.OnBallsSpawning -= OnBallsSpawning;

        GameEvents.OnCooldownTimerExpired -= OnCooldownTimerExpired;

        GameEvents.OnGrab -= OnGrab;
        GameEvents.OnThrow -= OnThrow;
        GameEvents.OnRippleSpawned -= OnRippleSpawned;
        GameEvents.OnRippleActivated -= OnRippleActivated;

        GameEvents.OnLoseLife -= OnLoseLife;
        GameEvents.OnGainLife -= OnGainLife;
        GameEvents.OnAddScore -= OnAddScore;

        GameEvents.OnTouchBeatTiming -= OnTouchBeatTiming;
        GameEvents.OnReleaseBeatTiming -= OnReleaseBeatTiming;
        GameEvents.OnProgressionSuccess -= OnProgressionSuccess;
        GameEvents.OnProgressionFail -= OnProgressionFail;

        GameEvents.OnRestartGame -= OnRestartGame;
    }

    private void Start()
    {
        LeanTween.init(3200);

        LivesLeft = gameData.StartLives;
        GameEvents.OnLivesLeftUpdated?.Invoke(LivesLeft, 0, gameData.LivesWarning); // for UI

        Score = 0;
        GameEvents.OnScoreUpdated?.Invoke(Score, 0, BallColours.BallColour.Yellow, Vector2.zero);                               // for UI

        // load hi-score etc. from file into gameData scriptable object
        GameEvents.OnLoadGameData?.Invoke(out gameData.saveData, true);         // new hi-score event for UI
    }

    private void OnGameStart()
    {
        gameData.saveData.PlayCount++;
    }

    private void SetState(GameState newState, bool force = false)
    {
        if (CurrentState == newState && !force)
            return;

        if (CurrentState == GameState.GameOver)
            return;

        var oldState = CurrentState;
        CurrentState = newState;

        GameEvents.OnGameStateChanged?.Invoke(oldState, CurrentState);
    }

    private void OnRestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }


    // lives
    private void OnLoseLife(float livesLost)
    {
        if (CurrentState == GameState.GameOver)
            return;

        LivesLeft -= livesLost;

        if (LivesLeft <= 0f)
        {
            LivesLeft = 0f;

            SetState(GameState.GameOver);
            GameEvents.OnGameOver?.Invoke(Score);

            if (Score > gameData.saveData.HiScore)
                GameEvents.OnNewHiScore?.Invoke(Score, true);               // saves data after new hi-score set
            else
                GameEvents.OnSaveGameData?.Invoke(gameData.saveData);       // saves data (eg. play count)
        }

        GameEvents.OnLivesLeftUpdated?.Invoke(LivesLeft, -livesLost, gameData.LivesWarning);
    }

    private void OnGainLife(float livesGained, bool keySuccess)
    {
        if (CurrentState == GameState.GameOver)
            return;

        float livesFactor = keySuccess ? AccuracyLivesFactor * AverageTouchAccuracy : 1f;

        LivesLeft += (livesGained * livesFactor);

        // reset for next key success
        touchAccuracy = 0f;
        touchCount = 0;

        GameEvents.OnTouchBeatAccuracy?.Invoke(AverageTouchAccuracy);

        GameEvents.OnLivesLeftUpdated?.Invoke(LivesLeft, livesGained, gameData.LivesWarning);
    }

    // score
    private void OnAddScore(int scoreAdded, BallColours.BallColour ballColour, Vector2 worldPosition)
    {
        float scoreFactor = AccuracyScoreFactor * AverageTouchAccuracy;

        int scoreToAdd = (int)(scoreAdded * scoreFactor);
        Score += scoreToAdd;
        GameEvents.OnScoreUpdated?.Invoke(Score, scoreToAdd, ballColour, worldPosition);
    }


    // beat accuracy of touch and release
    private void OnTouchBeatTiming(float touchBeatAccuracy)
    {
        // lives gained affected by cumulative average of touch and release beat timing accuracy
        touchCount++;
        touchAccuracy += touchBeatAccuracy;
        GameEvents.OnTouchBeatAccuracy?.Invoke(AverageTouchAccuracy);
    }

    private void OnReleaseBeatTiming(float touchBeatAccuracy, float releaseBeatAccuracy)
    {
        //Debug.Log($"OnReleaseBeatTiming: touchBeatAccuracy {touchBeatAccuracy} releaseBeatAccuracy {releaseBeatAccuracy}");

        // lives gained affected by cumulative average of touch and release beat timing accuracy
        touchCount++;
        touchAccuracy += releaseBeatAccuracy;
        GameEvents.OnTouchBeatAccuracy?.Invoke(AverageTouchAccuracy);
    }


    // successful chords / progressions within 'round'
    // gain a life!
    private void OnProgressionSuccess(int keyCount, int barsPerKey)
    {
        if (CurrentState == GameState.GameOver)
            return;

        GameEvents.OnGainLife?.Invoke(1, true);   // use touch beat accuracy factor
    }

    // unsuccessful chords / progressions within 'round'
    // lose a life!
    private void OnProgressionFail(int keyCount, int barsPerKey)
    {
        if (CurrentState == GameState.GameOver)
            return;

        GameEvents.OnLoseLife?.Invoke(1);
    }

    // balls / ripples
    private void OnGrab(Projection grabObject, Vector2 grabPosition)
    {
        if (grabbedBall != null)
            grabbedBall.ActivatePointLight(false);

        grabbedBall = grabObject;

        SetState(GameState.Projecting);
    }

    private void OnThrow(Grabber throwObject, Vector3 throwForce, bool catapult)
    {
        SetState(GameState.WaitingForGame);
    }

    private void OnRippleSpawned(Ripple newRipple)
    {
        SetState(GameState.Projecting);
    }

    private void OnRippleActivated()
    {
        SetState(GameState.WaitingForGame);
    }

    private void OnBallsSpawning(bool spawning, bool fromGrab)
    {
        if (!fromGrab)     // ie. initial random balls
        {
            if (!spawning)
                SetState(GameState.WaitingForPlayer);     // initial ball spawning stopped
            else
                SetState(GameState.BallSetup);
        }
    }

    private void OnCooldownTimerExpired()
    {
        SetState(GameState.WaitingForPlayer, true);
    }
}
