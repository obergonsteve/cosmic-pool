using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Environment : MonoBehaviour
{
    public Transform LeftWall;
    public Transform RightWall;
    public Transform TopWall;
    public Transform BottomWall;

    private float wallThickness = 1f;
    private float wallVisible = 0.05f;     // overlap into visible scene

    public float HudHeight = 0.8f;

    //perspective camera (3D)
    //private float wallHeight = 50f;

    //private float DistanceFromCamera => mainCam.transform.position.y - transform.position.y;

    //private float CameraHeight => 2.0f * DistanceFromCamera * Mathf.Tan(mainCam.fieldOfView * 0.5f * Mathf.Deg2Rad);
    //private float CameraWidth => CameraHeight * mainCam.aspect;

    //private float CameraTopZ => (CameraHeight / 2f) + mainCam.transform.position.z;
    //private float CameraBottomZ => -(CameraHeight / 2f) + mainCam.transform.position.z;

    //private float CameraRightX => (CameraWidth / 2f) + mainCam.transform.position.x;
    //private float CameraLeftX => -(CameraWidth / 2f) + mainCam.transform.position.x;

    // ortho camera (2D)
    private float CameraHeight => 2f * mainCam.orthographicSize;
    private float CameraWidth => CameraHeight * mainCam.aspect;

    private float CameraTopY => (CameraHeight / 2f) + mainCam.transform.position.y;
    private float CameraBottomY => -(CameraHeight / 2f) + mainCam.transform.position.y;

    private float CameraRightX => (CameraWidth / 2f) + mainCam.transform.position.x;
    private float CameraLeftX => -(CameraWidth / 2f) + mainCam.transform.position.x;

    private Camera mainCam;


    private void Start()
    {
        mainCam = Camera.main;
        FitWallsToCamera2D();
    }

    private void FitWallsToCamera2D()
    {
        var halfWall = wallThickness / 2f;

        TopWall.localScale = new Vector2(CameraWidth + wallThickness, wallThickness);
        TopWall.localPosition = new Vector2(mainCam.transform.position.x, CameraTopY + halfWall - wallVisible - HudHeight);

        BottomWall.localScale = new Vector2(CameraWidth + wallThickness, wallThickness);
        BottomWall.localPosition = new Vector2(mainCam.transform.position.x, CameraBottomY - halfWall + wallVisible);

        LeftWall.localScale = new Vector2(wallThickness, CameraHeight + wallThickness);
        LeftWall.localPosition = new Vector2(CameraLeftX - halfWall + wallVisible, mainCam.transform.position.y);

        RightWall.localScale = new Vector2(wallThickness, CameraHeight + wallThickness);
        RightWall.localPosition = new Vector2(CameraRightX + halfWall - wallVisible, mainCam.transform.position.y);

        GameEvents.OnWallsPositioned?.Invoke(CameraHeight, CameraWidth, LeftWall, RightWall, TopWall, BottomWall, wallThickness);
    }

    //private void FitWallsToCamera3D() 
    //{
    //    var halfWall = wallThickness / 2f;
    //    var wallY = wallHeight / 2f;

    //    TopWall.localScale = new Vector3(CameraWidth + wallThickness, wallHeight, wallThickness);
    //    TopWall.localPosition = new Vector3(mainCam.transform.position.x, wallY, CameraTopZ + halfWall - wallVisible);

    //    BottomWall.localScale = new Vector3(CameraWidth + wallThickness, wallHeight, wallThickness);
    //    BottomWall.localPosition = new Vector3(mainCam.transform.position.x, wallY, CameraBottomZ - halfWall + wallVisible);

    //    LeftWall.localScale = new Vector3(wallThickness, wallHeight, CameraHeight + wallThickness);
    //    LeftWall.localPosition = new Vector3(CameraLeftX - halfWall + wallVisible, wallY, mainCam.transform.position.z);

    //    RightWall.localScale = new Vector3(wallThickness, wallHeight, CameraHeight + wallThickness);
    //    RightWall.localPosition = new Vector3(CameraRightX + halfWall - wallVisible, wallY, mainCam.transform.position.z);

    //    GameEvents.OnWallsPositioned?.Invoke(CameraHeight, CameraWidth, LeftWall, RightWall, TopWall, BottomWall);
    //}
}
