using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class ShootingStarSpawner : MonoBehaviour
{
    public GameData gameData;       // SO
    public Transform StarPrefab;


    public ParticleSystem starParticles;

    private float minStarScale = 0.2f;
    private float maxStarScale = 0.5f;
    private float RandomStarScale => Random.Range(minStarScale, maxStarScale);

    private float minShootTime = 0.1f;
    private float maxShootTime = 2f;
    private float RandomShootTime => Random.Range(minShootTime, maxShootTime);

    private float minShootDelay = 0.1f;
    private float maxShootDelay = 0.5f;
    private float RandomShootDelay => Random.Range(minShootDelay, maxShootDelay);

    private float minShootInterval = 0.1f;
    private float maxShootInterval = 5f;
    private float RandomShootInterval => Random.Range(minShootInterval, maxShootInterval);

    private float minRotateAngle = -90f;
    private float maxRotateAngle = 90f;
    private float RandomRotateAngle => Random.Range(minRotateAngle, maxRotateAngle);

    private bool spawning;

    // play area
    private Vector2 leftWallPosition;
    private Vector2 rightWallPosition;
    private Vector2 topWallPosition;
    private Vector2 bottomWallPosition;

    private Vector2 RandomSpawnPosition => new Vector2(Random.Range(leftWallPosition.x, rightWallPosition.x),
                                                    Random.Range(topWallPosition.y, bottomWallPosition.y));


    private void OnEnable()
    {
        GameEvents.OnWallsPositioned += OnWallsPositioned;
    }

    private void OnDisable()
    {
        GameEvents.OnWallsPositioned -= OnWallsPositioned;
    }

    private void Start()
    {
        starParticles.Play();

        spawning = true;
        StartCoroutine(SpawnStars());
    }

    private IEnumerator SpawnStars()
    {
        while (spawning)
        {
            yield return new WaitForSeconds(RandomShootInterval);
            SpawnRandomStar();
            SetRandomStarColour();
        }
    }

    private void SpawnRandomStar()
    {
        var randomStart = RandomSpawnPosition;
        var randomEnd = RandomSpawnPosition;
        var randomScale = RandomStarScale;
        var randomDelay = RandomShootDelay;
        var randomTime = RandomShootTime;
        var randomAngle = RandomRotateAngle;

        var randomBallColour = gameData.GetRandomBallColour();

        var star = Instantiate(StarPrefab, transform);
        star.transform.position = randomStart;
        star.transform.localScale = Vector2.one * randomScale;

        var starLight = star.GetComponent<Light2D>();
        starLight.color = randomBallColour;

        // initial rotation before moving / fading
        LeanTween.rotateAround(star.gameObject, Vector3.forward, randomAngle, randomDelay)
                    .setEaseOutSine();

        // initial light intensity pulse before moving / fading
        LeanTween.value(star.gameObject, starLight.intensity, starLight.intensity * 10, randomDelay * 0.5f)
                    .setEaseInOutSine()
                    .setLoopPingPong(1)
                    .setOnUpdate((float f) => starLight.intensity = f);

        // fade colour
        LeanTween.value(star.gameObject, randomBallColour, Color.clear, randomTime)
                    .setEaseInQuart()
                    .setDelay(randomDelay)
                    .setOnUpdate((Color c) => starLight.color = c);

        // move from start to end
        LeanTween.move(star.gameObject, randomEnd, randomTime)
                    .setEaseInQuart()
                    .setDelay(randomDelay)
                    .setOnComplete(() => Destroy(star.gameObject));
    }

    // intermittently change the star particles colour
    private void SetRandomStarColour()
    {
        var starsMain = starParticles.main;
        starsMain.startColor = gameData.GetRandomBallColour();
    }

    private void OnWallsPositioned(float cameraHeight, float cameraWidth, Transform leftWall, Transform rightWall, Transform topWall, Transform bottomWall, float wallThickness)
    {
        leftWallPosition = new Vector2(leftWall.position.x + wallThickness, leftWall.position.y);
        rightWallPosition = new Vector2(rightWall.position.x - wallThickness, rightWall.position.y);
        topWallPosition = new Vector2(topWall.position.x, topWall.position.y - wallThickness);
        bottomWallPosition = new Vector2(bottomWall.position.x, bottomWall.position.y + wallThickness);

        leftWallPosition = new Vector2(leftWall.position.x + wallThickness, leftWall.position.y);
        rightWallPosition = new Vector2(rightWall.position.x - wallThickness, rightWall.position.y);
        topWallPosition = new Vector2(topWall.position.x, topWall.position.y - wallThickness);
        bottomWallPosition = new Vector2(bottomWall.position.x, bottomWall.position.y + wallThickness);
        //Debug.Log($"leftWallPosition {leftWallPosition} rightWallPosition {rightWallPosition} topWallPosition {topWallPosition} bottomWallPosition {bottomWallPosition}");

        var starShape = starParticles.shape;
        starShape.scale = new Vector3(rightWallPosition.x - leftWallPosition.x, topWallPosition.y - bottomWallPosition.y, 1f);
    }
}
