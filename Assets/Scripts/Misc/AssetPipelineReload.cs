
#if UNITY_EDITOR
using UnityEditor;

public class AssetPipelineReloadEditor : Editor
{
    [MenuItem("Window/ControlAsset/AssetPipelineReloadEditor")]
    public static void AssetPipelineReload()
    {
        AssemblyReloadEvents.afterAssemblyReload += AssemblyReloadEventsAfterAssemblyReload;

        AssetDatabase.Refresh();
        EditorApplication.UnlockReloadAssemblies();
    }

    // Block the reloading of the assets after reloading
    private static void AssemblyReloadEventsAfterAssemblyReload()
    {
        AssemblyReloadEvents.afterAssemblyReload -= AssemblyReloadEventsAfterAssemblyReload;
        EditorApplication.LockReloadAssemblies();
    }
}
#endif