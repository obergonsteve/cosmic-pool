using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameEvents
{
    // Touch Input

    public delegate void OnTapDelegate(Vector2 tapPosition, int touchCount);
    public static OnTapDelegate OnTap;

    public delegate void OnSwipeStartDelegate(Vector2 startPosition, int touchCount);    
    public static OnSwipeStartDelegate OnSwipeStart;

    public delegate void OnSwipeMoveDelegate(Vector2 movePosition, int touchCount);
    public static OnSwipeMoveDelegate OnSwipeMove;

    public delegate void OnSwipeEndDelegate(Vector2 endPosition, Vector2 direction, float speed, int touchCount);
    public static OnSwipeEndDelegate OnSwipeEnd;

    // Drag / Throw

    public delegate void OnGrabDelegate(Projection grabObject, Vector2 grabPosition);
    public static OnGrabDelegate OnGrab;

    public delegate void OnDragDelegate(Grabber dragObject);
    public static OnDragDelegate OnDrag;

    public delegate void OnThrowDelegate(Grabber throwObject, Vector3 throwForce, bool catapult);
    public static OnThrowDelegate OnThrow;

    //public delegate void OnPlayerInactiveDelegate(int inactiveBeats);
    //public static OnPlayerInactiveDelegate OnPlayerInactive;        // after x beats with no player input

    //public delegate void OnThrowHitGrabPointDelegate(BallController throwObject);
    //public static OnThrowHitGrabPointDelegate OnThrowHitGrabPoint;

    // grab / ripple spawn simulation timer

    //public delegate void OnSimTimerStartDelegate(int timeLeft);
    //public static OnSimTimerStartDelegate OnSimTimerStart;

    public delegate void OnSimTimerCountdownDelegate(int timeLeft, bool isStart);
    public static OnSimTimerCountdownDelegate OnSimTimerCountdown;

    public delegate void OnSimTimerStopDelegate(int timeLeft);
    public static OnSimTimerStopDelegate OnSimTimerStop;

    public delegate void OnSimTimerExpiredDelegate(float suspendTime);
    public static OnSimTimerExpiredDelegate OnSimTimerExpired;          // also fired if dragged ball hits another ball

    // cooldown timer

    public delegate void OnCooldownCountdownDelegate(int timeLeft, bool isStart);
    public static OnCooldownCountdownDelegate OnCooldownCountdown;

    public delegate void OnCooldownTimerExpiredDelegate();
    public static OnCooldownTimerExpiredDelegate OnCooldownTimerExpired;

    // score / lives

    public delegate void OnLoseLifeDelegate(float livesLost);
    public static OnLoseLifeDelegate OnLoseLife;

    public delegate void OnGainLifeDelegate(float livesGained, bool keySuccess);
    public static OnGainLifeDelegate OnGainLife;

    public delegate void OnLivesLeftUpdatedDelegate(float livesLeft, float added, int livesWarning);
    public static OnLivesLeftUpdatedDelegate OnLivesLeftUpdated;

    public delegate void OnAddScoreDelegate(int scoreAdded, BallColours.BallColour ballColour, Vector2 worldPosition);
    public static OnAddScoreDelegate OnAddScore;

    public delegate void OnScoreUpdatedDelegate(int score, int scoreAdded, BallColours.BallColour ballColour, Vector2 worldPosition);
    public static OnScoreUpdatedDelegate OnScoreUpdated;

    public delegate void OnNewHiScoreDelegate(int hiScore, bool flash);
    public static OnNewHiScoreDelegate OnNewHiScore;

    //public delegate void OnTotalScoreInPlayDelegate(int totalScoreInPlay, int totalBallsInPlay);
    //public static OnTotalScoreInPlayDelegate OnTotalScoreInPlay;

    // game state

    public delegate void OnGameStateChangedDelegate(GameManager.GameState fromState, GameManager.GameState gameState);
    public static OnGameStateChangedDelegate OnGameStateChanged;         

    public delegate void OnGameStartDelegate();
    public static OnGameStartDelegate OnGameStart;          // eg. from menu

    public delegate void OnGameOverDelegate(int finalScore);
    public static OnGameOverDelegate OnGameOver;            // all lives lost

    public delegate void OnRestartGameDelegate();
    public static OnRestartGameDelegate OnRestartGame;

    // Camera / Walls

    public delegate void OnWallsPositionedDelegate(float cameraHeight, float cameraWidth, Transform leftWall, Transform rightWall, Transform topWall, Transform bottomWall, float wallThickness);
    public static OnWallsPositionedDelegate OnWallsPositioned;

    // Balls

    public delegate void OnBallUpgradeDelegate(Vector2 collisionPoint, BallController ball1, BallController ball2);
    public static OnBallUpgradeDelegate OnBallUpgrade;          // same colour / size

    public delegate void OnBallErrorDelegate(BallController ball);
    public static OnBallErrorDelegate OnBallError;              // different colour

    // ball spawning

    public delegate void OnGhostBallDelegate(BallController newBall);
    public static OnGhostBallDelegate OnGhostBall; 

    public delegate void OnBallsSpawningDelegate(bool spawning, bool grabbed);
    public static OnBallsSpawningDelegate OnBallsSpawning;

    // ripples

    public delegate void OnSpawnRippleDelegate(Vector2 position);   // world point
    public static OnSpawnRippleDelegate OnSpawnRipple;              // spawn new ripple on swipe start if ball not 'grabbed'

    public delegate void OnRippleSpawnedDelegate(Ripple newRipple);
    public static OnRippleSpawnedDelegate OnRippleSpawned;          // hook new ripple up to simulator and create ghost

    public delegate void OnDragRippleDelegate(Vector2 position);    // world point
    public static OnDragRippleDelegate OnDragRipple;                // on swipe move - simulate physics (ghost)

    public delegate void OnReleaseRippleDelegate(Vector2 position);     // world point
    public static OnReleaseRippleDelegate OnReleaseRipple;              // on swipe end - activate ripple

    public delegate void OnRippleActivatedDelegate();   
    public static OnRippleActivatedDelegate OnRippleActivated;     

    public delegate void OnDeactivateRippleDelegate(Ripple ripple);
    public static OnDeactivateRippleDelegate OnDeactivateRipple; 

    // simulation

    public delegate void OnSimulatorCreatedDelegate(Simulator simulator);
    public static OnSimulatorCreatedDelegate OnSimulatorCreated;

    public delegate void OnSimulationFrameDelegate(int frameNumber); 
    public static OnSimulationFrameDelegate OnSimulationFrame;

    public delegate void OnSimulateThrowDelegate(int simulationFrames);
    public static OnSimulateThrowDelegate OnSimulateThrow;

    public delegate void OnRippleClonedDelegate(Ripple newRipple);
    public static OnRippleClonedDelegate OnRippleCloned;

    public delegate void OnSimulateRippleDelegate(int simulationFrames);
    public static OnSimulateRippleDelegate OnSimulateRipple;

    // musical notes

    //public delegate void OnChangeGameKeyDelegate(bool gameStart);
    //public static OnChangeGameKeyDelegate OnChangeGameKey;

    public delegate void OnResetBarsDelegate();
    public static OnResetBarsDelegate OnResetBars;

    public delegate void OnGameKeyChangedDelegate(Key gameKey, bool playScale, bool gameStart);
    public static OnGameKeyChangedDelegate OnGameKeyChanged;

    public delegate void OnBallColourInstrumentsChangedDelegate();
    public static OnBallColourInstrumentsChangedDelegate OnBallColourInstrumentsChanged;        // fired after OnGameKeyChanged

    public delegate void OnBassNoteChangedDelegate(Instrument instrument, Note note, AudioClip clip);
    public static OnBassNoteChangedDelegate OnBassNoteChanged;

    // chord progressions

    public delegate void OnChordPlayedDelegate(Instrument instrument, Triad chord, bool countChord, Color ballColour);
    public static OnChordPlayedDelegate OnChordPlayed;

    public delegate void OnChordCountUpdatedDelegate(int keyChordCount, int totalChordCount, int chordsPerKey);
    public static OnChordCountUpdatedDelegate OnChordCountUpdated;

    //public delegate void OnChordSuccessDelegate(int keyChordCount, int barsPerKey);
    //public static OnChordSuccessDelegate OnChordSuccess;

    public delegate void OnChordProgressionPlayedDelegate(Instrument instrument);
    public static OnChordProgressionPlayedDelegate OnChordProgressionPlayed;

    public delegate void OnProgressionCountUpdatedDelegate(int keyProgCount, int totalProgCount, int progsPerKey);
    public static OnProgressionCountUpdatedDelegate OnProgressionCountUpdated;


    public delegate void OnProgressionSuccessDelegate(int keyCount, int barsPerKey);
    public static OnProgressionSuccessDelegate OnProgressionSuccess;

    public delegate void OnProgressionFailDelegate(int keyCount, int barsPerKey);
    public static OnProgressionFailDelegate OnProgressionFail;

    // beat keeper

    public delegate void OnBeatDelegate(BeatData beatData);
    public static OnBeatDelegate OnBeat;

    public delegate void OnTouchBeatTimingDelegate(float touchBeatAccuracy);
    public static OnTouchBeatTimingDelegate OnTouchBeatTiming;

    public delegate void OnReleaseBeatTimingDelegate(float touchBeatAccuracy, float releaseBeatAccuracy);
    public static OnReleaseBeatTimingDelegate OnReleaseBeatTiming;

    public delegate void OnTouchBeatAccuracyDelegate(float averageTouchAccuracy);
    public static OnTouchBeatAccuracyDelegate OnTouchBeatAccuracy;

    // music UI

    public delegate void OnBuildKeyGridDelegate(GameData gameData, InstrumentKeys instrumentKeys, BeatData beatData, bool flash);
    public static OnBuildKeyGridDelegate OnBuildKeyGrid;

    public delegate void OnStartPlayHeadDelegate(BeatData beatData);
    public static OnStartPlayHeadDelegate OnStartPlayHead;

    // data save to / load from file

    public delegate void OnSaveGameDataDelegate(SaveData saveData);
    public static OnSaveGameDataDelegate OnSaveGameData;

    public delegate void OnLoadGameDataDelegate(out SaveData saveData, bool newHiScore);
    public static OnLoadGameDataDelegate OnLoadGameData;

    public delegate void OnResetGameDataDelegate();
    public static OnResetGameDataDelegate OnResetGameData;
}
