using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Projection))]
[RequireComponent(typeof(CircleCollider2D))]


// a circular trigger collider that pushes balls away from its centre
// can be inverted to become a 'black hole'
// expanding sprite visual effect 

public class Ripple : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer spriteRenderer;

    [SerializeField]
    private Light2D spriteLight;

    [SerializeField]
    private Color spriteStartColour;
    [SerializeField]
    private Color spriteEndColour;

    [SerializeField]
    private float rippleForce = 10f;

    [SerializeField]
    private bool isBlackHole = false;

    [SerializeField]
    private AudioClip spawnClip;

    [SerializeField]
    private AudioClip activateClip;

    //private float simulationFactor = 4f;        // mutiplied by ripple lifetime to extend simulation

    private float fadeDelay = 0.25f;

    private float ghostAlpha = 0.008f;      // barely visible
    private Color ghostSpriteColour;
    private float ghostScaleTime = 0.25f;
    private float ghostShowDelay = 0.5f;
    private bool ghostSpriteShowing;

    private AudioSource audioSource;
    private CircleCollider2D rippleTrigger;
    private Projection projection;

    private Vector2 startScale;  
    private Vector2 spriteScale;         // sprite


    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        rippleTrigger = GetComponent<CircleCollider2D>();
        projection = GetComponent<Projection>();

        startScale = transform.localScale;
        spriteScale = spriteRenderer.transform.localScale;

        ghostSpriteColour = new Color(spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, ghostAlpha);

        rippleTrigger.enabled = false;
        spriteRenderer.gameObject.SetActive(false);
        spriteLight.gameObject.SetActive(false);
    }

    // activate the trigger collider (at full scale)
    // and scale up then fade the sprite if not ghost
    public void Activate(bool isGhost, float expandTime)
    {
        transform.localScale = startScale;                      // instantly activate collider at start scale
        rippleTrigger.enabled = true;

        if (!isGhost)
        {
            // scale up and fade sprite and light, for effect
            //spriteRenderer.transform.localScale = Vector2.zero;     // to be tweened below
            //spriteRenderer.gameObject.SetActive(true);
            spriteLight.transform.localScale = Vector2.zero;     // to be tweened below
            spriteLight.gameObject.SetActive(true);

            //LeanTween.scale(spriteRenderer.gameObject, spriteScale, expandTime)
            //            .setEaseOutQuart();

            //LeanTween.value(gameObject, spriteRenderer.color, Color.clear, expandTime - fadeDelay)
            //            .setOnUpdate((Color c) => spriteRenderer.color = c)
            //            .setDelay(fadeDelay)
            //            .setEaseOutQuart();

            LeanTween.scale(spriteLight.gameObject, spriteScale, expandTime)
                        .setEaseOutQuart()
                        .setOnComplete(() => GameEvents.OnDeactivateRipple?.Invoke(this));      // clean up ghost/simulator and destroy

            LeanTween.value(gameObject, spriteLight.intensity, 0f, expandTime - fadeDelay)
                        .setOnUpdate((float f) => spriteLight.intensity = f)
                        .setDelay(fadeDelay)
                        .setEaseOutQuart();

            LeanTween.value(gameObject, spriteStartColour, spriteEndColour, expandTime - fadeDelay)
                        .setOnUpdate((Color c) => spriteLight.color = c)
                        .setDelay(fadeDelay)
                        .setEaseOutQuart();

            ActivateAudio();
        }
        else        // 'ghost' sprite
        {
            ShowGhostSprite(true);
        }
    }

    public void ShowGhostSprite(bool show)
    {
        if (ghostSpriteShowing == show)
            return;

        ghostSpriteShowing = show;

        if (!show)
        {
            spriteRenderer.gameObject.SetActive(false);
            return;
        }

        spriteRenderer.transform.localScale = Vector2.zero;
        spriteRenderer.color = ghostSpriteColour;

        spriteRenderer.gameObject.SetActive(true);

        LeanTween.scale(spriteRenderer.gameObject, spriteScale, ghostScaleTime)
                        .setEaseOutQuart()
                        .setDelay(ghostShowDelay);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (!collision.gameObject.CompareTag("Ball"))
            return;

        var hitBall = collision.gameObject.GetComponent<BallController>();
        hitBall.ActivateSpriteLight();

        Vector2 direction = hitBall.transform.position - transform.position;        // larger if further away
        float magnitude = 1f / direction.sqrMagnitude;                              // invert so magnitude gets higher as distance gets smaller
        float pushForce = isBlackHole ? -rippleForce : rippleForce;  

        hitBall.Push(magnitude * pushForce * direction.normalized, hitBall.transform.position);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (!collision.gameObject.CompareTag("Ball"))
            return;

        var hitBall = collision.gameObject.GetComponent<BallController>();
        if (hitBall != null)
        {
            hitBall.PlayHitNote();
            hitBall.ActivateMagnet(true);
        }
    }

    public void SimulatePhysics(float expandTime)
    {
        projection.SimulateRipple(expandTime); // * simulationFactor);
    }

    public void StopSimulate()
    {
        projection.StopSimulateRipple();        // hide ghost ripple sprite
    }

    private void ActivateAudio()
    {
        if (activateClip != null)
        {
            audioSource.clip = activateClip;
            audioSource.Play();
        }
    }

    public void SpawnAudio()
    {
        if (spawnClip != null)
        {
            audioSource.clip = spawnClip;
            audioSource.Play();
        }
    }
}
