using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// handles all ripple spawning

public class RippleSpawner : MonoBehaviour
{
    [SerializeField]
    private GameData gameData;

    [SerializeField]
    private Ripple ripplePrefab;
    [SerializeField]
    private ParticleSystem dragParticles;            // looping
    [SerializeField]
    private ParticleSystem spawnParticles;           // on ripple spawn

    private Ripple currentRipple;                   // set on cloned in simulator

    private GameManager.GameState currentGameState;


    private void OnEnable()
    {
        GameEvents.OnGameStateChanged += OnGameStateChanged;

        GameEvents.OnSpawnRipple += OnSpawnRipple;
        GameEvents.OnRippleCloned += OnRippleCloned;
        GameEvents.OnDragRipple += OnDragRipple;
        GameEvents.OnReleaseRipple += OnReleaseRipple;

        GameEvents.OnSimTimerExpired += OnSimTimerExpired;        // release ripple
    }

    private void OnDisable()
    {
        GameEvents.OnGameStateChanged -= OnGameStateChanged;

        GameEvents.OnSpawnRipple -= OnSpawnRipple;
        GameEvents.OnRippleCloned -= OnRippleCloned;
        GameEvents.OnDragRipple -= OnDragRipple;
        GameEvents.OnReleaseRipple -= OnReleaseRipple;

        GameEvents.OnSimTimerExpired -= OnSimTimerExpired;
    }

    private void OnGameStateChanged(GameManager.GameState fromState, GameManager.GameState gameState)
    {
        currentGameState = gameState;
    }

    // spawn a new ripple and simulate physics caused by ghost trigger collider
    private void OnSpawnRipple(Vector2 position)
    {
        if (currentGameState != GameManager.GameState.WaitingForPlayer)
            return;

        SpawnRipple(position);

        dragParticles.transform.position = position;
        dragParticles.Play();
    }

    // start simulating ripple as soon as it's been cloned by the simulator
    private void OnRippleCloned(Ripple newRipple)
    {
        if (currentGameState != GameManager.GameState.WaitingForPlayer)
            return;

        currentRipple = newRipple;
        currentRipple.SimulatePhysics(gameData.RippleLifeTime);       // simulate for ripple lifetime
    }

    // set position and simulate physics caused by ghost trigger collider
    private void OnDragRipple(Vector2 position)
    {
        if (currentGameState != GameManager.GameState.Projecting)
            return;

        if (currentRipple != null)
        {
            currentRipple.transform.position = position;
            currentRipple.SimulatePhysics(gameData.RippleLifeTime);            // simulate for ripple lifetime
        }

        dragParticles.transform.position = position; 
        spawnParticles.transform.position = position;
    }

    private void OnSimTimerExpired(float suspendTime)
    {
        if (currentGameState != GameManager.GameState.Projecting)
            return;

        if (currentRipple != null)
        {
            currentRipple.StopSimulate();      // hide ghost sprite
            ActivateRipple();                  // at existing ripple position
        }
    }       

    // activate trigger collider and expand/fade sprite for effect
    private void OnReleaseRipple(Vector2 position)
    {
        if (currentRipple != null)
        {
            currentRipple.transform.position = position;
            currentRipple.StopSimulate();       // hide ghost sprite
        }

        spawnParticles.transform.position = position;
        ActivateRipple();
    }

    private void ActivateRipple()
    {
        if (currentGameState != GameManager.GameState.Projecting)
            return;

        if (currentRipple == null)
            return;

        currentRipple.Activate(false, gameData.RippleLifeTime);     // destroyed on complete
        currentRipple = null;
    
        spawnParticles.Play();

        if (dragParticles.isPlaying)
            dragParticles.Stop();

        GameEvents.OnRippleActivated?.Invoke();
    }

    // starts at zero scale, expands on release
    private Ripple SpawnRipple(Vector2 spawnPosition)
    {
        spawnParticles.transform.position = spawnPosition;
        spawnParticles.Play();

        var newRipple = Instantiate(ripplePrefab, transform);
        newRipple.transform.position = spawnPosition;

        // hook up newRipple to simulator and create its ghost clone
        GameEvents.OnRippleSpawned?.Invoke(newRipple);

        newRipple.SpawnAudio();
        return newRipple;
    }
}

