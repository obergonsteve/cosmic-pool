using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]

// script to attach to any object that moves under physics or fires physics projectiles
// in order to plot its trajectory given its position and a force
// SimulateTrajectory() 'predicts' any number of frames forwards and renders a line accordingly,
// plotting the object's projection position at each frame
public class Projection : MonoBehaviour
{
    [SerializeField]
    private int throwSimulationFrames = 600;           // for throw (not ripple)

    public bool HasGhost => ghostProjection != null;
    public bool IsGhost => !HasGhost;

    public bool IsBall => ballController != null;
    public bool IsRipple => ripple != null;

    private Color ballColour;

    private bool gameOver = false;

    private LineRenderer projectionLine;
    private BallController ballController;
    private Ripple ripple;
    private LightController lightController;

    private SpriteRenderer spriteRenderer;

    private Simulator simulator;                   // set via event
    private Projection ghostProjection;            // looked up from simulator dictionary


    private void Awake()
    {
        projectionLine = GetComponent<LineRenderer>();       // required
        ballController = GetComponent<BallController>();
        ripple = GetComponent<Ripple>();
        lightController = GetComponent<LightController>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void OnEnable()
    {
        GameEvents.OnSimulatorCreated += OnSimulatorCreated;
        GameEvents.OnSimulateThrow += OnSimulateThrow;
        GameEvents.OnSimulateRipple += OnSimulateRipple;
        GameEvents.OnRippleActivated += OnRippleActivated;
        GameEvents.OnSimulationFrame += OnSimulationFrame;          // ghosts only

        GameEvents.OnBallUpgrade += OnBallUpgrade;
        GameEvents.OnBallError += OnBallError;                      // hide ghost projection
        GameEvents.OnGameOver += OnGameOver;                        // hide ghost projection
    }

    private void OnDisable()
    {
        GameEvents.OnSimulatorCreated -= OnSimulatorCreated;
        GameEvents.OnSimulateThrow -= OnSimulateThrow;
        GameEvents.OnSimulateRipple -= OnSimulateRipple;
        GameEvents.OnRippleActivated -= OnRippleActivated;
        GameEvents.OnSimulationFrame -= OnSimulationFrame;          // ghosts only

        GameEvents.OnBallUpgrade -= OnBallUpgrade;
        GameEvents.OnBallError -= OnBallError;
        GameEvents.OnGameOver -= OnGameOver;
    }

    // on simulation scene created, with cloned objects
    private void OnSimulatorCreated(Simulator simulator)
    {
        InitSimulator(simulator);

        var ghost = this.simulator.LookupObjectGhost(transform);        // from dictionary

        if (ghost != null)
        {
            SetGhost(ghost.GetComponent<Projection>());
        }
    }

    public void InitSimulator(Simulator simulator)
    {
        if (!IsGhost)
            return;

        this.simulator = simulator;
        projectionLine.positionCount = throwSimulationFrames;       // default - overridden for ripples based on ripple life time
    }

    public void SetGhost(Projection ghost)
    {
        ghostProjection = ghost;
    }

    // stop when throw simulation starts
    private void OnSimulateThrow(int simulationFrames)
    {
        Stop();

        // restore line colour to ball colour on each throw simulation
        projectionLine.positionCount = simulationFrames; // _throwSimulationFrames;
        ResetLineColour();  
    }

    // all projections stop and reset on ripple simulation start
    private void OnSimulateRipple(int simulationFrames)
    {
        Stop();

        SyncGhostPosition();
        HideGhost(false);

        projectionLine.positionCount = simulationFrames;

        // restore line colour to ball colour on each ripple simulation
        ResetLineColour();
    }

    private void OnRippleActivated()
    {
        SyncGhostPosition();
        HideGhost(true);
    }

    private void SyncGhostPosition()
    {
        if (!HasGhost)
            return;

        ghostProjection.transform.position = transform.position;
    }

    private void ResetLine()
    {
        for (int i = 0; i < projectionLine.positionCount; i++)
            projectionLine.SetPosition(i, Vector3.zero);
    }

    public void SetLineColour(Color colour, bool isBallColour)
    {
        if (isBallColour)           // else error / upgrade colour
            ballColour = colour;

        if (IsGhost)
        {
            //var lineColour = new Color(ballColour.r, ballColour.g, ballColour.b, 0.25f);
            projectionLine.startColor = colour;
            projectionLine.endColor = colour;
        }
    }

    private void ResetLineColour()
    {
        SetLineColour(ballColour, false); 
    }

    // called for a 'grabbed' object in main scene
    // simulates its ghost ball thrown with the given velocity
    // simulates _simulationFrames 'into the future'
    // and sets the position of a point on the line renderer for each physics frame
    public void SimulateThrow(Vector3 force)
    {
        if (!HasGhost)
            return;

        if (gameOver)
            return;

        //Debug.Log($"SimulateThrow");

        GameEvents.OnSimulateThrow?.Invoke(throwSimulationFrames);               // stop all Projection objects (real and ghost)

        ghostProjection.ProjectThrow(force);
        simulator.SimulatePhysics(throwSimulationFrames);     // fires event every simulated physics frame
    }

    // to simulate ripple until ripple is released (activated)
    // called on touch start if no ball grabbed
    // expandTime converted to physics frames for SimulatePhysics
    public void SimulateRipple(float expandTime)
    {
        if (!HasGhost)
            return;

        if (gameOver)
            return;

        //Debug.Log($"SimulateRipple");

        //SyncGhostPosition();
        //HideGhost(false);

        int simFrames = (int)(expandTime / Time.fixedDeltaTime);

        GameEvents.OnSimulateRipple?.Invoke(simFrames);               // stop all Projection objects (real and ghost)

        ghostProjection.ProjectRipple(expandTime);
        simulator.SimulatePhysics(simFrames);     // fires event every simulated physics frame
    }

    public void StopSimulateRipple()
    {
        if (!HasGhost)
            return;

        ghostProjection.StopProjectRipple();
    }

    public void ActivateGhost(bool activate)
    {
        if (HasGhost)
        {
            ghostProjection.ActivatePointLight(activate);
            ghostProjection.ActivateSpriteLight(false);     // it's a ghost!
        }
    }

    public void ActivatePointLight(bool activate)
    {
        if (lightController != null)
            lightController.ActivatePointLight(activate);
    }

    public void ActivateSpriteLight(bool activate)
    {
        if (lightController != null)
            lightController.ActivateSpriteLight(activate);
    }

    private void ProjectThrow(Vector2 force)
    {
        if (!IsGhost)
            return;

        if (ballController != null)
            ballController.Push(force, transform.position);
    }

    private void ProjectRipple(float expandTime)
    {
        if (!IsGhost)
            return;

        if (ripple != null)
            ripple.Activate(true, expandTime);
    }

    // hide ghost ripple sprite
    private void StopProjectRipple()
    {
        if (!IsGhost)
            return;

        if (ripple != null)
            ripple.ShowGhostSprite(false);
    }

    // ghost object plots its position on its projection line
    private void OnSimulationFrame(int frameNumber)
    {
        if (!IsGhost)
            return;

        if (gameOver)
            return;

        projectionLine.SetPosition(frameNumber, transform.position);
    }

    // disable projection line and ghost as soon as an upgrade occurs??
    private void OnBallUpgrade(Vector2 collisionPoint, BallController ball1, BallController ball2)
    {
        HideGhost(true);
    }

    public void Stop()
    {
        if (ballController != null)
            ballController.Stop();
    }

    private void OnGameOver(int finalScore)
    {
        gameOver = true;
        HideGhost(true);
    }

    private void OnBallError(BallController ball)
    {
        HideGhost(true);
    }

    public void SyncBallGhost(Vector2 scale, int ballSize, BallColours.BallColour colour, string name)
    {
        if (!HasGhost || !IsBall)
            return;

        ghostProjection.InitBall(scale, ballSize, colour, name);
    }

    private void InitBall(Vector2 scale, int ballSize, BallColours.BallColour colour, string name)
    {
        if (!IsGhost || !IsBall)
            return;

        ballController.transform.localScale = scale;
        ballController.BallSize = ballSize;

        ballController.SetStatus(BallController.BallStatus.InPlay);
        ballController.SetColour(colour, false, false);

        ballController.name = name + " GHOST";
    }

    public void HideGhost(bool hide)
    {
        //Debug.Log($"HideGhost {name}: hide = {hide}  HasGhost = {HasGhost}");

        if (!HasGhost)
            return;

        // ?? seem to need to reset ghost here to prevent 'flash' on grab
        //if (hide)
            SyncGhostPosition();

        HideGhostSprite(hide);
        //HideGhostLine(hide);

        if (hide)
            ghostProjection.ResetLine();

        ghostProjection.gameObject.SetActive(!hide);
        //Debug.Log($"HideGhost {name}: {hide}");
    }

    public void ActivateGhostMagnet(bool activate)
    {
        if (!IsBall || !HasGhost)
            return;

        ghostProjection.ActivateMagnet(activate);
    }

    private void ActivateMagnet(bool activate)
    {
        if (IsBall)
            ballController.ActivateMagnet(activate);
    }

    //private void HideLine(bool hide)
    //{
    //    _line.enabled = !hide;
    //}

    //private void HideGhostLine(bool hide)
    //{
    //    if (!HasGhost)
    //        return;

    //    _ghostProjection.HideLine(hide);
    //}

    private void HideSprite(bool hide)
    {
        if (spriteRenderer != null)
            spriteRenderer.enabled = !hide;
    }

    public void HideGhostSprite(bool hide)
    {
        if (!HasGhost)
            return;

        ghostProjection.HideSprite(hide);
    }
}

