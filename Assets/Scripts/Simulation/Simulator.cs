using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


// script to attach to any object that moves under physics
// in order to plot its trajectory given its position and a force.
// 'Predicts' any number of frames forwards and renders a line accordingly,
// plotting the object's projection position at each frame
public class Simulator : MonoBehaviour
{
    [SerializeField] private Transform _simulationParent;
    [SerializeField] private Transform _obstaclesParent;
    [SerializeField] private Transform _destroyablesParent;

    [SerializeField] private bool doSimulation = true;

    //[SerializeField] private Transform _grabbedPoint;                  // to indicate grabbed position
    //private Collider2D _grappedPointCollider;

    [SerializeField]
    private float _simSpriteAlpha = 0.2f;

    private bool gameOver = false;

    private Scene _simulationScene;
    private PhysicsScene2D _physicsScene;

    // dictionary of non-kinematic and ripple ghost objects in scene,
    // so their position can be kept in sync with main scene as they move under physics
    // Key is real object, Value is ghost object
    private readonly Dictionary<Transform, Transform> simulatedObjects = new Dictionary<Transform, Transform>();


    private void OnEnable()
    {
        GameEvents.OnWallsPositioned += OnWallsPositioned;
        GameEvents.OnGhostBall += OnGhostBall;              // hook up to simulator and create ghost

        GameEvents.OnRippleSpawned += OnRippleSpawned;          // hook up to simulator and create ghost
        GameEvents.OnDeactivateRipple += OnDeactivateRipple;       // remove from simulator and destroy (pool?) real and ghost objects

        //GameEvents.OnGrab += OnGrab;
        //GameEvents.OnThrow += OnThrow;

        GameEvents.OnGameOver += OnGameOver;
    }

    private void OnDisable()
    {
        GameEvents.OnWallsPositioned -= OnWallsPositioned;
        GameEvents.OnGhostBall -= OnGhostBall;

        GameEvents.OnRippleSpawned -= OnRippleSpawned;
        GameEvents.OnDeactivateRipple -= OnDeactivateRipple;

        //GameEvents.OnGrab -= OnGrab;
        //GameEvents.OnThrow -= OnThrow;

        GameEvents.OnGameOver -= OnGameOver;
    }

    private void Start()
    {
        //_grappedPointCollider = _grabbedPoint.GetComponent<Collider2D>();
    }

    private void OnWallsPositioned(float cameraHeight, float cameraWidth, Transform leftWall, Transform rightWall, Transform topWall, Transform bottomWall, float wallThickness)
    {
        if (doSimulation)
            CreatePhysicsScene();
    }

    // create a 'ghost' simulation scene to overlay the main scene and keep a reference to its physics scene
    // replicate each object in the main scene and move it to the simulation scene
    private void CreatePhysicsScene()
    {
        _simulationScene = SceneManager.CreateScene("SimulationScene", new CreateSceneParameters(LocalPhysicsMode.Physics2D));
        _physicsScene = _simulationScene.GetPhysicsScene2D();

        CreateGhostObjects(_simulationParent);          // recursive
        CreateGhostObjects(_obstaclesParent);           // recursive
        CreateGhostObjects(_destroyablesParent);        // recursive

        GameEvents.OnSimulatorCreated?.Invoke(this);
    }

    // recursive
    private void CreateGhostObjects(Transform parent)
    {
        foreach (Transform obj in parent)
        {
            if (obj.gameObject.activeSelf)
            {
                CloneGhost(obj, true);      // recursive
            }
        }
    }

    // clone realObj and add real and ghost to dictionary if not kinematic or a ripple
    private Projection CloneGhost(Transform realObj, bool recursive)
    {
        var objRenderer = realObj.GetComponent<Renderer>();

        Projection realProjection;
        Projection ghostProjection = null;

        //Debug.Log($"CloneGhost: {realObj.name}");

        if (objRenderer != null)
        {
            GameObject ghostObj = Instantiate(realObj.gameObject, realObj.position, realObj.rotation);
            InitGhost(ghostObj);

            ghostObj.name = realObj.name + " GHOST";
            ghostProjection = ghostObj.GetComponent<Projection>();

            SceneManager.MoveGameObjectToScene(ghostObj, _simulationScene);

            var ghostRb = ghostObj.GetComponent<Rigidbody2D>();
            if (! simulatedObjects.ContainsKey(realObj) && ((ghostRb != null && !ghostRb.isKinematic) || ghostObj.GetComponent<Ripple>() != null))
            {
                simulatedObjects.Add(realObj, ghostObj.transform);
            }

            // having cloned, disable real object projection
            realProjection = realObj.GetComponent<Projection>();
            if (realProjection != null)
            {
                realProjection.enabled = false;
                //GameEvents.OnGhostCloned?.Invoke(realProjection, ghostProjection);
            }
        }

        // recursive
        if (recursive)
            CreateGhostObjects(realObj);

        return ghostProjection;
    }


    // called to simulate a throw or ripple expansion
    public void SimulatePhysics(int physicsFrames)
    {
        if (doSimulation && !gameOver)
        {
            SyncGhostObjects();

            for (int i = 0; i < physicsFrames; i++)
            {
                GameEvents.OnSimulationFrame?.Invoke(i);            // all ghost Projection objects plot their lines, frame by frame
                _physicsScene.Simulate(Time.fixedDeltaTime);        // physics frame step
            }
        }
    }

    private void SyncGhostObjects()
    {
        foreach (var item in simulatedObjects)     // dictionary
        {
            // Value is ghost object, Key is real object
            item.Value.position = item.Key.position;
            item.Value.rotation = item.Key.rotation;
        }
    }

    private void InitGhost(GameObject ghostObj)
    {
        var ghostSprite = ghostObj.GetComponent<SpriteRenderer>();      // in simulation scene
        if (ghostSprite != null)
        {
            ghostSprite.color = new Color(ghostSprite.color.r, ghostSprite.color.g, ghostSprite.color.b, _simSpriteAlpha);
            ghostSprite.sortingOrder = -1;                          // behind real object
        }

        var ghostLight = ghostObj.GetComponent<LightController>();      // in simulation scene
        if (ghostLight != null)
        {
            ghostLight.InitGhostLights();
        }
    }

    public Transform LookupObjectGhost(Transform realObject)
    {
        if (simulatedObjects.ContainsKey(realObject))
            return simulatedObjects[realObject];

        return null;
    }

    //// set grap point
    //private void OnGrab(Projection grabObject, Vector2 grabPosition)
    //{
    //    //_grabbedPoint.transform.position = grabPosition;

    //    // disable grab point trigger collider until ball is 'thrown'
    //    //_grappedPointCollider.enabled = false;
    //    //_grabbedPoint.gameObject.SetActive(true);
    //}

    //private void OnThrow(Grabber flickObject, Vector3 throwForce, bool catapult)
    //{
    //    // enable grab point trigger collider until ball is 'thrown'
    //    //_grappedPointCollider.enabled = true;
    //}


    // a newly spawned ball must be hooked up to the simulator
    // and a ghost created
    private void OnGhostBall(BallController newBall)
    {
        InitProjectionGhost(newBall.transform, false);
    }

    // a newly spawned ripple must be hooked up to the simulator
    // and a ghost created
    private void OnRippleSpawned(Ripple newRipple)
    {
        InitProjectionGhost(newRipple.transform, true);

        GameEvents.OnRippleCloned?.Invoke(newRipple);
        //SyncGhostObjects();
    }

    // link new object to simulator and clone it for ghost simulation
    private void InitProjectionGhost(Transform newObject, bool isRipple)
    {
        var projection = newObject.GetComponent<Projection>();

        if (projection != null)
        {
            projection.InitSimulator(this);      // simulator and frame count

            var ghostProjection = CloneGhost(newObject, false);
            projection.SetGhost(ghostProjection);

            if (! isRipple)
                projection.HideGhost(true);     // to avoid having to sync with initial movement (force) of this ball - re-enabled on grab
        }
    }

    private void OnDeactivateRipple(Ripple ripple)
    {
        var ghostRipple = LookupObjectGhost(ripple.transform);

        if (ghostRipple != null)
        {
            simulatedObjects.Remove(ripple.transform);
            Destroy(ghostRipple.gameObject);
        }

        Destroy(ripple.gameObject);
    }

    private void OnGameOver(int finalScore)
    {
        gameOver = true;
    }
}