using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]

public class InstrumentBar : MonoBehaviour
{
    public Color InstrumentColour { get; private set; }

    private Image barImage;

    private void Awake()
    {
        barImage = GetComponent<Image>();
    }

    public void SetColour(Color colour)
    {
        barImage.color = InstrumentColour = colour;
    }

    // used by MusicUI to 'flash' bar
    // does not change instrument colour
    public void ChangeColour(Color colour)
    {
        barImage.color = colour;
    }
}
