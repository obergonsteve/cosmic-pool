using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(menuName = "InstrumentKeys")]

// contains a list of all instruments and the 12 notes of an octave for each
// also all the keys available, with the notes in each scale

// utility for getting random notes and chords from game key (set OnKeyChange) for a random instrument

public class InstrumentKeys : ScriptableObject
{
    public InstrumentGroups instrumentGroups;       // set up in Inspector (data structure)

    public InstrumentGroup instrumentGroup { get; private set; }     // random from instrumentGroups - changed on key change

    public InstrumentGroup drumInstruments;             // set up in Inspector
    public InstrumentGroup bassInstruments;             // set up in Inspector

    [Space]
    public Keys keys;                   // set up in Inspector

    private Key gameKey = null;         // set on game start for duration of each game

    // a random instrument is assigned to each colour
    private Dictionary<BallColours.BallColour, Instrument> ballColourInstruments = new();

    public List<BallColours.BallColour> BallColours { get; private set; }      // keys extracted from dictionary once built

    public static readonly int NotesInOctave = 12;

    public Instrument RandomInstrument => instrumentGroup.RandomInstrument;
    public Key RandomKey => keys.RandomKey;

    private void OnEnable()
    {
        GameEvents.OnGameStart += OnGameStart;
        //GameEvents.OnPlayerInactive += OnPlayerInactive;
    }

    private void OnDisable()
    {
        GameEvents.OnGameStart -= OnGameStart;
        //GameEvents.OnPlayerInactive -= OnPlayerInactive;
    }


    private void OnGameStart()
    {
        // pick a random key
        ChangeGameKey(true);

        // set up instrument note/audioclip dictionaries
        drumInstruments.InitInstrumentNotes();
        bassInstruments.InitInstrumentNotes();
    }

    //private void OnPlayerInactive(int inactiveBeats)
    //{
    //    ChangeGameKey(false);
    //}

    // sets a new random gameKey
    public void ChangeGameKey(bool gameStart)
    {
        instrumentGroup = instrumentGroups.RandomInstrumentGroup;

        //Debug.Log($"ChangeGameKey {instrumentGroup.instruments.Count}");
        instrumentGroup.InitInstrumentNotes();

        var currentKey = gameKey;

        do
        {
            gameKey = keys.RandomKey;       // pick a key at random...
        }
        while (gameKey == currentKey);     // ..until it's different from currentKey

        GameEvents.OnGameKeyChanged?.Invoke(gameKey, true, gameStart);

        // assign a random instrument to each ball colour whenever key is changed
        InitBallColourInstruments();
    }

    // assign a random instrument to each ball colour (on each key change)
    private void InitBallColourInstruments()
    {
        ballColourInstruments.Clear();

        foreach (BallColours.BallColour ballColour in Enum.GetValues(typeof(BallColours.BallColour)))
        {
            // assign a random instrument to each colour
            ballColourInstruments[ballColour] = RandomInstrument;

            //// making sure that it's not already used by another colour
            //Instrument randomInstrument;
            //do
            //{
            //    randomInstrument = RandomInstrument;
            //}
            //while (ballColourInstruments.ContainsValue(randomInstrument));

            //Debug.Log($"InitBallColourInstruments: {randomInstrument.instrumentName} assigned to {ballColour}");
            //ballColourInstruments[ballColour] = randomInstrument;
        }

        BallColours = new(ballColourInstruments.Keys);

        GameEvents.OnBallColourInstrumentsChanged?.Invoke();
    }

    // lookup instrument by colour
    public Instrument GetColourInstrument(BallColours.BallColour ballColour)
    {
        Instrument colourInstrument;

        if (ballColourInstruments.TryGetValue(ballColour, out colourInstrument))
        {
            return colourInstrument;        // colour assigned a random intrument
        }

        Debug.LogError($"GetColourInstrument: instrument for ball colour {ballColour} not found!!");
        return null;
    }

    //public BallColours.BallColour ballColour GetInstrumentColour(Instrument instrument)
    //{
    //    var instruments = ballColourInstruments.Values;

    //    var instrumentColour = instruments.FirstOrDefault((instr) => instrument == instr);

    //    if (ballColourInstruments.TryGetValue(ballColour, out colourInstrument))
    //    {
    //        return colourInstrument;        // colour assigned a random intrument
    //    }

    //    Debug.LogError($"GetInstrumentColour: instrument for ball colour {ballColour} not found!!");
    //    return null;
    //}

    // returns a random note from the game key
    // played by the given instrument
    public AudioClip RandomInstrumentNote(Instrument instrument, out int noteIndex, out Note note)
    {
        if (gameKey == null)
            OnGameStart();      // to make sure!

        noteIndex = gameKey.RandomKeyNoteIndex;
        note = gameKey.KeyNoteByIndex(noteIndex);
        return instrument.GetNoteClip(note);
    }

    // returns a random note from the game key
    // played by a random instrument
    public AudioClip RandomInstrumentNote(out Instrument instrument, out Note note)
    {
        if (gameKey == null)
            OnGameStart();      // to make sure!

        instrument = RandomInstrument;

        int noteIndex;
        return RandomInstrumentNote(instrument, out noteIndex, out note);
    }

    // make chord from random note in key
    public List<AudioClip> RandomInstrumentChord(Instrument instrument)
    {
        if (gameKey == null)
            OnGameStart();      // to make sure!

        List<AudioClip> audioClips = new();

        Triad randomChord = gameKey.RandomKeyTriad;

        foreach (Note note in randomChord.chordNotes)
        {
            audioClips.Add(instrument.GetNoteClip(note));
        }

        return audioClips;
    }

    // make chord from given rootNote in game key
    public Triad InstrumentChord(Instrument instrument, int rootNoteNumber)
    {
        if (gameKey == null)
            OnGameStart();      // to make sure!

        return gameKey.MakeTriad(rootNoteNumber, false);
    }

    public List<AudioClip> InstrumentChordClips(Instrument instrument, Triad chord)
    {
        if (gameKey == null)
            OnGameStart();      // to make sure!

        List<AudioClip> audioClips = new();
 
        foreach (Note note in chord.chordNotes)
        {
            audioClips.Add(instrument.GetNoteClip(note));
        }

        return audioClips;
    }

    // utility for full note names
    public static string NoteName(Note note)
    {
        switch (note)
        {
            case Note.A:
            case Note.B:
            case Note.C:
            case Note.D:
            case Note.E:
            case Note.F:
            case Note.G:
                return note.ToString();

            case Note.Bf:
            case Note.Ef:
                return note.ToString()[0] + " Flat";

            case Note.Cs:
            case Note.Fs:
            case Note.Gs:
                return note.ToString()[0] + " Sharp";

            default:
                return note.ToString();
        }

        // switch converted to expression!!!
        // more concise but not as easy to read...

        //return note switch
        //{
        //    Note.A or Note.B or Note.C or Note.D or Note.E or Note.F or Note.G => note.ToString(),
        //    Note.Bf or Note.Ef => note.ToString()[0] + " flat",
        //    Note.Cs or Note.Fs or Note.Gs => note.ToString()[0] + " sharp",
        //    _ => note.ToString(),
        //};
    }
}