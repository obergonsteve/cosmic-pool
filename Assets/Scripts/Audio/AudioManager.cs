using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(AudioSource))]     // music (looping)

public class AudioManager : MonoBehaviour
{
    [Header("Audio Sources")]
    [SerializeField]
    private AudioSource musicSource;

    [SerializeField]
    private AudioSource sfxSource;

    [Header("Audio Clips")]
    [SerializeField]
    private AudioClip gameMusic;

    [SerializeField]
    private AudioClip menuMusic;        // not yet used

    [SerializeField]
    private AudioClip gameOverMusic;

    [SerializeField]
    private AudioClip grabTimerClip;

    [SerializeField]
    private AudioClip cooldownClip;

    [SerializeField]
    private AudioClip playersTurnClip;

    [SerializeField]
    private AudioClip grabTimerExpiredClip;

    [SerializeField]
    private AudioClip newHiScoreClip;


    private void OnEnable()
    {
        GameEvents.OnGameStart += OnGameStart;
        GameEvents.OnGameStateChanged += OnGameStateChanged;

        GameEvents.OnSimTimerCountdown += OnSimTimerCountdown;
        GameEvents.OnSimTimerExpired += OnSimTimerExpired;

        GameEvents.OnCooldownCountdown += OnCooldownCountdown;

        GameEvents.OnNewHiScore += OnNewHiScore;
    }

    private void OnDisable()
    {
        GameEvents.OnGameStart -= OnGameStart;
        GameEvents.OnGameStateChanged -= OnGameStateChanged;

        GameEvents.OnSimTimerCountdown -= OnSimTimerCountdown;
        GameEvents.OnSimTimerExpired -= OnSimTimerExpired;

        GameEvents.OnCooldownCountdown -= OnCooldownCountdown;

        GameEvents.OnNewHiScore -= OnNewHiScore;
    }


    private void OnGameStart()
    {
        if (musicSource.enabled)
        {
            musicSource.loop = true;
            musicSource.clip = gameMusic;
            musicSource.Play();
        }
    }


    private void OnGameStateChanged(GameManager.GameState fromState, GameManager.GameState gameState)
    {
        switch (gameState)
        {
            case GameManager.GameState.WaitingToStart:
                break;
            case GameManager.GameState.BallSetup:
                break;

            case GameManager.GameState.WaitingForPlayer:
                PlayersTurn();
                break;

            case GameManager.GameState.Projecting:
                break;
            case GameManager.GameState.WaitingForGame:
                break;

            case GameManager.GameState.GameOver:
                GameOver();
                break;
        }
    }

    private void OnSimTimerCountdown(int timeLeft, bool isStart)
    {
        if (timeLeft > 0)
            GrabTimerBeep();
    }

    private void OnCooldownCountdown(int timeLeft, bool isStart)
    {
        if (timeLeft > 0)
            CooldownTimerBeep();
    }

    private void GameOver()
    {
        sfxSource.clip = gameOverMusic;
        sfxSource.Play();
    }

    private void OnNewHiScore(int hiScore, bool flash)
    {
        if (flash)
        {
            sfxSource.clip = newHiScoreClip;
            sfxSource.Play();
        }
    }

    private void OnSimTimerExpired(float suspendTime)
    {
        sfxSource.clip = grabTimerExpiredClip;
        sfxSource.Play();
    }

    private void GrabTimerBeep()
    {
        sfxSource.clip = grabTimerClip;
        sfxSource.Play();
    }

    private void PlayersTurn()
    {
        sfxSource.clip = playersTurnClip;
        sfxSource.Play();
    }

    private void CooldownTimerBeep()
    {
        sfxSource.clip = cooldownClip;
        sfxSource.Play();
    }
}
