using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// fires OnBeat event every beat with BeatData

public class BeatKeeper : MonoBehaviour
{
    public bool KeepBeat = true;
    private bool beatPumping;

    public InstrumentKeys instrumentKeys;           // scriptable object, all instruments and keys/scales
    public GameData gameData;                       // scriptable object, ball colours, save data, etc

    [SerializeField]
    private BeatData beatData;

    [Header("Drums")]
    [SerializeField] private AudioSource drumSource;
    private AudioClip drumClip;

    [Header("Bass")]
    [SerializeField] private AudioSource bassSource;
    private AudioClip bassClip;         // set according to key

    [Header("Click track")]
    [SerializeField] private AudioSource clickSource;

    private string drumInstrumentName = "Drums 1";
    private string bassInstrumentName = "Electric Bass";

    private Instrument drumInstrument;      // set on key change?
    private Instrument bassInstrument;      // set on key change?

    private bool touching;        // true while touching

    // beat / touch timestamps
    private DateTime lastBeatTime = DateTime.Now;

    private TimeSpan touchTimeSinceLastBeat; // => lastTouchTime - lastBeatTime;
    private float touchAccuracy;
    private TimeSpan releaseTimeSinceLastBeat; // => lastReleaseTime - lastBeatTime;
    private float releaseAccuracy;

    private float halfBeatInterval => beatData.BeatInterval * 0.5f;

    private int targetChordsPerBar = 2;
    private int targetProgsPerBar = 1;
    private bool progressionSuccess;                // hit chord / progression target (ie. bars per key)

    public bool CountChords = true;                 // to extend bars. if false, count chord progressions (3+ chords) instead

    public int TotalChordCount { get; private set; }            // chords played during the game
    public int KeyChordCount { get; private set; }              // chords played during the current 'round'

    public int TotalProgressionCount { get; private set; }      // chord progressions (3+ chords) played during the game
    public int KeyProgressionCount { get; private set; }        // chord progressions played during the current 'round' 

    private GameManager.GameState currentGameState;

    //[SerializeField]
    //private int inactiveBeatTimeout = 16;      // number of beats with no player activity
    //private int inactiveBeatsLeft = 0;
    //private bool inactiveTimeoutPaused = false;

    private Coroutine beatCoroutine;


    private void OnEnable()
    {
        GameEvents.OnGameStart += OnGameStart;
        GameEvents.OnGameStateChanged += OnGameStateChanged;

        GameEvents.OnChordPlayed += OnChordPlayed;
        GameEvents.OnChordProgressionPlayed += OnChordProgressionPlayed;

        GameEvents.OnBallColourInstrumentsChanged += OnBallColourInstrumentsChanged;      // fired after OnGameKeyChanged

        GameEvents.OnSwipeStart += OnSwipeStart;
        GameEvents.OnSwipeEnd += OnSwipeEnd;

        //GameEvents.OnGrab += OnGrab;
        //GameEvents.OnRippleSpawned += OnRippleSpawned;
        //GameEvents.OnThrow += OnThrow;
        //GameEvents.OnRippleActivated += OnRippleActivated;
    }

    private void OnDisable()
    {
        GameEvents.OnGameStart -= OnGameStart;
        GameEvents.OnGameStateChanged -= OnGameStateChanged;

        GameEvents.OnChordPlayed -= OnChordPlayed;
        GameEvents.OnChordProgressionPlayed -= OnChordProgressionPlayed;

        GameEvents.OnBallColourInstrumentsChanged -= OnBallColourInstrumentsChanged;

        GameEvents.OnSwipeStart -= OnSwipeStart;
        GameEvents.OnSwipeEnd -= OnSwipeEnd;

        //GameEvents.OnGrab -= OnGrab;
        //GameEvents.OnRippleSpawned -= OnRippleSpawned;
        //GameEvents.OnThrow -= OnThrow;
        //GameEvents.OnRippleActivated -= OnRippleActivated;
    }

    private void OnGameStart()
    {
        if (! KeepBeat)
            return;

        ResetBars(true, false);

        KeyChordCount = 0;
        TotalChordCount = 0;
        if (CountChords)
            GameEvents.OnChordCountUpdated?.Invoke(KeyChordCount, TotalChordCount, beatData.BarsPerKey * targetChordsPerBar);

        KeyProgressionCount = 0;
        TotalProgressionCount = 0;
        if (! CountChords)
            GameEvents.OnProgressionCountUpdated?.Invoke(KeyProgressionCount, TotalProgressionCount, beatData.BarsPerKey * targetProgsPerBar);

        drumSource.clip = drumClip;
        bassSource.clip = bassClip;

        //ResetInactiveBeatCount();
        beatCoroutine = StartCoroutine(StartBeat());
    }

    private void OnSwipeStart(Vector2 startPosition, int touchCount)
    {
        if (currentGameState != GameManager.GameState.WaitingForPlayer)
            return;

        touching = true;
        touchTimeSinceLastBeat = DateTime.Now - lastBeatTime;
        touchAccuracy = BeatAccuracy((float)touchTimeSinceLastBeat.TotalSeconds);

        //Debug.Log($"OnSwipeStart: touchAccuracy {touchAccuracy}");
        GameEvents.OnTouchBeatTiming?.Invoke(touchAccuracy);
    }

    private void OnSwipeEnd(Vector2 endPosition, Vector2 direction, float speed, int touchCount)
    {
        //if (currentGameState != GameManager.GameState.Projecting)
        //    return;

        if (!touching)
            return;

        releaseTimeSinceLastBeat = DateTime.Now - lastBeatTime;
        releaseAccuracy = BeatAccuracy((float)releaseTimeSinceLastBeat.TotalSeconds);
        touching = false;

        GameEvents.OnReleaseBeatTiming?.Invoke(touchAccuracy, releaseAccuracy);
    }

    private void OnGameStateChanged(GameManager.GameState fromState, GameManager.GameState gameState)
    {
        currentGameState = gameState;

        switch (gameState)
        {
            case GameManager.GameState.WaitingToStart:
                break;
            case GameManager.GameState.BallSetup:
                break;
            case GameManager.GameState.WaitingForPlayer:
                break;
            case GameManager.GameState.Projecting:
                break;
            case GameManager.GameState.WaitingForGame:
                break;
            case GameManager.GameState.GameOver:
                StopBeat();
                break;
        }
    }

    private IEnumerator StartBeat()
    {
        if (beatPumping)
            yield break;

        beatPumping = true;
        beatData.Reset(true);

        while (beatPumping)
        {
            PlayBeat(beatData);
            yield return new WaitForSecondsRealtime(beatData.BeatInterval);
        }

        yield return null;
    }

    private void StopBeat()
    {
        if (beatCoroutine != null)
            StopCoroutine(beatCoroutine);

        beatCoroutine = null;
        beatPumping = false;
    }

    // play drums / bass and change bass note on the right beats
    // timing here is critical!
    private void PlayBeat(BeatData beatData)
    {
        clickSource.Play();     // 'click track'

        lastBeatTime = DateTime.Now;

        if (beatData.IsLastBeat)
            ResetBars(false, true);     // beat count reset to 0

        if (beatData.IsDrumBeat)
            drumSource.Play();

        if (beatData.IsBassBeat)
        {
            if (beatData.IsBassChangeBeat)
                SetRandomBassNote(bassInstrument);

            bassSource.Play();
        }

        beatData.CountBeat();       // so first BeatCount == 1
        GameEvents.OnBeat?.Invoke(beatData);
    }


    // returns beat timing accuracy of touch (0-1)
    // by determing if touch timeSinceLastBeat was closer to last or next beat
    // 1 == perfect timing (exactly on beat)
    // 0 == exactly mid-beat (worst timing)
    private float BeatAccuracy(float timeSinceLastBeat)
    {
        if (timeSinceLastBeat <= halfBeatInterval)       // under half-way - closer to last beat
        {
            return 1f - (timeSinceLastBeat / halfBeatInterval);          // 1 == max accuracy, 0 == mid-beat
        }
        else      // over half-way - closer to next beat
        {
            float timeToNextBeat = beatData.BeatInterval - timeSinceLastBeat;
            return 1f - (timeToNextBeat / halfBeatInterval);           // 1 == max accuracy, 0 == mid-beat
        }
    }

    private void OnChordPlayed(Instrument instrument, Triad chord, bool countChord, Color ballColour)
    {
        if (!countChord)
            return;

        KeyChordCount++;
        TotalChordCount++;

        if (CountChords)        // as opposed to progressions
            GameEvents.OnChordCountUpdated?.Invoke(KeyChordCount, TotalChordCount, beatData.BarsPerKey * targetChordsPerBar);
    }

    private void OnChordProgressionPlayed(Instrument instrument)
    {
        KeyProgressionCount++;
        TotalProgressionCount++;

        if (! CountChords)
            GameEvents.OnProgressionCountUpdated?.Invoke(KeyProgressionCount, TotalProgressionCount, beatData.BarsPerKey * targetProgsPerBar);
    }

    // instrument colours have been set, so UI key bar grid can be built
    // fired after OnGameKeyChanged
    private void OnBallColourInstrumentsChanged()
    {
        GameEvents.OnBuildKeyGrid?.Invoke(gameData, instrumentKeys, beatData, progressionSuccess);
        progressionSuccess = false;
    }

    // reset counters and set a random drum and bass note from the game key
    private void ResetBars(bool changeKey, bool checkForSuccess)
    {
        if (changeKey)
            instrumentKeys.ChangeGameKey(true);

        // reset beat count for new key
        beatData.Reset(changeKey);

        if (checkForSuccess)
        {
            // add a bar if at least targetChordsPerBar chords per bar were played during this key 'round'... // TODO: refine this?
            if (CountChords && KeyChordCount >= (beatData.BarsPerKey * targetChordsPerBar))
            {
                beatData.ExtendBars();      // also increases BPM
                progressionSuccess = true;
                instrumentKeys.ChangeGameKey(false);

                GameEvents.OnProgressionSuccess?.Invoke(KeyChordCount, beatData.BarsPerKey);
            }
            // add a bar if at least targetProgsPerBar progressions per bar were played during this key 'round'... // TODO: refine this?
            else if (KeyProgressionCount >= (beatData.BarsPerKey * targetProgsPerBar))
            {
                beatData.ExtendBars();      // also increases BPM
                progressionSuccess = true;
                instrumentKeys.ChangeGameKey(false);

                GameEvents.OnProgressionSuccess?.Invoke(KeyProgressionCount, beatData.BarsPerKey);
            }
            else        // did not 'play' enough chords/progressions - lose life
            {
                GameEvents.OnProgressionFail?.Invoke(KeyChordCount, beatData.BarsPerKey);
            }
        }

        // reset for next key
        KeyChordCount = 0;
        KeyProgressionCount = 0;

        if (CountChords)
            GameEvents.OnChordCountUpdated?.Invoke(KeyChordCount, TotalChordCount, beatData.BarsPerKey * targetChordsPerBar);
        else
            GameEvents.OnProgressionCountUpdated?.Invoke(KeyProgressionCount, TotalProgressionCount, beatData.BarsPerKey * targetProgsPerBar);

        if (changeKey)
        {
            beatData.RandomNoteIntervals();     // drum and bass intervals and bass note change

            // TODO: change to a random drum instrument?
            drumInstrument = instrumentKeys.drumInstruments.GetInstrumentByName(drumInstrumentName);
            if (drumInstrument == null)
            {
                Debug.LogError($"Reset: no drum instrument '{drumInstrumentName}' found!");
                return;
            }

            // set drum note according to gameKey
            SetRandomDrumNote(drumInstrument);

            // TODO: change to a random bass instrument?
            bassInstrument = instrumentKeys.bassInstruments.GetInstrumentByName(bassInstrumentName);
            if (bassInstrument == null)
            {
                Debug.LogError($"Reset: no bass instrument '{bassInstrumentName}' found!");
                return;
            }

            // set bass note according to gameKey
            SetRandomBassNote(bassInstrument);
        }

        GameEvents.OnStartPlayHead?.Invoke(beatData);
    }

    private void SetRandomDrumNote(Instrument drum)
    {
        drumClip = instrumentKeys.RandomInstrumentNote(drum, out _, out _);        // _ means 'discard'
        drumSource.clip = drumClip;
    }

    private void SetRandomBassNote(Instrument bass)
    {
        Note bassNote;
        bassClip = instrumentKeys.RandomInstrumentNote(bass, out _, out bassNote);        // _ means 'discard'
        bassSource.clip = bassClip;

        GameEvents.OnBassNoteChanged?.Invoke(bass, bassNote, bassClip);
    }

    // detect player inactivity

    //private void ResetInactiveBeatCount()
    //{
    //    inactiveBeatsLeft = inactiveBeatTimeout;
    //}

    //private void OnGrab(Projection grabObject, Vector2 grabPosition)
    //{
    //    inactiveTimeoutPaused = true;
    //    ResetInactiveBeatCount();
    //}

    // restart countdown on ball throw
    //private void OnThrow(Grabber throwObject, Vector3 throwForce, bool catapult)
    //{
    //    inactiveTimeoutPaused = false;
    //    ResetInactiveBeatCount();
    //}

    //private void OnRippleSpawned(Ripple newRipple)
    //{
    //    inactiveTimeoutPaused = true;
    //    ResetInactiveBeatCount();
    //}

    // restart countdown on ripple activated
    //private void OnRippleActivated()
    //{
    //    inactiveTimeoutPaused = false;
    //    ResetInactiveBeatCount();
    //}
}

