using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// plays a single audio clip, or a sequence (ie. scale, chord)
// also plays:
// a single chord or note for a given instrument (by looking up instrument's audio clip for a given note)
// all chords or notes in a given key / scale for a given instrument

// in scene, plays chords in the game key once set (OnGameStart)
// also attached to Ball prefab, used by BallAudio script to play a musical sound
// (chord from the game key) with the ball's instrument

public class NotePlayer : MonoBehaviour
{
    public InstrumentKeys instrumentKeys;           // all instruments and keys/scales
    public List<AudioSource> audioSources = new();  // 3, for polyphonic chords. cycles round when playing scales

    public bool isBallNotePlayer = false;

    private const int minChordsInProgression = 3;

    private int nextSourceIndex = 0;                 // next source to use (cycles)

    public bool playScaleOnKeyChange = true;
    public bool randomScaleInstrument = false;
    public string scaleInstrumentName = "Bells";     // name, for initial playthrough of chords OnSetGameKey
    public string gameOverInstrumentName = "Piano";  // name, scale on game over

    private float chordInterval = 0.15f;              // between each chord in key (arpeggio)
    private float noteInterval = 0.05f;              // between notes in scale or chord/arpeggio

    public bool PlayingChordProg { get; private set; }      // don't change chord progressions while playing!

    private Coroutine playChordsCoroutine;
    private Coroutine playChordNotesCoroutine;

    private Key currentGamekey;


    private void OnEnable()
    {
        GameEvents.OnGameKeyChanged += OnGameKeyChanged;        // on game start and after period of player inactivity
        GameEvents.OnBeat += OnBeat;
        GameEvents.OnGameStateChanged += OnGameStateChanged;
    }

    private void OnDisable()
    {
        GameEvents.OnGameKeyChanged -= OnGameKeyChanged;
        GameEvents.OnBeat -= OnBeat;
        GameEvents.OnGameStateChanged -= OnGameStateChanged;
    }


    private void OnGameKeyChanged(Key gameKey, bool playScale, bool gameStart)
    {
        currentGamekey = gameKey;

        //if (!isBallNotePlayer)
        //  StartCoroutine(PlayKeyIntro());
    }

    private void OnBeat(BeatData beatData)
    {
        //if (!isBallNotePlayer)
        // TODO: sync all notes / chords to beat?
    }

    private IEnumerator PlayKeyIntro()
    {
        if (playScaleOnKeyChange && !string.IsNullOrEmpty(scaleInstrumentName))
        {
            var keyIntroInstrument = randomScaleInstrument ? instrumentKeys.RandomInstrument
                                        : instrumentKeys.instrumentGroup.GetInstrumentByName(scaleInstrumentName);

            // chords sound nicer than key notes, which don't always ascend in pitch
            // ok for chords - just different 'voicings'

            // each chord in key, using keyIntroInstrument
            if (keyIntroInstrument != null)
            {
                yield return StartCoroutine(PlayChordsInKey(keyIntroInstrument, currentGamekey));
                yield return new WaitForSeconds(chordInterval);
            }

            //// each note in key, all instruments simultaneously
            //yield return new WaitForSeconds(chordInterval);
            //yield return StartCoroutine(PlayAllInstruments(gameKey));

            //// each note in 12-note octave, all instruments simultaneously
            //yield return new WaitForSeconds(chordInterval);
            //yield return StartCoroutine(PlayAllInstruments());
        }

        yield return null;
    }

    private void OnGameStateChanged(GameManager.GameState fromState, GameManager.GameState gameState)
    {
        if (gameState == GameManager.GameState.GameOver && !isBallNotePlayer)
        {
            var gameOverInstrument = instrumentKeys.instrumentGroup.GetInstrumentByName(gameOverInstrumentName);

            if (gameOverInstrument != null)
                StartCoroutine(PlayChordsInKey(gameOverInstrument, currentGamekey, true));
        }
    }

    // plays all instruments, each note in a key/scale in turn
    private IEnumerator PlayAllInstruments(Key key)
    {
        foreach (Note note in key.keyNotes)
        {
            //Debug.Log($"PlayChordsInKey: ({key.keyName}) chord: {triad.FullName} [ {triad.NotesInChord()}]");
            yield return StartCoroutine(PlayAllInstruments(note));
            yield return new WaitForSeconds(chordInterval);
        }

        // repeat root note to 'resolve'
        yield return StartCoroutine(PlayAllInstruments(key.keyNotes[0]));
    }

    // a single note, all instruments sumultaneously
    private IEnumerator PlayAllInstruments(Note note)
    {
        List<AudioClip> instrumentNotes = new();

        //Debug.Log($"PlayInstrumentChord: {chord.FullName} [ {chord.NotesInChord()}]");

        foreach (Instrument instrument in instrumentKeys.instrumentGroup.instruments)
        {
            instrumentNotes.Add(instrument.GetNoteClip(note));
        }

        yield return StartCoroutine(PlayClipSequence(instrumentNotes, false));
    }

    // a single note number (zero based), all instruments sumultaneously
    private IEnumerator PlayAllInstruments(int noteIndex)
    {
        List<AudioClip> instrumentNotes = new();

        foreach (Instrument instrument in instrumentKeys.instrumentGroup.instruments)
        {
            instrumentNotes.Add(instrument.NoteAtIndex(noteIndex));
        }

        yield return StartCoroutine(PlayClipSequence(instrumentNotes, true));
    }

    // plays through entire 12-note octave, each note played by all instruments
    private IEnumerator PlayAllInstruments()
    {
        for (int i = 0; i < InstrumentKeys.NotesInOctave; i++)
        {
            yield return StartCoroutine(PlayAllInstruments(i));
            yield return new WaitForSeconds(chordInterval);
        }

        // repeat root note to 'resolve'
        yield return StartCoroutine(PlayAllInstruments(0));
    }

    private IEnumerator PlayChordsInKey(Instrument instrument, Key key, bool reverse = false)
    {
        List<Triad> keyTriads = key.MakeKeyTriads();        // one for each note in key

        if (!reverse)
        {
            foreach (Triad triad in keyTriads)
            {
                //Debug.Log($"PlayChordsInKey: ({key.keyName}) chord: {triad.FullName} [ {triad.NotesInChord()}]");

                yield return StartCoroutine(PlayInstrumentChord(instrument, triad));
                yield return new WaitForSeconds(chordInterval);
            }

            // repeat root note to 'resolve'
            yield return StartCoroutine(PlayInstrumentChord(instrument, keyTriads[0]));
        }
        else
        {
            // start with root note
            yield return StartCoroutine(PlayInstrumentChord(instrument, keyTriads[0]));
            yield return new WaitForSeconds(chordInterval);

            // then play in reverse back to root note
            for (int i = keyTriads.Count-1; i >= 0; i--)
            {
                yield return StartCoroutine(PlayInstrumentChord(instrument, keyTriads[i]));
                yield return new WaitForSeconds(chordInterval);
            }
        }
    }

    // used by PlayChordsInKey
    private IEnumerator PlayInstrumentChord(Instrument instrument, Triad chord)
    {
        List<AudioClip> triadNotes = new();

        //Debug.Log($"PlayInstrumentChord: {chord.FullName} [ {chord.NotesInChord()}]");

        foreach (Note note in chord.chordNotes)
        {
            triadNotes.Add(instrument.GetNoteClip(note));
        }

        GameEvents.OnChordPlayed?.Invoke(instrument, chord, false, Color.clear);
        yield return StartCoroutine(PlayClipSequence(triadNotes, false));
    }

    // make a list of audioclips for the given instrument and key
    // and then play them in sequence, starting with the root note
    public void PlayInstrumentScale(Instrument instrument, Key key)
    {
        List<AudioClip> clipSequence = new();

        foreach (var note in key.keyNotes)
        {
            clipSequence.Add(instrument.GetNoteClip(note));
        }
        // repeat root note to 'resolve'
        clipSequence.Add(instrument.GetNoteClip(key.keyNotes[0]));

        StartCoroutine(PlayClipSequence(clipSequence, false));
    }

    public void PlayChords(List<Triad> chords, Instrument instrument, bool useNextSource, BallController ballController, bool countChords)
    {
        playChordsCoroutine = StartCoroutine(PlayChordProgression(chords, instrument, useNextSource, ballController, countChords));
    }

    public void StopPlaying()
    {
        StopCoroutine(playChordsCoroutine);
        playChordsCoroutine = null;

        StopCoroutine(playChordNotesCoroutine);
        playChordNotesCoroutine = null;

        PlayingChordProg = false;
    }

    // play a list of chords in sequence, cycling through available audio sources
    private IEnumerator PlayChordProgression(List<Triad> chords, Instrument instrument, bool useNextSource, BallController ballController, bool countChords)
    {
        if (audioSources.Count == 0)
        {
            Debug.LogError("PlayChordProgression requires at least one AudioSource!");
            yield break;
        }

        if (chords.Count == 0)
        {
            Debug.LogError("PlayChordProgression requires at least one chord!");
            yield break;
        }

        if (instrument == null)
        {
            Debug.LogError("PlayChordProgression requires an instrument!");
            yield break;
        }

        while (ballController.IsSpawning)
        {
            yield return null;
        }

        PlayingChordProg = true;

        // 3 or more chords counts as a progression!
        if (isBallNotePlayer && chords.Count >= minChordsInProgression)
            GameEvents.OnChordProgressionPlayed?.Invoke(instrument);

        foreach (Triad chord in chords)
        {
            GameEvents.OnChordPlayed?.Invoke(instrument, chord, countChords, ballController.Colour);
            //var playChordNotesCoroutine = PlayClipSequence(instrumentKeys.InstrumentChordClips(instrument, chord), useNextSource);
            var playChordNotesCoroutine = PlayChordNotes(chord, instrument, useNextSource);

            yield return StartCoroutine(playChordNotesCoroutine);
            yield return new WaitForSeconds(chordInterval);
        }

        //// 3 or more chords counts as a progression!
        //if (isBallNotePlayer && chords.Count >= minChordsInProgression)
        //    GameEvents.OnChordProgressionPlayed?.Invoke(instrument);

        PlayingChordProg = false;
    }

    private IEnumerator PlayChordNotes(Triad chord, Instrument instrument, bool useNextSource)
    {
        var playNotesCoroutine = PlayClipSequence(instrumentKeys.InstrumentChordClips(instrument, chord), useNextSource);
        playChordNotesCoroutine = StartCoroutine(playNotesCoroutine);
        yield return playChordNotesCoroutine;
    }

    // play a list of audio clips in sequence, cycling through available audio sources
    private IEnumerator PlayClipSequence(List<AudioClip> sequence, bool useNextSource)
    {
        if (audioSources.Count == 0)
            Debug.LogError("PlayClipSequence requires at least one AudioSource!");

        if (sequence.Count == 0)
            Debug.LogError("PlayClipSequence requires at least one AudioClip!");

        if (! useNextSource)
            nextSourceIndex = 0;

        foreach (AudioClip clip in sequence)
        {
            PlayClip(clip, nextSourceIndex);
            yield return new WaitForSeconds(noteInterval);

            nextSourceIndex++;

            // re-use (loop round) audio sources if more notes than sources!
            if (nextSourceIndex >= audioSources.Count)
                nextSourceIndex = 0;
        }
    }

    // play an audioclip

    public void PlayInstrumentNote(Instrument instrument, Note note)
    {
        PlayClip(instrument.GetNoteClip(note));
    }

    public void PlayClip(AudioClip audioClip, int sourceIndex = 0)
    {
        if (audioClip != null)
        {
            audioSources[sourceIndex].clip = audioClip;
            audioSources[sourceIndex].Play();
        }
    }
}
