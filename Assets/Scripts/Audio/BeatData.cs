﻿using System;
using UnityEngine;

// data broadcast on every beat event

[Serializable]
public class BeatData
{
    [Range(20, 200)]
    public int BPM = 40;
    public float BeatInterval => 60f / BPM;        // seconds

    public float KeyBarsTime => BeatsPerKey * BeatInterval;        // seconds

    [Range(1, 10)]
    public int BPMIncrement = 4;               // increased on every 'successful' key change

    [Range(1, 8)]
    public int BeatsPerBar = 4;             // set in inspector
    private readonly int[] beatIntervals = {1, 2, 4};         // for drum and bass intervals and bass note changes  (change on 3rd note sounds weird)
    private int RandomBarBeat => beatIntervals [ UnityEngine.Random.Range(0, beatIntervals.Length) ];      // for drum and bass intervals and bass note changes

    public int BarsPerKey = 8;          // change key after this many bars

    private int BeatsPerDrum = 2;       // beats between each drum note. default only - changed on key change
    private int BeatsPerBass = 1;       // beats between each bass note. default only - changed on key change
    private int BassNoteBeats = 4;      // bass note changes every this many beats. default only - changed on key change

    // counters for current beats and bars
    public int BeatCount { get; private set; } = 0;                 // reset on key change. first beat == 1
    public int BarCount => ((BeatCount-1) / BeatsPerBar) + 1;       // current bar (starts at 1)
    public int BarBeats => (BarCount-1) * BeatsPerBar;              // number of full bar beats so far
    public int BarBeatCount => BeatCount - BarBeats;                // number of beats into the current bar
    public int BeatsPerKey => BarsPerKey * BeatsPerBar;             // total number of beats between key changes

    // determine when to play drum / bass notes and when to change bass note and key
    public bool IsDrumBeat => BeatCount % BeatsPerDrum == 0;              // divides exactly, no remainder
    public bool IsBassBeat => BeatCount % BeatsPerBass == 0;              // divides exactly, no remainder
    public bool IsBassChangeBeat => BeatCount % BassNoteBeats == 0;       // divides exactly, no remainder
    public bool IsBarChangeBeat => BeatCount % BeatsPerBar == 0;          // divides exactly, no remainder
    public bool IsLastBeat => BeatCount == BeatsPerKey;              // last beat of bars in this key   


    public void CountBeat()
    {
        BeatCount++;
    }

    public void Reset(bool resetBars)
    {
        BeatCount = 0;      // set to 1 on first beat

        if (resetBars)
            BarsPerKey = 1;     // TODO: start with one bar?
    }

    // eg. on successful 'progression' at key change
    public void ExtendBars()
    {
        BarsPerKey++;
        BPM += BPMIncrement;
    }

    public void RandomNoteIntervals()
    {
        BeatsPerDrum = RandomBarBeat;
        BeatsPerBass = RandomBarBeat;
        BassNoteBeats = RandomBarBeat;
    }

    public string ToUIString()
    {
        return "Beat " + BarBeatCount + "/" + BeatsPerBar + "  Bar " + BarCount + "/" + BarsPerKey;
    }

    public override string ToString()
    {
        return "BeatCount " + BeatCount + " BarBeats " + BarBeats + " BarCount " + BarCount + " BarBeatCount " + BarBeatCount;
    }
}