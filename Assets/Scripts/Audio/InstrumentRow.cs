using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstrumentRow : MonoBehaviour
{
    public Instrument instrument;

    public InstrumentBar GetInstrumentBar(int index) => InstrumentBars[index];

    public Color RowColour => InstrumentBars[0].InstrumentColour;       // semi-transparent
    public Color RowFullColour => new Color(RowColour.r, RowColour.g, RowColour.b, 1f);

    public List<InstrumentBar> InstrumentBars { get; private set; } = new();

    public void AddInstrumentBar(InstrumentBar instrumentBar, Instrument instr)
    {
        instrument = instr;
        InstrumentBars.Add(instrumentBar);
    }
}
