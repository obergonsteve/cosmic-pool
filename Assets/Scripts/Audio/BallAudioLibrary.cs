using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "BallAudioLibrary")]

// list of musical audio clips assigned to spawned balls at random

public class BallAudioLibrary : ScriptableObject
{
    public List<AudioClip> ballAudioList = new List<AudioClip>();

    public AudioClip RandomAudioClip => ballAudioList[UnityEngine.Random.Range(0, ballAudioList.Count)];
}