﻿

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

// bunch of classes (not MonoBehaviours) used to represent instruments and the sounds they make (12)
// and 'data' in music theory - ie. keys/scales, chords(triads), major/minor

// available instruments and keys data stored in InstrumentKeys scriptable object

//public enum InstrumentRange
//{
//    All,
//    Treble,
//    MidRange,
//    Bass,
//    Percussion
//};

// container for all the available instruments
// in groups that sound nice together
[Serializable]
public class InstrumentGroups
{
    public string GroupsName;       // eg. treble, mid-range, bass etc
    //public InstrumentRange GroupsRange;

    public List<InstrumentGroup> instrumentGroups = new();
    public InstrumentGroup RandomInstrumentGroup => instrumentGroups[UnityEngine.Random.Range(0, instrumentGroups.Count)];
}

// container for all the instruments in a set/ensemble/group (sound nice together)
[Serializable]
public class InstrumentGroup
{
    public string GroupName;

    public List<Instrument> instruments = new List<Instrument>();
    public Instrument RandomInstrument => instruments[UnityEngine.Random.Range(0, instruments.Count)];

    // set up dictionary for each instrument, so audio clips can be looked up by Note enum
    public void InitInstrumentNotes()
    {
        foreach (var instrument in instruments)
        {
            instrument.InitNotes();
        }
    }

    // use LINQ to return the instrument in the list that matches the given name. null if instrumentName not found
    public Instrument GetInstrumentByName(string instrumentName) => instruments.FirstOrDefault((instr) => instr.instrumentName == instrumentName);
}

// container for the 12 audio clips (A to Gs/Af) for an instrument's octave
[Serializable]
public class Instrument
{
    public string instrumentName;
    public List<AudioClip> audioClips = new List<AudioClip>();      // 12 notes of the octave

    public AudioClip NoteAtIndex(int index) => audioClips[index];

    // dictionary so we can look up the audio clip for a given note (enum) from this instrument's 12 notes
    private Dictionary<Note, AudioClip> notesDict;

    // construct dictionary for note lookup
    // called OnGameStart
    public void InitNotes()
    {
        notesDict = new();

        for (int i = 0; i < audioClips.Count; i++)
        {
            AudioClip clip = audioClips[i];
            notesDict.Add((Note)i, clip);
        }
    }

    // NB: assumes Init has been called to set up notesDict (ie. called after OnGameStart)
    public AudioClip GetNoteClip(Note note)
    {
        //Debug.Log($"GetNoteClip: {note} count = {audioClips.Count}");
        if (notesDict == null)
        {
            Debug.LogError("GetNoteClip: notes dictionary not initialised!!");
            return null;
        }

        return notesDict[note];
    }

    // NB: assumes Init has been called to set up dict (ie. called after OnGameStart)
    public List<AudioClip> GetNoteClips(List<Note> notes)
    {
        if (notesDict == null)
        {
            Debug.LogError("GetNoteClips: notes dictionary not initialised!!");
            return null;
        }

        List<AudioClip> audioClips = new();

        foreach (var note in notes)
        {
            audioClips.Add(GetNoteClip(note));
        }

        return audioClips;
    }
}

// container for all the available keys
[Serializable]
public class Keys
{
    public List<Key> keys = new List<Key>();
    public Key RandomKey => keys[UnityEngine.Random.Range(0, keys.Count)];
}

// all the notes in a key (typically (always?) 7)
// starting with the root note
// aka 'key signature' - can somehow be derived form circle of fifths...
[Serializable]
public class Key
{
    public string keyName;

    public Note rootNote;
    public ScaleMode scaleMode;     // major, minor, multiple others!  set in inspector

    public string FullName => InstrumentKeys.NoteName(rootNote) + " " + scaleMode;

    public List<Note> keyNotes = new();     // set in inspector

    public int RandomKeyNoteIndex => UnityEngine.Random.Range(0, keyNotes.Count);
    public Note KeyNoteByIndex(int index) => keyNotes[index];
    public Note RandomKeyNote => keyNotes[RandomKeyNoteIndex];
    public Triad RandomKeyTriad => MakeTriad(UnityEngine.Random.Range(0, keyNotes.Count) + 1, false);      // MakeTriad uses key's note number (1-7)

    // make up a chord from this key
    // using the given note number in the key (1-7) as the root
    public Triad MakeTriad(int rootNoteNumber, bool skipDiminished)
    {
        Triad chord = new() { inKey = this, chordName = keyNotes[rootNoteNumber - 1] };

        int noteNumber = rootNoteNumber;

        switch (scaleMode)
        {
            case ScaleMode.Major:
            {
                // major if starting on 1, 4 or 5
                // minor if starting on 2, 3 or 6
                // diminished 7
                switch (rootNoteNumber)
                {
                    case 1:
                        chord.majorMinor = MajorMinor.Major;
                        break;
                    case 2:
                        chord.majorMinor = MajorMinor.Minor;
                        break;
                    case 3:
                        chord.majorMinor = MajorMinor.Minor;
                        break;
                    case 4:
                        chord.majorMinor = MajorMinor.Major;
                        break;
                    case 5:
                        chord.majorMinor = MajorMinor.Major;
                        break;
                    case 6:
                        chord.majorMinor = MajorMinor.Minor;
                        break;
                    case 7:
                        if (skipDiminished)
                            return null;
                        chord.majorMinor = MajorMinor.Dim;
                        break;
                    default:
                        return null;
                    }
                break;
            }

            case ScaleMode.Minor:
            {
                // minor if starting on 1, 4 or 5
                // major if starting on 3, 6 or 7
                // diminished 2
                switch (rootNoteNumber)
                {
                    case 1:
                        chord.majorMinor = MajorMinor.Minor;
                        break;
                    case 2:
                        if (skipDiminished)
                            return null;
                        chord.majorMinor = MajorMinor.Dim;
                        break;
                    case 3:
                        chord.majorMinor = MajorMinor.Major;
                        break;
                    case 4:
                        chord.majorMinor = MajorMinor.Minor;
                        break;
                    case 5:
                        chord.majorMinor = MajorMinor.Minor;
                        break;
                    case 6:
                        chord.majorMinor = MajorMinor.Major;
                        break;
                    case 7:
                        chord.majorMinor = MajorMinor.Major;
                        break;
                    default:
                          return null;
                }
                break;
            }
        }

        // get the key's 1, 3 and 5 notes from root note (ie. 2 notes apart)
        // to make the triad
        for (int i = 0; i < Triad.NumberOfNotes; i++)
        {
            chord.chordNotes.Add(keyNotes [ noteNumber - 1 ]);       // noteNumber indexed from 1

            if (noteNumber + 2 <= keyNotes.Count)
                noteNumber += 2;
            else if (noteNumber == keyNotes.Count - 1)       // wrap around to first note
                noteNumber = 1;
            else                                           // wrap around to second note
                noteNumber = 2;
        }

        //Debug.Log($"MakeTriad: (key {chord.inKey.keyName}) chord: {chord.FullName} [ {chord.NotesInChord()}]");
        return chord;
    }

    // make chords for each of the notes in the key
    // starting at chordRoot in the scale
    // 3 notes in triad chord
    public List<Triad> MakeKeyTriads()
    {
        List<Triad> keyTriads = new();

        for (int i = 1; i <= keyNotes.Count; i++)
        {
            var triad = MakeTriad(i, false);
            if (triad != null)      // could be if skipDiminished
                keyTriads.Add(triad);
        }

        return keyTriads;
    }

    public string NotesInKey()
    {
        string notes = "";
        foreach (var note in keyNotes)
        {
            notes += note + " ";
        }
        return notes;
    }
}

// 3 notes from a key that make up a chord
[Serializable]
public class Triad
{
    public static int NumberOfNotes = 3;

    public Key inKey;
    public Note chordName;
    public MajorMinor majorMinor;
    public string FullName => chordName + " " + majorMinor;

    public List<Note> chordNotes = new List<Note>(NumberOfNotes);

    public Note chordRoot => chordNotes[0];

    // string containing all notes in triad
    public string NotesInTriad()
    {
        string notes = "";
        foreach (var note in chordNotes)
        {
            notes += note + " ";
        }
        return notes;
    }
}

// the 12 notes in a musical octave
public enum Note
{
    A = 0,
    Bf = 1,
    B = 2,
    C = 3,
    Cs = 4,
    D = 5,
    Ef = 6,
    E = 7,
    F = 8,
    Fs = 9,
    G = 10,
    Gs = 11
}

// the 12 notes that make up the circle of fifths, starting from C (at 12 o'clock)
// each a 'perfect fifth?' (7 semitones) apart
// seems to be used to find key signatures...
public class CircleOfFifths
{
    // create and initialise a list in one statement...
    public static readonly List<Note> Notes = new() { Note.C, Note.G, Note.D, Note.A, Note.E, Note.B, Note.Fs, Note.Cs, Note.Gs, Note.Ef, Note.Bf, Note.F };
    public static readonly List<Note> RelativeMinor = new() { Note.C, Note.G, Note.D, Note.A, Note.E, Note.B, Note.Fs, Note.Cs, Note.Gs, Note.Ef, Note.Bf, Note.F };
}

// chord type (acording to ScaleType)
public enum MajorMinor
{
    Major,
    Minor,
    Dim
}

public enum ScaleMode
{
    Major,
    Minor,
    // many others!
}