using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


// visual for countdowns (eg. grab/ripple sim time or cooldown time)
// enabled and disabled as needed

public class CountdownSlider : MonoBehaviour
{
    public Slider countdownSlider;

    private int sliderTweenId;
    private int pulseTweenId;

    private float pulseScale = 1.1f;
    private float pulseTime = 0.1f;

    private Vector2 startScale = Vector2.one;


    public void Init()
    {
        countdownSlider.value = 0f;
        transform.localScale = startScale;
    }

    public void StartCountdown(float time)
    {
        LeanTween.cancel(sliderTweenId);      // stop existing tween
        LeanTween.cancel(pulseTweenId);      // stop existing tween

        countdownSlider.minValue = 0f;
        countdownSlider.maxValue = time;

        //gameObject.SetActive(true);

        sliderTweenId = LeanTween.value(time, 0f, time)
                            .setEaseLinear()
                            .setOnUpdate((float f) => countdownSlider.value = f)
                            //.setOnComplete(() => gameObject.SetActive(false))
                            .id;
    }

    public void StopCountdown()
    {
        LeanTween.cancel(sliderTweenId);      // stop existing tween
        LeanTween.cancel(pulseTweenId);      // stop existing tween
        //gameObject.SetActive(false);
    }

    public void Pulse()
    {
        LeanTween.cancel(pulseTweenId);      // stop existing tween

        transform.localScale = startScale;
        pulseTweenId = LeanTween.scale(gameObject, startScale * pulseScale, pulseTime)
                            .setEaseOutCubic()
                            .setLoopPingPong(1)
                            .id;
    }
}

