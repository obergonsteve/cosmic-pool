using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;


// all UI related to playing notes and chords =)
//
// bar grid (row for each instrument / colour, column for each bar)
// play head sweep over bars
// pulsing on the beat

public class MusicUI : MonoBehaviour
{
    [Header("Feedback Text")]
    [SerializeField]
    private TextMeshProUGUI gameKeyText;            // bottom right, blue

    [SerializeField]
    private Image BPMPanel;

    [SerializeField]
    private TextMeshProUGUI BPMText;    

    [SerializeField]
    private TextMeshProUGUI bassNotePlayedText;      // bottom right, white

    [SerializeField]
    private TextMeshProUGUI beatText;        // bottom right, orange

    [SerializeField]
    private TextMeshProUGUI successText;      

    [SerializeField]
    private TextMeshProUGUI newKeyText;       

    [SerializeField]
    private TextMeshProUGUI keysText;        // top left

    [SerializeField]
    private TextMeshProUGUI chordProgressionsText;

    [SerializeField]
    private Slider progressionsSlider;        // top left

    [SerializeField]
    private Slider timingSlider;            // top left

    private Vector2 sliderScale;
    private float progSliderUpdateTime = 0.15f;
    private float timingSliderUpdateTime = 0.1f;

    [SerializeField]
    private Color newKeyColour = Color.blue;

    //[SerializeField]
    //private Color successColour = Color.magenta;

    [SerializeField]
    private Image beatImage;

    // musical 'score' built as a grid of cells

    [Header("Instrument / Bar Grid")]
    [SerializeField]
    private Transform KeyChords;                      // parent of ChordUIs

    [SerializeField]
    private KeyBarsUI KeyBars;                      // vertical layout group (rows)
    private Image keyBarsImage;
    private float barAlpha = 0.025f;             // instrument row / bar colour

    [SerializeField]
    private Image PlayHead;

    [Header("Row & Bar Prefabs")]
    [SerializeField]
    private InstrumentRow InstrumentRowPrefab;             // horizontal layout group (bars)

    private List<InstrumentRow> instrumentRows = new();

    [SerializeField]
    private InstrumentBar InstrumentBarPrefab;            // each bar in a row

    [SerializeField]
    private ChordUI ChordUIPrefab;                      // placed on KeyBars when chord played

    // flash times are divided by bars in key
    private float barFlashTime = 0.5f;              // time to tween from on/off/on
    private float barFlashOnTime = 1f;              // time flash stays on/off
    private float barFlashAlpha = 0.15f;            // instrument row / bar colour
    private float barFlashDelay = 0.75f;             // instrument row / bar colour

    // pulsing to the beat!
    private float textPulseScale = 1.15f;
    private float imagePulseScale = 1.2f;
    private float pulseTime = 0.075f;

    // key change feedback
    private float flashTime = 0.75f;             // time to tween from on/off/on
    private float flashOnTime = 1.5f;             // time text stays on/off

    private const string FlatChar = "A";          // in special font
    private const string SharpChar = "C";          // in special font

    private GameManager.GameState currentGameState;
    private int timingSliderTween;

    private void OnEnable()
    {
        GameEvents.OnGameStateChanged += OnGameStateChanged;

        GameEvents.OnGameKeyChanged += OnGameKeyChanged;
        GameEvents.OnStartPlayHead += OnStartPlayHead;
        GameEvents.OnBuildKeyGrid += OnBuildKeyGrid;            // after game key changed and instrument colours set

        GameEvents.OnBassNoteChanged += OnBassNoteChanged;
        GameEvents.OnChordPlayed += OnChordPlayed;
        GameEvents.OnBeat += OnBeat;
        GameEvents.OnTouchBeatAccuracy += OnTouchBeatAccuracy;

        GameEvents.OnChordCountUpdated += OnChordCountUpdated;
        GameEvents.OnProgressionCountUpdated += OnProgressionCountUpdated;
    }

    private void OnDisable()
    {
        GameEvents.OnGameStateChanged -= OnGameStateChanged;

        GameEvents.OnGameKeyChanged -= OnGameKeyChanged;
        GameEvents.OnStartPlayHead -= OnStartPlayHead;
        GameEvents.OnBuildKeyGrid -= OnBuildKeyGrid;

        GameEvents.OnBassNoteChanged -= OnBassNoteChanged;
        GameEvents.OnChordPlayed -= OnChordPlayed;
        GameEvents.OnBeat -= OnBeat;
        GameEvents.OnTouchBeatAccuracy -= OnTouchBeatAccuracy;

        GameEvents.OnChordCountUpdated -= OnChordCountUpdated;
        GameEvents.OnProgressionCountUpdated -= OnProgressionCountUpdated;
    }

    private void Awake()
    {
        keyBarsImage = KeyBars.GetComponent<Image>();
        sliderScale = progressionsSlider.transform.localScale;
    }

    private void OnGameKeyChanged(Key gameKey, bool playScale, bool gameStart)
    {
        if (currentGameState == GameManager.GameState.GameOver)
            return;

        // reset
        timingSlider.value = 0;

        //gameKeyText.text = inTheKeyOfText + " " + gameKey.FullName;
        Pulse(gameKeyText.transform, gameKeyText.transform.localScale, textPulseScale);

        FlashFeedback(newKeyText, newKeyColour, gameKey.FullName, 0f);
    }

    // restart play head on key change
    private void OnStartPlayHead(BeatData beatData)
    {
        BPMText.text = beatData.BPM.ToString();
        StartPlayHead(beatData.KeyBarsTime);

        ClearChords();
    }

    // runs from left to right edge of the parent 
    private void StartPlayHead(float playTime)
    {
        PlayHead.transform.localPosition = new Vector2(-keyBarsImage.rectTransform.rect.width / 2f, 0f);        // left edge
        var targetPosition = new Vector2(keyBarsImage.rectTransform.rect.width / 2f, 0f);                       // right edge

        LeanTween.moveLocalX(PlayHead.gameObject, targetPosition.x, playTime);
    }

    // build instrument rows and bars after game key changed and instrument colours have been set
    private void OnBuildKeyGrid(GameData gameData, InstrumentKeys instrumentKeys, BeatData beatData, bool flashBars)
    {
        if (currentGameState == GameManager.GameState.GameOver)
            return;

        ClearKeyGrid();

        foreach (BallColours.BallColour ballColour in instrumentKeys.BallColours)
        {
            var colour = gameData.GetBallColour(ballColour);
            Color rowColour = new(colour.r, colour.g, colour.b, barAlpha);

            InstrumentRow instrumentRow = Instantiate(InstrumentRowPrefab, KeyBars.transform);
            instrumentRow.instrument = instrumentKeys.GetColourInstrument(ballColour);

            instrumentRows.Add(instrumentRow);
            KeyBars.AddInstrumentRow(instrumentRow);

            Color rowFlashColour = new(colour.r, colour.g, colour.b, barFlashAlpha);

            for (int bar = 0; bar < beatData.BarsPerKey; bar++)
            {
                InstrumentBar instrumentBar = Instantiate(InstrumentBarPrefab, instrumentRow.transform);
                instrumentBar.SetColour(rowColour);

                instrumentRow.AddInstrumentBar(instrumentBar, instrumentKeys.GetColourInstrument(ballColour));

                if (flashBars)
                    FlashBar(instrumentBar, rowColour, rowFlashColour, barFlashDelay * bar, beatData.BarsPerKey);
            }
        }
    }

    // instantiate a ChordUI and place on grid at playhead / instrument row intersection
    private void OnChordPlayed(Instrument instrument, Triad chord, bool countChord, Color ballColour)
    {
        if (currentGameState == GameManager.GameState.GameOver)
            return;

        InstrumentRow instrumentRow = KeyBars.GetInstrumentRowByColour(ballColour);

        if (instrumentRow != null)
        {
            ChordUI chordUI = Instantiate(ChordUIPrefab, KeyChords);

            // set text to ball colour
            var rowColour = instrumentRow.RowColour;        // tinted down for UI background
            var fullNoteColour = new Color(rowColour.r, rowColour.g, rowColour.b, 1f);
            chordUI.SetChordText(chord.chordName.ToString(), fullNoteColour);

            chordUI.transform.position = new Vector2(PlayHead.transform.position.x, instrumentRow.transform.position.y);
            Pulse(chordUI.transform, chordUI.transform.localScale, textPulseScale);
        }
    }

    private void OnChordCountUpdated(int keyChordCount, int totalChordCount, int chordsPerKey)
    {
        if (currentGameState != GameManager.GameState.GameOver)
            UpdateProgressionSlider(keyChordCount, chordsPerKey);
    }

    private void OnProgressionCountUpdated(int keyProgCount, int totalProgCount, int progsPerKey)
    {
        if (currentGameState != GameManager.GameState.GameOver)
            UpdateProgressionSlider(keyProgCount, progsPerKey);
    }

    private void UpdateProgressionSlider(int newValue, int maxValue)
    {
        if (progressionsSlider.value != newValue && newValue <= maxValue)
        {
            progressionsSlider.maxValue = maxValue;

            LeanTween.value(progressionsSlider.value, (float)newValue, progSliderUpdateTime)
                                .setEaseInSine()
                                .setOnUpdate((float v) => progressionsSlider.value = v);

            if (newValue > 0)
                Pulse(progressionsSlider.transform, sliderScale, imagePulseScale);
        }

        chordProgressionsText.text = newValue + "/" + maxValue;
    }

    // event handlers for touch/release timing calculated from last beat time

    private void OnTouchBeatAccuracy(float averageTouchAccuracy)
    {
        if (currentGameState != GameManager.GameState.GameOver)
            UpdateTimingSlider(averageTouchAccuracy);
    }


    private void UpdateTimingSlider(float lastBeatIntervalPercent)
    {
        if (timingSlider.value != lastBeatIntervalPercent)
        {
            timingSliderTween = LeanTween.value(timingSlider.value, (float)lastBeatIntervalPercent, timingSliderUpdateTime)
                                .setEaseInSine()
                                .setOnUpdate((float v) => timingSlider.value = v)
                                .id;
        }
    }


    private void OnBassNoteChanged(Instrument instrument, Note note, AudioClip clip)
    {
        bassNotePlayedText.text = instrument.instrumentName + " : " + InstrumentKeys.NoteName(note);
    }

    private void OnBeat(BeatData beatData)
    {
        beatText.text = beatData.ToUIString();

        Pulse(beatText.transform, beatText.transform.localScale, textPulseScale);

        Pulse(beatImage.transform, beatImage.transform.localScale, imagePulseScale);
        Pulse(BPMPanel.transform, BPMPanel.transform.localScale, textPulseScale);
        Pulse(PlayHead.transform, PlayHead.transform.localScale, textPulseScale);

        // pulse timing slider on every beat
        Pulse(timingSlider.transform, timingSlider.transform.localScale, imagePulseScale);
    }

    private void ClearKeyGrid()
    {
        instrumentRows.Clear();

        KeyBars.ClearInstrumentRows();

        foreach (Transform row in KeyBars.transform)
        {
            Destroy(row.gameObject);
        }

        ClearChords();
    }

    private void ClearChords()
    {
        foreach (Transform chordUI in KeyChords.transform)
        {
            Destroy(chordUI.gameObject);
        }
    }

    private void OnGameStateChanged(GameManager.GameState fromState, GameManager.GameState gameState)
    {
        currentGameState = gameState;

        switch (gameState)
        {
            case GameManager.GameState.WaitingToStart:
                break;

            case GameManager.GameState.BallSetup:
                break;

            case GameManager.GameState.WaitingForPlayer:
                break;

            case GameManager.GameState.Projecting:          // countdown slider
                break;

            case GameManager.GameState.WaitingForGame:      // countdown slider
                break;

            case GameManager.GameState.GameOver:
                GameOver();
                break;
        }
    }

    private void GameOver()
    {
        gameKeyText.gameObject.SetActive(false);
        beatImage.gameObject.SetActive(false);
        bassNotePlayedText.gameObject.SetActive(false);
        beatText.gameObject.SetActive(false);
    }

    private void Pulse(Transform scaleObject, Vector2 startScale, float toScale)
    {
        //var startScale = scaleObject.localScale;
        LeanTween.scale(scaleObject.gameObject, startScale * toScale, pulseTime)
                            .setEaseOutCubic()
                            .setLoopPingPong(1);
    }

    // tween on, wait, off
    // faster as bars increase
    private void FlashBar(InstrumentBar bar, Color rowColour, Color flashColour, float flashDelay, int barsPerKey)
    {
        LeanTween.value(bar.gameObject, rowColour, flashColour, barFlashTime / barsPerKey)
                    .setDelay(flashDelay / barsPerKey)
                    .setOnUpdate((Color c) =>
                    {
                        bar.ChangeColour(c);
                    })
                    .setOnComplete(() =>        // back to row colour after delay
                    {
                        LeanTween.value(bar.gameObject, flashColour, rowColour, barFlashTime / barsPerKey)
                                    .setDelay(barFlashOnTime)
                                    .setOnUpdate((Color c) =>
                                    {
                                        bar.ChangeColour(c);
                                    });
                    });
    }

    // tween on, wait, off
    private void FlashFeedback(TextMeshProUGUI feedbackText, Color colour, string feedback, float delay = 0f)
    {
        LeanTween.value(feedbackText.gameObject, Color.clear, colour, flashTime)
                    .setDelay(delay)
                    .setOnStart(() =>
                    {
                        feedbackText.color = Color.clear;
                        feedbackText.text = feedback;
                        feedbackText.gameObject.SetActive(true);
                    })
                    .setOnUpdate((Color c) =>
                    {
                        feedbackText.color = c;
                    })
                    .setOnComplete(() =>        // turn off after delay
                    {
                        LeanTween.value(feedbackText.gameObject, colour, Color.clear, flashTime)
                                    .setDelay(flashOnTime)
                                    .setOnUpdate((Color c) =>
                                    {
                                        feedbackText.color = c;
                                    })
                                    .setOnComplete(() =>
                                    {
                                        feedbackText.gameObject.SetActive(false);
                                    });
                    });
    }
}
