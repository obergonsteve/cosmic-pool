using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ChordUI : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI chordText;

    public void SetChordText(string chord, Color colour)
    {
        chordText.text = chord;
        chordText.color = colour;
    }
}
