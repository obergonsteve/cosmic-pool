using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class LivesPanel : MonoBehaviour
{
    public Image LivesImage;

    private float pulseTime = 0.25f;
    private float pulseScale = 1.5f;

    private bool pulsing;

    private void OnEnable()
    {
        GameEvents.OnLivesLeftUpdated += OnLivesLeftUpdated;
    }

    private void OnDisable()
    {
        GameEvents.OnLivesLeftUpdated -= OnLivesLeftUpdated;
    }

    private void OnLivesLeftUpdated(float livesLeft, float added, int livesWarning)
    {
        if (! pulsing)
            PulseLives();
    }

    private void PulseLives()
    {
        if (pulsing)
            return;

        pulsing = true;
        LeanTween.scale(LivesImage.gameObject, LivesImage.transform.localScale * pulseScale, pulseTime)
                    .setLoopPingPong(1)
                    .setEaseInOutSine()
                    .setOnComplete(() => { pulsing = false; });
    }
}
