using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverPanel : MonoBehaviour
{
    public Button RestartButton;

    private float pulseTime = 0.5f;
    private float pulseScale = 1.1f;

    private int pulseTweenId;

    private void OnEnable()
    {
        PulseRestartButton();
    }

    private void OnDisable()
    {
        LeanTween.cancel(pulseTweenId);
    }

    private void PulseRestartButton()
    {
        pulseTweenId = LeanTween.scale(RestartButton.gameObject, RestartButton.transform.localScale * pulseScale, pulseTime)
                    .setLoopPingPong()
                    .setEaseInOutSine()
                    .id;
    }
}
