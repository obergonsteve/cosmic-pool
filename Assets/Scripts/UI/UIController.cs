using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class UIController : MonoBehaviour
{
    [SerializeField]
    private GameData gameData;

    [SerializeField]
    private BallUI ballUIPrefab;

    [SerializeField]
    private TextMeshProUGUI grabTimer;

    [SerializeField]
    private Image livesPanel;

    [SerializeField]
    private TextMeshProUGUI livesText;

    //[SerializeField]
    //private TextMeshProUGUI chordProgressionsText;

    [SerializeField]
    private TextMeshProUGUI scoreText;

    [SerializeField]
    private Image menuPanel;

    [SerializeField]
    private Image gameOverPanel;

    [SerializeField]
    private TextMeshProUGUI gameOverText;

    [SerializeField]
    private TextMeshProUGUI hiScoreText;

    [SerializeField]
    private TextMeshProUGUI newHiScoreText;

    [SerializeField]
    private TextMeshProUGUI feedbackText;           // top centre, flashes

    [SerializeField]
    private TextMeshProUGUI gameStatusText;         // bottom left, white

    [SerializeField]
    private TextMeshProUGUI totalScoreInPlayText;   // bottom right, cyan

    [SerializeField]
    private TextMeshProUGUI debugText;              // bottom right

    [SerializeField]
    private Image countdownPanel;  

    [SerializeField]
    private CountdownSlider countdownSlider;        // visual for grab timer and cooldown timer

    [SerializeField]
    private Button restartButton;                   // on game over panel

    [SerializeField]
    private Color warningColour = Color.red;

    private int newHiScoreFlashes = 0;
    private bool EndlessFlashes => newHiScoreFlashes == 0;

    private int newHiScoreFlashesLeft = 0;

    private float feedbackFlashTime = 0.5f;          // time to tween from on/off/on
    private float feedbackFlashOnTime = 1.25f;        // time text stays on/off

    private float pulseScale = 1.15f;
    private float pulseTime = 0.1f;

    private Color hiScoreColour;        // set on start
    private Color newLivesColour;       // set on start

    private Camera mainCamera;

    private GameManager.GameState currentGameState;

    // game state text
    private const string spawningFeedback = "New Balls Please...";              // while spawning balls (at start or on grab)
    //private const string waitForPlayerFeedback = "Make Your Move...";         // after ball thrown / ripple released
    private const string waitForPlayerFeedback = "Take Your Best Shot...";      // after ball thrown / ripple released
    //private const string waitForGameFeedback = "Please Wait...";              // after cooldown

    // feedback text
    //private const string newLifeFeedback = "Life's on the Up!";                 // life gained
    //private const string lifeWarningFeedback = "Life is but Short";             // life low
    private const string lifeWarningFeedback = "Keys are Low...";                  // life low
    //private const string lastLifeFeedback = "Beware the End...";              // one life left
    private const string gameOverFeedback = "Oh. That was Careless...";         // no new hi-score
    private const string newHiScoreFeedback = "At Least it Ended on a Hi...";   // game over, new hi-score


    private void OnEnable()
    {
        GameEvents.OnGameStateChanged += OnGameStateChanged;

        GameEvents.OnGameStart += OnGameStart;
        GameEvents.OnBallsSpawning += OnBallsSpawning;

        GameEvents.OnSimTimerCountdown += OnSimTimerCountdown;
        GameEvents.OnSimTimerExpired += OnSimTimerExpired;        // forced throw / ripple activation

        GameEvents.OnCooldownCountdown += OnCooldownCountdown;
        GameEvents.OnCooldownTimerExpired += OnCooldownTimerExpired;

        GameEvents.OnLivesLeftUpdated += OnLivesLeftUpdated;
        GameEvents.OnScoreUpdated += OnScoreUpdated;

        GameEvents.OnNewHiScore += OnNewHiScore;

        restartButton.onClick.AddListener(OnRestartClicked);
    }

    private void OnDisable()
    {
        GameEvents.OnGameStateChanged -= OnGameStateChanged;

        GameEvents.OnGameStart -= OnGameStart;
        GameEvents.OnBallsSpawning -= OnBallsSpawning;

        GameEvents.OnSimTimerCountdown -= OnSimTimerCountdown;
        GameEvents.OnSimTimerExpired -= OnSimTimerExpired;

        GameEvents.OnCooldownCountdown -= OnCooldownCountdown;
        GameEvents.OnCooldownTimerExpired -= OnCooldownTimerExpired;

        GameEvents.OnLivesLeftUpdated -= OnLivesLeftUpdated;
        GameEvents.OnScoreUpdated -= OnScoreUpdated;

        GameEvents.OnNewHiScore -= OnNewHiScore;

        restartButton.onClick.RemoveListener(OnRestartClicked);
    }


    private void Start()
    {
        mainCamera = Camera.main;

        gameOverPanel.gameObject.SetActive(false);
        grabTimer.gameObject.SetActive(false);      // enabled on grab
        gameOverText.gameObject.SetActive(false);
        restartButton.gameObject.SetActive(false);

        hiScoreColour = newHiScoreText.color;
        newLivesColour = livesText.color;

        countdownSlider.Init();

        menuPanel.gameObject.SetActive(gameData.ShowMenu);

        if (!gameData.ShowMenu)
        {
            GameEvents.OnGameStart?.Invoke();   // start immediately (resets gameData.ShowMenu to true)
        }
    }

    private void OnGameStateChanged(GameManager.GameState fromState, GameManager.GameState gameState)
    {
        currentGameState = gameState;

        debugText.text = currentGameState.ToString();

        switch (currentGameState)
        {
            case GameManager.GameState.WaitingToStart:
                break;

            case GameManager.GameState.BallSetup:           
                break;

            case GameManager.GameState.WaitingForPlayer:
                gameStatusText.text = waitForPlayerFeedback;
                break;

            case GameManager.GameState.Projecting:          // countdown slider
                gameStatusText.text = "";                   
                break;

            case GameManager.GameState.WaitingForGame:      // countdown slider
                gameStatusText.text = "";
                break;

            case GameManager.GameState.GameOver:
                GameOver();
                break;
        }
    }

    private void OnBallsSpawning(bool spawning, bool fromGrab)
    {
        if (spawning && !fromGrab)      // ie. initial balls at startup
            gameStatusText.text = spawningFeedback;
    }

    // sim (grab/ripple) timer event handlers

    private void OnSimTimerCountdown(int timeLeft, bool isStart)
    {
        if (isStart)
        {
            if (currentGameState != GameManager.GameState.GameOver)
                gameStatusText.text = "";

            // slider countdown
            countdownPanel.gameObject.SetActive(true);
            countdownSlider.StartCountdown(timeLeft);
        }

        if (timeLeft > 0)
            countdownSlider.Pulse();
    }

    private void OnSimTimerExpired(float suspendTime)
    {
        if (currentGameState != GameManager.GameState.GameOver)
            gameStatusText.text = waitForPlayerFeedback;
    }

    // cooldown timer event handlers

    private void OnCooldownCountdown(int timeLeft, bool isStart)
    {
        if (isStart)
        {
            if (currentGameState != GameManager.GameState.GameOver)
                gameStatusText.text = "";

            // slider countdown
            countdownSlider.StartCountdown(timeLeft);
        }

        if (timeLeft > 0)
            countdownSlider.Pulse();
    }

    private void OnCooldownTimerExpired()
    {
        if (currentGameState != GameManager.GameState.GameOver)
            gameStatusText.text = waitForPlayerFeedback;
    }

    // lives left

    private void OnLivesLeftUpdated(float livesLeft, float added, int livesWarning)
    {
        if (currentGameState == GameManager.GameState.GameOver)
            return;

        livesText.text = ((int)livesLeft).ToString();

        Pulse(livesPanel.transform); 

        if (livesLeft > 0f)      // else game over
        {
            //if (added > 0)
            //    FlashFeedback(feedbackText, newLifeFeedback, newLivesColour);
            //else if (added < 0f && livesLeft == 1f)                      // last life
            //    FlashFeedback(feedbackText, lastLifeFeedback, warningColour);
            if (added < 0f && livesLeft <= livesWarning)           // lost life and below warning threshold
                FlashFeedback(feedbackText, lifeWarningFeedback, warningColour);
        }
    }

    private void OnScoreUpdated(int score, int scoreAdded, BallColours.BallColour ballColour, Vector2 worldPosition)
    {
        scoreText.text = score.ToString();

        if (scoreAdded > 0)
            SpawnBallUI(scoreAdded.ToString(), ballColour, worldPosition);
    }

    private void SpawnBallUI(string text, BallColours.BallColour ballColour, Vector2 worldPosition)
    {
        Vector2 screenPosition = mainCamera.WorldToScreenPoint(worldPosition);

        var ballUI = Instantiate(ballUIPrefab, transform);      // parented to this canvas
        ballUI.transform.position = screenPosition;

        ballUI.SetScoreText(text);
        ballUI.SetScoreColour(gameData.GetBallColour(ballColour));
    }

    private void GameOver()
    {
        gameOverPanel.gameObject.SetActive(true);
        gameOverText.gameObject.SetActive(true);
        restartButton.gameObject.SetActive(true);

        feedbackText.gameObject.SetActive(false);

        countdownPanel.gameObject.SetActive(false);
        countdownSlider.StopCountdown();

        gameStatusText.text = gameOverFeedback;
    }

    private void OnNewHiScore(int hiScore, bool flash)
    {
        hiScoreText.text = hiScore.ToString();

        if (flash)
        {
            newHiScoreFlashesLeft = newHiScoreFlashes;
            FlashNewHiScore();

            gameStatusText.text = newHiScoreFeedback;
        }
    }

    // tween on, wait, tween off 
    private void FlashNewHiScore()
    {
        if (!EndlessFlashes && newHiScoreFlashesLeft <= 0)
            return;

        LeanTween.value(newHiScoreText.gameObject, Color.clear, hiScoreColour, feedbackFlashTime)
                    .setOnStart(() =>
                    {
                        newHiScoreText.color = Color.clear;
                        newHiScoreText.gameObject.SetActive(true);
                    })
                    .setOnUpdate((Color c) =>
                    {
                        newHiScoreText.color = c;
                    })
                    .setOnComplete(() =>        // off after delay
                    {
                        LeanTween.value(newHiScoreText.gameObject, hiScoreColour, Color.clear, feedbackFlashTime)
                                    .setDelay(feedbackFlashOnTime)
                                    .setOnUpdate((Color c) =>
                                    {
                                        newHiScoreText.color = c;
                                    })
                                    .setOnComplete(() =>
                                    {
                                        newHiScoreText.gameObject.SetActive(false);
                                        newHiScoreText.color = hiScoreColour;

                                        if (! EndlessFlashes)
                                            newHiScoreFlashesLeft--;

                                        // repeat?
                                        if (EndlessFlashes || newHiScoreFlashesLeft > 0)
                                            FlashNewHiScore();
                                    });
                    });

    }

    // tween on, wait, off
    private void FlashFeedback(TextMeshProUGUI feedbackText, string feedback, Color feedbackColour)
    {
        var flashColour = new Color(feedbackColour.r, feedbackColour.g, feedbackColour.b, 1f);

        LeanTween.value(feedbackText.gameObject, Color.clear, flashColour, feedbackFlashTime)
                    .setOnStart(() =>
                    {
                        feedbackText.color = Color.clear;
                        feedbackText.text = feedback;
                        feedbackText.gameObject.SetActive(true);
                    })
                    .setOnUpdate((Color c) =>
                    {
                        feedbackText.color = c;
                    })
                    .setOnComplete(() =>        // turn off after delay
                    {
                        LeanTween.value(feedbackText.gameObject, flashColour, Color.clear, feedbackFlashTime)
                                    .setDelay(feedbackFlashOnTime)
                                    .setOnUpdate((Color c) =>
                                    {
                                        feedbackText.color = c;
                                    })
                                    .setOnComplete(() =>
                                    {
                                        feedbackText.gameObject.SetActive(false);
                                    });
                    });

    }

    private void Pulse(Transform scaleObject)
    {
        var startScale = scaleObject.localScale;
        LeanTween.scale(scaleObject.gameObject, startScale * pulseScale, pulseTime)
                            .setEaseOutCubic()
                            .setLoopPingPong(1);
    }

    private void OnGameStart()
    {
        menuPanel.gameObject.SetActive(false);
    }

    private void OnRestartClicked()
    {
        GameEvents.OnRestartGame?.Invoke();
    }
}
