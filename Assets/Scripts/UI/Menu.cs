using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class Menu : MonoBehaviour
{
    public TextMeshProUGUI GameName;
    public TextMeshProUGUI Credit;
    public Button PlayButton;

    public AudioSource audioSource;
    public AudioClip whooshAudio;
    public AudioClip pingAudio;

    private float buttonDelay = 0.25f;
    private float buttonScaleTime = 1f;


    private void OnEnable()
    {
        PlayButton.onClick.AddListener(PlayGame);
    }

    private void OnDisable()
    {
        PlayButton.onClick.RemoveListener(PlayGame);
    }

    private void Start()
    {
        PlayButton.gameObject.SetActive(false);
        Credit.gameObject.SetActive(false);
        ScaleInButton();
    }

    private void ScaleInButton()
    {
        var scale = PlayButton.gameObject.transform.localScale;

        PlayButton.gameObject.transform.localScale = Vector2.zero;
        PlayButton.gameObject.SetActive(true);

        LeanTween.scale(PlayButton.gameObject, scale, buttonScaleTime)
                        .setEaseOutElastic()
                        .setDelay(buttonDelay)
                        .setOnStart(() =>
                        {
                            audioSource.clip = whooshAudio;
                            audioSource.Play();
                        })
                        .setOnComplete(() =>
                        {
                            audioSource.clip = pingAudio;
                            audioSource.Play();
                            Credit.gameObject.SetActive(true);
                        });
    }

    private void PlayGame()
    {
        GameEvents.OnGameStart?.Invoke();
    }
}
