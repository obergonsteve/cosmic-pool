using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

// UI to allow ChordUIs to be placed as chords are played
public class KeyBarsUI : MonoBehaviour
{
    private List<InstrumentRow> instrumentRows = new();

    public void ClearInstrumentRows()
    {
        instrumentRows.Clear();
    }

    public void AddInstrumentRow(InstrumentRow instrumentRow)
    {
        instrumentRows.Add(instrumentRow);
        //Debug.Log($"AddInstrumentRow: {instrumentRow.instrument.instrumentName}");
    }

    public InstrumentRow GetInstrumentRow(Instrument instrument)
    {
        return instrumentRows.FirstOrDefault((row) => row.instrument == instrument);
    }

    public InstrumentRow GetInstrumentRowByColour(Color rowColour)
    {
        // LINQ...
        return instrumentRows.FirstOrDefault((row) => row.RowFullColour == rowColour);

        // long hand..
        //foreach (var row in instrumentRows)
        //{
        //    if (row.RowFullColour == rowColour)
        //        return row;
        //}
        //return null;
    }
}
