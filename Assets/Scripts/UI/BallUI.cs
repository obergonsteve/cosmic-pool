using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


// UI attached to a ball to show the chords 'contained' in it
// Also instantiated by UIController to show the score associated with a ball 'upgrade'

public class BallUI : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI scoreText;

    [SerializeField]
    private TextMeshProUGUI chordText;

    private float floatDistance = 20f;
    private float floatDelay = 0.25f;
    private float floatTime = 1.5f;

    private float pulseFactor = 2f;
    private float pulseTime = 0.35f;
    private bool pulsing;

    private float fadeDelay = 0.5f;
    private float fadeTime = 1.5f;

    private float vanishDelay = 0.5f;
    private float vanishTime = 1.5f;

    private Vector2 chordTextScale;


    private void Awake()
    {
        chordTextScale = chordText.transform.localScale;
    }

    public void SetScoreText(string score)
    {
        scoreText.text = score;
        Pulse(scoreText);
        FloatUp(scoreText);
    }

    public void SetScoreColour(Color colour)
    {
        scoreText.color = colour;
        Fade(scoreText);
        Vanish(scoreText);
    }

    public void ClearChordUIText()
    {
        chordText.text = "";
    }

    public void SetChordUIText(List<Triad> chords, int scoreValue, Color colour)
    {
        var chordString = "";

        foreach (var chord in chords)
        {
            chordString += chord.chordName + " ";
        }

        // scale down for large score balls
        chordText.transform.localScale = chordTextScale / scoreValue;
        chordText.text = chordString;
        chordText.color = colour;

        Pulse(chordText);
    }

    private void FloatUp(TextMeshProUGUI text)
    {
        var startPosition = transform.position;

        LeanTween.moveY(gameObject, startPosition.y + floatDistance, floatTime)
                    .setDelay(floatDelay)
                    .setEaseOutSine();
    }

    private void Pulse(TextMeshProUGUI text)
    {
        if (pulsing)
            return;

        pulsing = true;
        var startScale = transform.localScale;

        LeanTween.scale(gameObject, startScale * pulseFactor, pulseTime)
                    .setEaseOutSine()
                    .setLoopPingPong(1)
                    .setOnComplete(() => pulsing = false);
    }

    private void Fade(TextMeshProUGUI text)
    {
        var startColour = text.color;

        LeanTween.value(gameObject, startColour, Color.clear, fadeTime)
                    .setDelay(fadeDelay)
                    .setEaseOutSine()
                    .setOnUpdate((Color c) => text.color = c)
                    .setOnComplete(() => gameObject.SetActive(false));
    }

    private void Vanish(TextMeshProUGUI text)
    {
        LeanTween.scale(gameObject, Vector2.zero, vanishTime)
                    .setDelay(vanishDelay)
                    .setEaseOutSine();
    }
}
