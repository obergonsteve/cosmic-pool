using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// handles all ball spawning, including upgrades

public class BallSpawner : MonoBehaviour
{
    [SerializeField]
    private GameManager gameManager;

    [SerializeField]
    private BallPool ballPool;

    [SerializeField]
    private BallController ballPrefab;

    [SerializeField]
    private LayerMask hitLayerMask;                  // balls only

    private float raycastRadius;                       // radius of ball collider
    private float raycastMargin = 1.5f;              // for raycast to allow space around existing balls

    private float ballUpgradeScaleFactor = 1.8f;
              
    private int maxBallSize = 3;                    // no more upgrades!  new life!

    public int TotalScoreInPlay { get; private set; }
    public int TotalBallsInPlay { get; private set; }
    public int MaxBallsInPlay = 100;

    // play area
    private Vector2 leftWallPosition;
    private Vector2 rightWallPosition;
    private Vector2 topWallPosition;
    private Vector2 bottomWallPosition;

    private Vector2 RandomSpawnPosition => new Vector2(UnityEngine.Random.Range(leftWallPosition.x, rightWallPosition.x),
                                                        UnityEngine.Random.Range(topWallPosition.y, bottomWallPosition.y));


    private void OnEnable()
    {
        GameEvents.OnWallsPositioned += OnWallsPositioned;
        GameEvents.OnGameStart += OnGameStart;
        GameEvents.OnGrab += OnGrab;
        GameEvents.OnRippleSpawned += OnRippleSpawned;
        GameEvents.OnBallUpgrade += OnBallUpgrade;
        GameEvents.OnGameKeyChanged += OnGameKeyChanged;
    }

    private void OnDisable()
    {
        GameEvents.OnWallsPositioned -= OnWallsPositioned;
        GameEvents.OnGameStart -= OnGameStart;
        GameEvents.OnGrab -= OnGrab;
        GameEvents.OnRippleSpawned -= OnRippleSpawned;
        GameEvents.OnBallUpgrade -= OnBallUpgrade;
        GameEvents.OnGameKeyChanged -= OnGameKeyChanged;
    }

    private void Start()
    {
        raycastRadius = ballPrefab.GetComponent<CircleCollider2D>().radius * ballPrefab.transform.localScale.x * raycastMargin; // ballPrefab.MagnetRadius;
        //Debug.Log($"ballRadius = {ballRadius}");
    }

    // spawn a new upgrade ball at the collision point
    // and vanish the two upgraded balls
    private void OnBallUpgrade(Vector2 collisionPoint, BallController ball1, BallController ball2)
    {
        //UpdateTotalScoreInPlay(-(ball1.ScoreValue + ball2.ScoreValue), -2);     // two balls out of play

        if (ball1.BallSize < maxBallSize)
        {
            // new ball inherits average velocity of 2 upgraded balls, scaled down according to size (mass)
            var combinedForce = (ball1.LastFrameVelocity + ball2.LastFrameVelocity) / (2f * (ball1.BallSize + 1));

            SpawnBall(collisionPoint, ball1.transform.localScale * ballUpgradeScaleFactor,
                        ball1.BallSize + 1, ball1.ScoreValue * 2, ball1.LivesValue,      // TODO: ball1.LivesValue * 2 ??
                        ball1.ballColour, combinedForce, true, false, true, CombineChords(ball1, ball2));
        }
        else
        {
            // gained a life!
            GameEvents.OnGainLife?.Invoke(1, true);    
        }

        ball1.UpgradeVanish(collisionPoint, ballPool.ReturnBallToPool);      // vanish away, return ball to pool on complete
        ball2.UpgradeVanish(collisionPoint, ballPool.ReturnBallToPool);
    }


    private List<Triad> CombineChords(BallController ball1, BallController ball2)
    {
        List<Triad> combinedChordProg = new();

        foreach (Triad chord in ball1.ChordProgression)
        {
            combinedChordProg.Add(chord);
        }

        foreach (Triad chord in ball2.ChordProgression)
        {
            combinedChordProg.Add(chord);
        }

        return combinedChordProg;
    }

    private BallController SpawnRandomBall(Vector2 scale, int scoreValue, float livesValue, BallColours.BallColour ballColour, Vector2 startForce, bool hiLight, bool spawnAudio)
    {
        //Debug.Log($"SpawnRandomBall: scale {scale} scoreValue {scoreValue} livesValue {livesValue} ballColour {ballColour}");
        Vector2 randomPosition;

        if (VacantRandomPosition(out randomPosition))
        {
            var randomBall = SpawnBall(randomPosition, scale, 1, scoreValue, livesValue, ballColour, startForce, false, hiLight, spawnAudio);
            randomBall.HideProjectionGhost(false);      // all existing balls already unhidden OnGrab
            return randomBall;
        }
        return null;
    }

    private IEnumerator SpawnBalls(int numberOfBalls, bool fromGrab)
    {
        GameEvents.OnBallsSpawning?.Invoke(true, fromGrab);       // prevents grab and ripple

        yield return new WaitForSeconds(gameManager.gameData.BallScaleTime);        // initial pause

        int ballsSpawned = 0;
        while (ballsSpawned < numberOfBalls && TotalBallsInPlay < MaxBallsInPlay)
        {
            // SpawnRandomBall returns null if a ball was not spawned successfully (ie. couldn't find a vacant space)
            if (SpawnRandomBall(ballPrefab.transform.localScale, ballPrefab.ScoreValue, ballPrefab.LivesValue,
                         Utility.RandomEnum<BallColours.BallColour>(), Vector2.zero, false, fromGrab) != null) 
            {
                ballsSpawned++;
                yield return new WaitForSeconds(gameManager.gameData.BallScaleTime);
            }
            else
                break;        // couldn't find a vacant random position within a reasonable time.. so quit trying to spawn more
        }

        GameEvents.OnBallsSpawning?.Invoke(false, fromGrab);
    }


    // gets ball from pool and initialises it for play

    private BallController SpawnBall(Vector2 collisionPoint, Vector2 scale, int ballSize, int scoreValue, float livesValue,
                                            BallColours.BallColour ballColour, Vector2 startForce, bool isUpgrade,
                                            bool hilight, bool spawnAudio, List<Triad> chordProg = null)
    {
        BallController newBall = ballPool.GetBall(); // Instantiate(ballPrefab);

        newBall.transform.parent = transform;
        newBall.transform.position = collisionPoint;
        newBall.transform.localScale = scale;
        newBall.transform.rotation = Quaternion.Euler(0f, 0f, UnityEngine.Random.Range(0f, 360f));
        newBall.BallSize = ballSize;
        newBall.ScoreValue = scoreValue;
        newBall.LivesValue = livesValue;
        newBall.HitCount = 0;
        newBall.SetStatus(BallController.BallStatus.InPlay);   
        newBall.SetColour(ballColour, scoreValue > 1, false);       // sprite light on if not base score level
        newBall.Push(startForce, newBall.transform.position);       // newBall 'inherits' combined forces of 2 upgraded balls
        newBall.SpawnedFlash();
        newBall.ActivateMagnet(false);
        newBall.SetToTrigger(false);

        newBall.name = ballColour.ToString().ToUpper() + " " + newBall.BallSize;

        if (! newBall.HasGhost)            // first instantiation (to fill pool) - hook up to simulator and create its ghost (one time only)
            GameEvents.OnGhostBall?.Invoke(newBall);
        else                               // ball from pool - reset ghost size, colour, etc
            newBall.SyncProjectionGhost();       

        // start particles / set audio after ghost has been cloned (don't want on ghost)
        var colour = gameManager.gameData.GetBallColour(ballColour);
        newBall.StartSparkles(colour);
        newBall.StartTrail(colour);

        newBall.HideProjectionGhost(true);

        newBall.SetSpawning(true);                 // can't upgrade or error while spawning

        // assign instrument according to ball colour
        // if not upgrade, assign note and chord from game key, adds to chord progression
        // upgrade ball will inherit chords from upgraded balls
        newBall.SetRandomMusicAudio(isUpgrade);

        if (isUpgrade)
        {
            if (chordProg != null)
                newBall.AddChordProgression(chordProg);

            newBall.UpgradeSpawnAudio();
            newBall.UpgradeSpawnParticles();

            newBall.ActivateMagnet(true);
        }
        else if (spawnAudio)
        {
            newBall.SpawnAudio();
        }

        newBall.SpawnScaleIn(scale, hilight);      // includes ghost - sets status to InPlay
        newBall.SetSpawning(false);

        // kludge: fire game state event after ball spawned
        // so new ball Grabber can enable/disable grabbing until 'waiting for player' state
        // which it might have missed during spawning
        GameEvents.OnGameStateChanged?.Invoke(gameManager.CurrentState, gameManager.CurrentState);

        return newBall;
    }


    //private BallController SpawnBall(Vector2 collisionPoint, Vector2 scale, int ballSize, int scoreValue, float livesValue,
    //                                            BallColours.BallColour ballColour, Vector2 startForce, bool isUpgrade,
    //                                            bool hilight, bool spawnAudio, List<Triad> chordProg = null)
    //{
    //    var newBall = Instantiate(ballPrefab, transform);

    //    newBall.transform.position = collisionPoint;
    //    newBall.transform.localScale = scale;
    //    newBall.transform.rotation = Quaternion.Euler(0f, 0f, UnityEngine.Random.Range(0f, 360f));
    //    newBall.BallSize = ballSize;  
    //    newBall.ScoreValue = scoreValue;
    //    newBall.LivesValue = livesValue;   
    //    newBall.HitCount = 0;
    //    newBall.SetStatus(BallController.BallStatus.InPlay);        // to make sure! 
    //    newBall.SetColour(ballColour, scoreValue > 1, false);       // sprite light on if not base score level
    //    newBall.Push(startForce, newBall.transform.position);       // newBall 'inherits' combined forces of 2 upgraded balls
    //    newBall.SpawnedFlash();
    //    newBall.ActivateMagnet(false);

    //    newBall.name = ballColour.ToString().ToUpper() + " " + newBall.BallSize;

    //    // hook up newBall to simulator and create its ghost
    //    GameEvents.OnBallSpawned?.Invoke(newBall);

    //    // start particles / set audio after ghost has been cloned (don't want on ghost)
    //    var colour = gameManager.gameData.GetBallColour(ballColour);
    //    newBall.StartSparkles(colour);
    //    newBall.StartTrail(colour);

    //    //newBall.CropProjectionGhost();
    //    newBall.HideProjectionGhost(true);

    //    newBall.SetSpawning(true);                 // can't upgrade or error while spawning
    //    newBall.SetRandomMusicAudio();           // assign note and chord from game key, instrument according to ball colour. adds to chord progression

    //    if (isUpgrade)
    //    {
    //        if (chordProg != null)
    //            newBall.AddChordProgression(chordProg);

    //        newBall.UpgradeSpawnAudio();
    //        newBall.UpgradeSpawnParticles();

    //        newBall.ActivateMagnet(true);
    //    }
    //    else if (spawnAudio)
    //    {
    //        newBall.SpawnAudio();
    //    }

    //    UpdateTotalScoreInPlay(newBall.ScoreValue, 1);

    //    newBall.SpawnScaleIn(scale, hilight);      // includes ghost - sets status to InPlay
    //    newBall.SetSpawning(false);

    //    // kludge: fire game state event after ball spawned, so new ball Grabber can enable/disable grabbing until 'waiting for player'
    //    GameEvents.OnGameStateChanged?.Invoke(gameManager.CurrentState, gameManager.CurrentState);

    //    return newBall;
    //}

    // keep raycasting within spawn area until a 'vacant' space (no other ball) is found

    private bool VacantRandomPosition(out Vector2 position)
    {
        Collider2D overlap;
        Vector2 randomPosition;

        int countLimit = 10000;       // in case screen gets full!!

        do
        {
            randomPosition = RandomSpawnPosition;       // anywhere in play area
            overlap = Physics2D.OverlapCircle(randomPosition, raycastRadius, hitLayerMask);
            countLimit--;
        }
        while (countLimit > 0 && overlap != null);      // keep looping until nothing overlapping

        if (countLimit <= 0)
        {
            position = Vector2.zero;
            return false;
        }

        // no raycast hit - use this position!
        position = randomPosition;
        return true;
    }

    private void UpdateTotalScoreInPlay(int scoreAdded, int ballsAdded)
    {
        TotalScoreInPlay += scoreAdded;
        TotalBallsInPlay += ballsAdded;
        //GameEvents.OnTotalScoreInPlay?.Invoke(TotalScoreInPlay, TotalBallsInPlay);
    }

    private void OnWallsPositioned(float cameraHeight, float cameraWidth, Transform leftWall, Transform rightWall, Transform topWall, Transform bottomWall, float wallThickness)
    {
        leftWallPosition = new Vector2(leftWall.position.x + wallThickness, leftWall.position.y);
        rightWallPosition = new Vector2(rightWall.position.x - wallThickness, rightWall.position.y);
        topWallPosition = new Vector2(topWall.position.x, topWall.position.y - wallThickness);
        bottomWallPosition = new Vector2(bottomWall.position.x, bottomWall.position.y + wallThickness);

        leftWallPosition = new Vector2(leftWall.position.x + wallThickness, leftWall.position.y);
        rightWallPosition = new Vector2(rightWall.position.x - wallThickness, rightWall.position.y);
        topWallPosition = new Vector2(topWall.position.x, topWall.position.y - wallThickness);
        bottomWallPosition = new Vector2(bottomWall.position.x, bottomWall.position.y + wallThickness);
        //Debug.Log($"leftWallPosition {leftWallPosition} rightWallPosition {rightWallPosition} topWallPosition {topWallPosition} bottomWallPosition {bottomWallPosition}");
    }

    private void OnGameStart()
    {
        // get the first balls into play...
        StartCoroutine(SpawnBalls(gameManager.gameData.StartBalls, false));
    }

    // spawn balls according to score value of grabbed ball
    private void OnGrab(Projection grabObject, Vector2 grabPosition)
    {
        StartCoroutine(SpawnBalls(grabObject.GetComponent<BallController>().ScoreValue, true));
    }

    // spawning a ripple spawns a ball!
    private void OnRippleSpawned(Ripple newRipple)
    {
        StartCoroutine(SpawnBalls(1, true));
    }

    // spawn a new ball every time key is changed because of player inactivity
    //private void OnChangeGameKey()
    private void OnGameKeyChanged(Key gameKey, bool playScale, bool gameStart)
    {
        //Debug.Log($"OnGameKeyChanged: {gameKey.FullName}");
        if (! gameStart)
            StartCoroutine(SpawnBalls(1, true));
    }

    // spawn area
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawWireCube(Vector3.zero, new Vector3(rightWallPosition.x - leftWallPosition.x, topWallPosition.y - bottomWallPosition.y, 0f));
    }
}
