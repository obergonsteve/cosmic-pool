using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(Projection))]
[RequireComponent(typeof(BallAudio))]
[RequireComponent(typeof(BallParticles))]
[RequireComponent(typeof(Grabber))]
//[RequireComponent(typeof(LightController))]
//[RequireComponent(typeof(SpriteRenderer))]

public class BallController : MonoBehaviour
{
    public GameData gameData;       // SO (colours, etc)

    public int BallSize = 1;      
    public int ScoreValue = 1;              // score gained on 'upgrade' collision
    public float LivesValue = 0.5f;         // lives lost on 'error' collision  -  2 halves make a whole!

    [SerializeField]
    private BallMagnet ballMagnet;

    public bool MagnetActive => ballMagnet.gameObject.activeSelf;

    public List<Triad> ChordProgression => ballAudio.ChordProgression;

    [SerializeField]
    private SpriteRenderer spriteRenderer;
    private float spriteColourAlpha = 1f; //0.5f;     // sprite is semi-transparent ball colour

    public int HitCount = 0;            // by other balls

    public enum BallStatus
    {
        InPlay,         // currently must be first, so that cloned ghost ball is spawned with this status
        Suspended,      // to prevent upgrade/error on ball drag collision, which throws the dragged ball. hopefully enough time for the thrown ball to 'get away'
        Grabbed,
        Error,
        Upgraded
    };

    public BallStatus CurrentStatus { get; private set; }

    public bool InPlay => CurrentStatus == BallStatus.InPlay;
    public bool IsSpawning { get; private set; } = false;            // can't upgrade or error while spawning
    private bool gameOver;

    private int errorFlashes = 6;
    private float errorFlashTime = 0.25f;

    private float spawnFlashTime = 0.5f;

    [SerializeField]
    private float startIntensityFactor = 0.2f;              // eg. first ball slightly visible (sprite light)

    public Vector2 LastFrameVelocity { get; private set; }

    public BallColours.BallColour ballColour;
    public Color Colour => gameData.GetBallColour(ballColour); 

    private Rigidbody2D ballRigidbody;
    private Collider2D ballCollider;
    private BallAudio ballAudio;
    private BallParticles ballParticles;
    private Projection projection;

    [SerializeField]
    private BallUI ballUI;

    [SerializeField]
    private LightController lightController;

    public bool IsGhost => projection != null && projection.IsGhost;
    public bool HasGhost => projection != null && projection.HasGhost;

    // a ball re-activates its collider once it reaches its drag start point
    private Vector2 pushStartPoint;     // ball position when push (eg. throw, ripple, upgrade) was initiated
    private Vector2? pushTargetPoint;   // ball position when drag started - throw will pass through this point - collider re-enabled
    private float distanceToTarget;     // distance to pushTargetPoint if not null
    private float distanceTravelled;    // distance from pushStartPoint to currentPoint

    private float throwSuspendTime = 0.25f;      // if dragged ball hit another ball

    private bool bouncing;
    private float bounceScale = 1.1f;
    private float bounceTime = 0.1f;        // fraction of beatInterval

    private void OnEnable()
    {
        GameEvents.OnGrab += OnGrab;
        GameEvents.OnThrow += OnThrow;

        GameEvents.OnRippleSpawned += OnRippleSpawned;
        GameEvents.OnReleaseRipple += OnReleaseRipple;
        //GameEvents.OnRippleActivated += OnRippleActivated;
        GameEvents.OnBeat += OnBeat;        // TODO!

        GameEvents.OnGameOver += OnGameOver;

        //GameEvents.OnThrowHitGrabPoint += OnThrowHitGrabPoint;
    }

    private void OnDisable()
    {
        GameEvents.OnGrab -= OnGrab;
        GameEvents.OnThrow -= OnThrow;

        GameEvents.OnRippleSpawned -= OnRippleSpawned;
        GameEvents.OnReleaseRipple -= OnReleaseRipple;
        //GameEvents.OnRippleActivated -= OnRippleActivated;

        GameEvents.OnBeat -= OnBeat;        // TODO!

        GameEvents.OnGameOver -= OnGameOver;

        //GameEvents.OnThrowHitGrabPoint -= OnThrowHitGrabPoint;
    }

    private void Awake()
    {
        ballRigidbody = GetComponent<Rigidbody2D>();
        ballCollider = GetComponent<Collider2D>();
        //lightController = GetComponent<LightController>();
        //spriteRenderer = GetComponent<SpriteRenderer>();
        projection = GetComponent<Projection>();
        ballAudio = GetComponent<BallAudio>();
        ballParticles = GetComponent<BallParticles>();
    }

    private void Start()
    {
        InitColour(); 
    }

    private void FixedUpdate()
    {
        // limit velocity
        ballRigidbody.velocity = Vector2.ClampMagnitude(ballRigidbody.velocity, gameData.MaxBallVelocity);
        LastFrameVelocity = ballRigidbody.velocity;

        //if (pushTargetPoint != null)        // eg. if push from drag / throw.  false if eg. from ripple or upgrade
        //{
        //    distanceTravelled = Vector2.Distance(transform.position, (Vector2)pushStartPoint);

        //    if (distanceTravelled >= distanceToTarget)      // reached or overshot target point
        //    {
        //        ballCollider.isTrigger = false;     // enable ball collider
        //        lightController.ActivateSpriteLight(true);

        //        ballAudio.TargetAudio();
        //        //GameEvents.OnThrowHitGrabPoint?.Invoke(this);

        //        pushTargetPoint = null;     // stop checking for target point
        //        distanceToTarget = 0f;
        //        distanceTravelled = 0f;
        //    }
        //}
    }

    // collisions detected for real and ghost balls
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (gameOver)
            return;

        if (collision.gameObject.CompareTag("Ball"))
        {
            // if dragging this ball, throw as soon as it hits another ball
            if (CurrentStatus == BallStatus.Grabbed)
            {
                GameEvents.OnSimTimerExpired?.Invoke(throwSuspendTime);         // throw ball instantly, but set status to Suspended to prevent upgrade, error, etc.
                return;
            }

            var hitProjection = collision.gameObject.GetComponent<Projection>();
            var hitBall = collision.gameObject.GetComponent<BallController>();

            if (CurrentStatus != BallStatus.InPlay || hitBall.CurrentStatus != BallStatus.InPlay)       // eg. error, upgraded - to prevent upgrade twice!
            {
                //Debug.Log($"OnCollisionEnter2D: {name} {CurrentStatus} / {hitBall.name} {hitBall.CurrentStatus}");
                return;
            }

            if (hitProjection != null && hitProjection.IsGhost == projection.IsGhost)        // ie. in same scene
            {
                if (IsSpawning || hitBall.IsSpawning)       //no upgrade / error while spawning
                    return;

                //AddHit(true);       // count hit, activate sprite light and play hit audio

                // lives & points gained / lost
                bool sameSize = BallSize == hitBall.BallSize;
                bool sameColour = ballColour == hitBall.ballColour;

                // error or upgrade on collision of balls of same score size
                if (sameSize)
                {
                    if (sameColour)     // status of this and hitBall will be changed by upgrade, so handle both together here
                    {
                        if (!projection.IsGhost)
                        {
                            GameEvents.OnAddScore?.Invoke(hitBall.ScoreValue + ScoreValue, ballColour, collision.contacts[0].point);
                            GameEvents.OnBallUpgrade?.Invoke(collision.contacts[0].point, this, hitBall);    // new upgrade ball spawned if same size and same colour
                            ballAudio.UpgradeAudio();
                        }
                        else
                        {
                            projection.SetLineColour(gameData.UpgradeColour, false);
                            projection.Stop();      // cuts off further simulation
                        }
                    }
                    else            // lives & status of each ball handled individually
                    {
                        if (!projection.IsGhost)
                        {
                            GameEvents.OnLoseLife?.Invoke(hitBall.LivesValue);
                            GameEvents.OnBallError?.Invoke(hitBall);        // eg. hide all ghost projections
                            ErrorFlash();
                            ballAudio.ErrorAudio();
                        }
                        else
                        {
                            projection.SetLineColour(gameData.ErrorColour, false);
                            projection.Stop();          // cuts off further simulation
                        }
                    }
                }

                AddHit(true);       // count hit, activate sprite light and play hit audio
            }
        }
        else if (collision.gameObject.CompareTag("Wall"))
        {
            // if dragging this ball, throw as soon as it hits a wall
            if (CurrentStatus == BallStatus.Grabbed)
            {
                GameEvents.OnSimTimerExpired?.Invoke(0f);         // throw ball instantly
                return;
            }
        }
    }

    public void AddHit(bool addHitCount)
    {
        if (addHitCount)
            HitCount++;

        ActivateSpriteLight();
        PlayHitNote();         // play music last of all
    }

    public void ActivateSpriteLight()
    {
        if (CurrentStatus != BallStatus.InPlay)
            return;

        lightController.ActivateSpriteLight(true, 0f);
        HideProjectionGhost(true);
    }

    public void PlayHitNote()
    {
        if (CurrentStatus != BallStatus.InPlay)
            return;

        if (!projection.IsGhost)
            ballAudio.PlayMusicAudio(false, false);  
    }

    public void AddChordProgression(List<Triad> chordProgression)
    {
        ballAudio.AddChordProgression(chordProgression);

        //foreach (var triad in chordProgression)
        //{
        //    ballAudio.AddToChordProgression(triad);
        //}
    }

    // re-enable all ghosts whenever any ball is grabbed
    private void OnGrab(Projection grabObject, Vector2 grabPosition)
    {
        Stop();

        //if (CurrentStatus == BallStatus.Spawning || CurrentStatus == BallStatus.InPlay)
        if (CurrentStatus != BallStatus.Upgraded)
        {
            HideProjectionGhost(false);         // ie. enable
        }
    }

    // enable ghost when a new ripple is spawned
    private void OnRippleSpawned(Ripple newRipple)
    {
        Stop();

        //if (CurrentStatus == BallStatus.Spawning || CurrentStatus == BallStatus.InPlay)
        if (CurrentStatus != BallStatus.Upgraded)
        {
            HideProjectionGhost(false);         // ie. enable
        }
    }

    // hide all ghosts whenever any ball is thown
    private void OnThrow(Grabber grabObject, Vector3 throwForce, bool catapult)
    {
        HideProjectionGhost(true); 
    }

    private void OnReleaseRipple(Vector2 position)
    {
        HideProjectionGhost(true);
    }

    //private void OnRippleActivated()
    //{
    //    HideProjectionGhost(true);
    //}

    public void HideProjectionGhost(bool hide)
    {
        if (CurrentStatus == BallStatus.Upgraded && !hide)      // can't enable an upgraded ball!
            return;

        //Debug.Log($"HideProjectionGhost {name}: {hide}");
        projection.HideGhost(hide);
    }

    // sync ghost ball size, colour
    // for ball retrieved from pool
    public void SyncProjectionGhost()
    {
        projection.SyncBallGhost(transform.localScale, BallSize, ballColour, name);
    }

    public void SetStatus(BallStatus newStatus)
    {
        if (CurrentStatus == newStatus)
            return;

        //var oldStatus = CurrentStatus;
        CurrentStatus = newStatus;

        //GameEvents.OnBallStatusChanged?.Invoke(this, oldStatus, CurrentStatus);
    }

    public void SetSpawning(bool spawning)
    {
        IsSpawning = spawning;
    }

    public void ActivateMagnet(bool activate)
    {
        ballMagnet.gameObject.SetActive(activate);
        projection.ActivateGhostMagnet(activate);

        if (activate)
            ballMagnet.PulseSpotLight();
        else
            ballMagnet.StopPulseSpotLight();
    }

    public void SetToTrigger(bool trigger)
    {
        ballCollider.isTrigger = trigger;
    }

    // vanish away
    public void UpgradeVanish(Vector2 vanishPoint, Action<BallController> onComplete)
    {
        SetStatus(BallStatus.Upgraded);

        SetToTrigger(true);
        projection.Stop();
        HideProjectionGhost(true);

        ballParticles.StopSparkles();
        ballParticles.StopTrail();

        ballMagnet.gameObject.SetActive(false);

        ShrinkAway(vanishPoint, onComplete);        // pool ball on complete
    }

    public void Push(Vector2 force, Vector2 startPosition, Vector2? targetPosition = null)
    {
        //Debug.Log($"SetForce: {force}");
        pushStartPoint = startPosition;
        pushTargetPoint = targetPosition;

        // set distance to target if a target point provided
        if (pushTargetPoint != null)
        {
            distanceToTarget = Vector2.Distance(pushStartPoint, (Vector2)pushTargetPoint);
            distanceTravelled = 0f;
        }

        force = Vector2.ClampMagnitude(force, gameData.MaxBallVelocity);
        ballRigidbody.AddForce(force, ForceMode2D.Impulse);
    }

    public void Stop()
    {
        ballRigidbody.velocity = Vector3.zero;
        ballRigidbody.angularVelocity = 0f;
    }

    private void ShrinkAway(Vector3 vanishPoint, Action<BallController> onComplete)
    {
        LeanTween.scale(gameObject, Vector2.zero, gameData.BallVanishTime)
                    .setEaseOutCubic()
                    .setOnComplete(() => onComplete.Invoke(this));

        lightController.FadePointLight(gameData.BallScaleTime);

        LeanTween.move(gameObject, vanishPoint, gameData.BallVanishTime)
                    .setEaseOutCubic();
    }

    private void InitColour()
    {
        SetColour(ballColour, false, false);
    }

    public void SetColour(BallColours.BallColour colour, bool activateSpriteLight, bool activatePointLight)
    {
        ballColour = colour;
        SetColour(gameData.GetBallColour(ballColour));

        if (activateSpriteLight)
            lightController.ActivateSpriteLight(true, 0f);

        if (activatePointLight)
            lightController.ActivatePointLight(true);
    }

    private void SetColour(Color colour)
    {
        var spriteColour = new Color(colour.r, colour.g, colour.b, spriteColourAlpha);
        spriteRenderer.color = spriteColour;

        lightController.SetColour(colour);
        projection.SetLineColour(colour, true);

        ballMagnet.InitLights(colour, BallSize);
    }

    public void InitIntensity(float intensityFactor)
    {
        lightController.SetSpriteLightIntensity(intensityFactor);
    }

    private void ErrorFlash()
    {
        if (CurrentStatus == BallStatus.Error) 
            return;

        LeanTween.value(gameObject, spriteRenderer.color, gameData.ErrorColour, errorFlashTime)
                            .setOnStart(() =>
                            {
                                CurrentStatus = BallStatus.Error;
                                lightController.ActivatePointLight(true);
                            })
                            .setOnUpdate((Color c) => SetColour(c))
                            .setLoopPingPong(errorFlashes)
                            .setOnComplete(() =>
                            {
                                CurrentStatus = BallStatus.InPlay;
                                lightController.ActivatePointLight(false);
                            });
    }

    public void SpawnedFlash()
    {
        lightController.PulseSpawnLight(spawnFlashTime, BallSize);
    }

    public void Grabbed()
    {
        CurrentStatus = BallStatus.Grabbed;
        //ballCollider.isTrigger = true;        // TODO: remove this?  ball now instantly thrown on collision with another ball

        ActivateMagnet(true);

        ballAudio.GrabAudio();
        ballParticles.GrabParticles();
    }

    public void Thrown(float suspendTime)
    {
        if (suspendTime > 0f)
            StartCoroutine(SuspendPlayStatus(suspendTime));
        else
            CurrentStatus = BallStatus.InPlay;

        ballAudio.ThrowAudio();
        ballParticles.ThrowParticles();

        lightController.ActivateSpriteLight(false);
    }

    private IEnumerator SuspendPlayStatus(float time)
    {
        CurrentStatus = BallStatus.Suspended;
        yield return new WaitForSeconds(time);
        CurrentStatus = BallStatus.InPlay;
    }

    // enable collider once ball hits grab position
    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    if (collision.gameObject.CompareTag("GrabPoint"))
    //    {
    //        DisableGrabPoint(collision.gameObject);
    //        ballCollider.isTrigger = false;     // enable ball collider

    //        GameEvents.OnThrowHitGrabPoint?.Invoke(this);
    //    }
    //}


    //private void DisableGrabPoint(GameObject grabPoint)
    //{
    //    if (grabPoint.CompareTag("GrabPoint"))
    //    {
    //        grabPoint.SetActive(false);
    //        //Debug.Log($"Ball {name} hit {grabPoint.name}!");
    //    }
    //}

    // when spawned
    public void SpawnScaleIn(Vector2 toScale, bool hiLight)
    {
        transform.localScale = Vector2.zero;
        projection.HideGhostSprite(true);

        LeanTween.scale(gameObject, toScale, gameData.BallScaleTime)
                    .setEaseOutElastic()
                    .setOnComplete(() =>
                    {
                        projection.HideGhostSprite(false);

                        SetStatus(BallStatus.InPlay);
                        //SetSpawning(false);

                        if (hiLight)
                            InitIntensity(startIntensityFactor);
                    });
    }

    private void OnGameOver(int finalScore)
    {
        gameOver = true;

        lightController.ActivateSpriteLight(false);
        lightController.ActivatePointLight(false);

        ballParticles.StopSparkles();
        ballParticles.StopTrail();

        ActivateMagnet(false);
    }

    public void UpgradeSpawnAudio()
    {
        if (!IsGhost)
            ballAudio.UpgradeSpawnAudio();
    }

    public void SpawnAudio()
    {
        if (!IsGhost)
            ballAudio.SpawnAudio();
    }

    public void SetRandomMusicAudio(bool instrumentOnly)
    {
        if (!IsGhost)
            StartCoroutine(ballAudio.SetRandomMusic(instrumentOnly));   // waits while playing current chord prog
    }

    public void UpgradeSpawnParticles()
    {
        if (!IsGhost)
            ballParticles.UpgradeParticles();
    }

    public void StartSparkles(Color colour)
    {
        if (!IsGhost)
            ballParticles.PlaySparkles(colour);
    }

    public void StartTrail(Color colour)
    {
        if (!IsGhost)
            ballParticles.PlayTrail(colour);
    }

    public void ClearChordUIText()
    {
        if (! IsGhost)
            ballUI.ClearChordUIText();
    }

    public void SetChordUIText(List<Triad> chords)
    {
        if (!IsGhost)
            ballUI.SetChordUIText(chords, ScoreValue, gameData.GetBallColour(ballColour));
    }

    private void OnBeat(BeatData beatData)
    {
        //if (!IsGhost)
        //    Bounce(beatData.BeatInterval);
    }

    private void Bounce(float beatInterval)
    {
        if (bouncing)
            return;

        bouncing = true;

        LeanTween.scale(spriteRenderer.gameObject, spriteRenderer.transform.localScale * bounceScale, bounceTime * beatInterval * ScoreValue)
                    .setEaseOutBack()
                    .setLoopPingPong(1);

        LeanTween.scale(lightController.gameObject, lightController.transform.localScale * bounceScale, bounceTime * beatInterval * ScoreValue)
                    .setEaseOutBack()
                    .setLoopPingPong(1)
                    .setOnComplete(() => bouncing = false );
    }
}
