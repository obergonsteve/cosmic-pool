using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;


[RequireComponent(typeof(CircleCollider2D))]      // trigger

// attached to a ball to give a magnetic field effect (balls only)
public class BallMagnet : MonoBehaviour
{
    [SerializeField]
    private float magnetForce = 1f;         // negative to attract

    [SerializeField]
    private Light2D spotLight;

    [SerializeField]
    private Light2D spriteLight;

    [SerializeField]
    private float lightAlpha = 1f; //0.75f;               // alpha of ball colour reduced for lights

    private float lightPulseTime = 1f;              // half cycle
    private float lightPulseLow = 0.4f;             // % of start intensity for pulsing
    private int lightPulseTweenId;                  // so pulse tween can be cancelled

    private float spotLightInnerRadius = 0.25f;             // scaled up with ball size
    private float spotLightOuterRadius = 0.5f;             // scaled up with ball size

    //public float Radius => transform.localScale.x * GetComponent<CircleCollider2D>().radius;

    private BallController parentBall;      // magnet is a child of BallController

     
    private void Awake()
    {
        parentBall = transform.parent.GetComponent<BallController>();
    }

    public void InitLights(Color colour, int ballSize)
    {
        var magnetColour = new Color(colour.r, colour.g, colour.b, lightAlpha);
        spotLight.color = magnetColour;
        spriteLight.color = magnetColour;

        spotLight.pointLightInnerRadius = spotLightInnerRadius * ballSize;
        spotLight.pointLightOuterRadius = spotLightOuterRadius * ballSize;
    }

    // ball magnets affected by ball colours (opposites repel, same colours attract)
    // encourages chain reactions from sequential upgrades
    // same size only
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Ball"))
        {
            BallController hitBall = collision.gameObject.GetComponent<BallController>();

            if (hitBall == null || !hitBall.MagnetActive)
                return;

            if (hitBall.InPlay && parentBall.InPlay)
            {
                bool sameSize = parentBall.BallSize == hitBall.BallSize;
                if (sameSize)                           // otherwise balls of same colour and size would stick to each other...
                {
                    bool sameColour = parentBall.ballColour == hitBall.ballColour;                          // opposites repel...

                    Vector2 direction = hitBall.transform.position - transform.position;                    // larger if further away
                    float magnitude = 1f / direction.sqrMagnitude;                                          // invert so magnitude gets higher as distance gets smaller
                    float pushForce = (sameColour ? -magnetForce : magnetForce) * parentBall.BallSize;      // ...so same colours attract

                    hitBall.Push(magnitude * pushForce * direction.normalized, hitBall.transform.position);
                    //Debug.Log($"Magnet push on {hitBall.name} = {direction.normalized * magnitude * pushForce}");
                }
            }
        }
    }

    public void PulseSpotLight()
    {
        var highIntensity = spotLight.intensity;
        var lowIntensity = highIntensity * lightPulseLow;
        spotLight.intensity = lowIntensity;

        lightPulseTweenId = LeanTween.value(spotLight.gameObject, lowIntensity, highIntensity, lightPulseTime)
                                        .setOnUpdate((float intensity) => spotLight.intensity = intensity)      // 'anonymous function'
                                        //.setOnUpdate(SetIntensity)                                            // named function
                                        .setEaseInOutQuad()
                                        .setLoopPingPong().id;
    }

    // longhand function for tween
    //private void SetIntensity(float intensity)
    //{
    //    spotLight.intensity = f;
    //}

    public void StopPulseSpotLight()
    {
        LeanTween.cancel(lightPulseTweenId);
    }
}
