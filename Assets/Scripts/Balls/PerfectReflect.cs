﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]

// reflect (ball) at exactly the collision velocity (unless below minVelocity) and angle
public class PerfectReflect : MonoBehaviour
{
    [SerializeField]
    private LayerMask reflectLayers; 

    [SerializeField]
    private float minVelocity = 4f;     // minimum reflection velocity (eg. pinball boost)

    [SerializeField]
    private bool doReflect = true;      // so can be easily turned off

    private Vector2 lastFrameVelocity;
    private Rigidbody2D rb;


    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        // save velocity so it can be maintained on reflection
        if (doReflect)
            lastFrameVelocity = rb.velocity;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (doReflect)
            Reflect(collision);
    }


    private void Reflect(Collision2D collision)
    {
        int hitLayerMask = 1 << collision.gameObject.layer;
        bool hitReflectLayer = ((reflectLayers & hitLayerMask) != 0);

        if (!hitReflectLayer)       // not a perfect reflection layer, so allow physics to take its natural course
            return;

        // force reflection velocity to lastFrameVelocity.magnitude
        Vector2 collisionNormal = collision.contacts[0].normal;     // perpendicular to reflect surface
        Vector2 reflectDirection = Vector2.Reflect(lastFrameVelocity.normalized, collisionNormal);  // in direction, in normal
        rb.velocity = reflectDirection * Mathf.Max(lastFrameVelocity.magnitude, minVelocity);

        //Debug.Log($"Reflect: {name} from {collision.transform.name} reflect velocity {rb.velocity} lastFrameVelocity {lastFrameVelocity}");
    }
}
