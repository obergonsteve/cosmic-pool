using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]         // sfx - eg. error, upgrade, etc. 
[RequireComponent(typeof(BallController))]      // to check if ghost (no audio)

public class BallAudio : MonoBehaviour
{
    [SerializeField]
    private InstrumentKeys instrumentKeys;  // used to assign random instrument and note from game key to this ball when spawned

    [SerializeField]
    private AudioClip spawnClip;
    [SerializeField]
    private AudioClip upgradeSpawnClip;
    [SerializeField]
    private AudioClip grabClip;
    [SerializeField]
    private AudioClip throwClip;

    public AudioClip MusicClip { get; private set; }          // randomly set on spawn. played by notePlayer
    public List<Triad> ChordProgression { get; private set; } = new(); // randomly set on spawn. played by notePlayer

    [SerializeField]
    private AudioClip upgradeClip;
    [SerializeField]
    private AudioClip errorClip;

    private AudioClip targetClip;       // thrown ball hit grab point - collider re-activated

    [SerializeField]
    private AudioSource audioSource = null;      // sfx - eg. spawn, error, upgrade, etc.

    [SerializeField]
    private NotePlayer notePlayer;              // on spawn and hitting other balls. keeps chords etc. clean, also quieter

    private BallController ballController;      // required

    private Instrument currentInstrument;       // set according to ball colour


    private void OnEnable()
    {
        GameEvents.OnBallColourInstrumentsChanged += OnBallColourInstrumentsChanged;      // fired after OnGameKeyChanged
    }

    private void OnDisable()
    {
        GameEvents.OnBallColourInstrumentsChanged -= OnBallColourInstrumentsChanged;
    }

    private void Awake()
    {
        ballController = GetComponent<BallController>();
    }


    // fired after OnGameKeyChanged
    private void OnBallColourInstrumentsChanged()
    {
        if (ballController.IsGhost)
            return;

        StartCoroutine(SetRandomMusic(false));       // set a random note and chord for the instrument assigned to this ball's colour. waits while playing current chord prog
    }

    public IEnumerator SetRandomMusic(bool instrumentOnly)
    {
        if (ballController.IsGhost)
            yield break;

        if (notePlayer.PlayingChordProg)
            notePlayer.StopPlaying();

        //while (notePlayer.PlayingChordProg)
        //{
        //    yield return null;
        //}

        SetRandomNoteAndChord(instrumentOnly);
    }

    // ball plays note in current key on spawn, hit, etc.
    // instrument set according to colour
    // and note set at random once when ball spawned or after key change
    private void SetRandomNoteAndChord(bool instrumentOnly)
    {
        if (ballController.IsGhost)
            return;

        // clear out existing chords and note in case of key change
        ChordProgression.Clear();
        MusicClip = null;

        ballController.ClearChordUIText();

        var ballColour = ballController.ballColour;

        // lookup instrument for this ball colour
        currentInstrument = instrumentKeys.GetColourInstrument(ballColour);
        //Debug.Log($"SetRandomNoteAndChord: {ballController.name} currentInstrument = {currentInstrument.instrumentName}");

        if (!instrumentOnly && currentInstrument != null)
        {
            // ball's music clip and chord root note are same note in the game key
            int randomNoteIndex;
            MusicClip = instrumentKeys.RandomInstrumentNote(currentInstrument, out randomNoteIndex, out _);

            Triad instrumentChord = instrumentKeys.InstrumentChord(currentInstrument, randomNoteIndex + 1);
            AddToChordProgression(instrumentChord);
        }
    }

    // musical sounds

    public void PlayMusicAudio(bool playChordProg, bool countChords)
    {
        if (ballController.IsGhost)
            return;

        if (playChordProg)
            notePlayer.PlayChords(ChordProgression, currentInstrument, false, ballController, countChords);
        else
            notePlayer.PlayClip(MusicClip);
    }

    // added when ball is an 'upgrade' - inherits upgraded balls' chords
    private void AddToChordProgression(Triad chord)
    {
        if (ballController.IsGhost)
            return;

        ChordProgression.Add(chord);
        ballController.SetChordUIText(ChordProgression);
    }

    public void AddChordProgression(List<Triad> chordProgression)
    {
        if (ballController.IsGhost)
            return;

        foreach (var triad in chordProgression)
        {
            AddToChordProgression(triad);
        }
    }

    // sfx

    public void SpawnAudio()
    {
        PlayMusicAudio(true, false);
        //PlayAudio(spawnClip);       // sfx?
    }

    public void UpgradeAudio()
    {
        PlayAudio(upgradeClip);
    }

    public void GrabAudio()
    {
        PlayAudio(grabClip);
    }

    public void ThrowAudio()
    {
        PlayAudio(throwClip);
    }

    public void UpgradeSpawnAudio()
    {
        PlayMusicAudio(true, true);
    }

    public void ErrorAudio()
    {
        PlayAudio(errorClip);
    }

    public void TargetAudio()
    {
        PlayAudio(targetClip);
    }


    private void PlayAudio(AudioClip clip)
    {
        if (ballController.IsGhost)
            return;

        if (clip == null)
            return;

        audioSource.clip = clip;
        audioSource.Play();
    }
}
