using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

// uses Unity 2021+ new object pooling system
public class BallPool : MonoBehaviour
{
    [SerializeField] BallController ballPrefab;
    [SerializeField] int MaxBallsInPlay = 100;
    [SerializeField] int MaxBalls = 200;

    private ObjectPool<BallController> ballPool;


    private void Start()
    {
        InitPool();
    }

    #region long version

    // long hand version
    public void InitPool()
    {
        // create and fill pool
        ballPool = new ObjectPool<BallController>(
            CreatePooledBall,       // instantiate prefab
            OnTakeFromPool,         // enable pooled ball
            OnReturnedToPool,       // disable pooled ball
            OnDestroyPoolObject,    // destroy function called if pool is already full or when pool is destroyed
            false,                  // collection check. if true, Unity prevents pooling problems if an object should be returned to the pool multiple times
            MaxBallsInPlay,         // default capacity
            MaxBalls                // max capacity
        );
    }

    private BallController CreatePooledBall()
    {
        return Instantiate(ballPrefab);
    }

    private void OnTakeFromPool(BallController ball)
    {
        ball.gameObject.SetActive(true);
    }

    private void OnReturnedToPool(BallController ball)
    {
        ball.gameObject.SetActive(false);
    }

    private void OnDestroyPoolObject(BallController ball)
    {
        Destroy(ball.gameObject);
    }

    #endregion

    #region short version

    // short hand version

    //public void InitPool()
    //{
    //    // create and fill pool
    //    ballPool = new ObjectPool<BallController>(
    //        () =>         // create function
    //        {
    //            return Instantiate(ballPrefab);
    //        },
    //        // get function returns a BallController - simply enable
    //        ball =>
    //        {
    //            ball.gameObject.SetActive(true);
    //        },
    //        // release function returns a BallController - simply disable
    //        ball =>
    //        {
    //            ball.gameObject.SetActive(false);
    //        },
    //        // destroy function called if pool is already full
    //        ball =>
    //        {
    //            Destroy(ball.gameObject);
    //        },
    //        false,                                               // collection check. if true, Unity prevents pooling problems if an object should be returned to the pool multiple times
    //        MaxBallsInPlay,                                      // default capacity
    //        MaxBalls                                             // max capacity
    //    );
    //}

    #endregion

    public BallController GetBall()
    {
        return ballPool.Get();
    }

    public void ReturnBallToPool(BallController ball)
    {
        ballPool.Release(ball);
    }
}
