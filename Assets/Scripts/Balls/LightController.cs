using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;



public class LightController : MonoBehaviour
{
    [SerializeField]
    private Light2D pointLight;

    [SerializeField]
    private Light2D spriteLight;

    [SerializeField]
    private Light2D spawnLight;         // point light

    //[SerializeField]
    private float lightColourAlpha = 1f; //0.75f;     // lights are semi-transparent ball colour
    private Color lightColour;

    [SerializeField]
    private float ghostSpriteIntensityFactor = 0.25f;

    [SerializeField]
    private float ghostPointIntensityFactor = 1.25f;

    //private float loopLightPulseTime = 2f;
    //private float singleLightPulseTime = 0.7f;
    //private float singleLightPulseRadius = 1.5f;      // factor

    //private bool loopPulsing = false;               // looping (eg. after being activated)
    //private bool singlePulsing = false;             // on collision with other ball

    private float spriteLightIntensity;         // at start

    private float pointLightIntensity;          // at start
    private float pointLightRadius;             // at start

    private float spawnLightIntensity;          // at start
    private float spawnLightRadius;             // at start



    private bool pulsingSpawnLight;

    private void Awake()
    {
        spriteLightIntensity = spriteLight.intensity;
        pointLightIntensity = pointLight.intensity;
        pointLightRadius = pointLight.pointLightOuterRadius;
        spawnLightIntensity = spawnLight.intensity;
        spawnLightRadius = spawnLight.pointLightOuterRadius;
    }


    public void FadePointLight(float shrinkTime)
    {
        //float startIntensity = pointLight.intensity;

        LeanTween.value(pointLight.gameObject, pointLightIntensity, 0f, shrinkTime)
                    .setEaseOutCubic()
                    .setOnStart(() =>
                    {
                        ActivatePointLight(true);
                    })
                    .setOnUpdate((float f) =>
                    {
                        pointLight.intensity = f;
                    })
                    .setOnComplete(() =>
                    {
                        // restore
                        ActivatePointLight(false);
                        pointLight.intensity = pointLightIntensity;
                    });
    }

    public void PulseSpawnLight(float pulseTime, int scoreValue)
    {
        if (pulsingSpawnLight)
            return;

        pulsingSpawnLight = true;

        //float startIntensity = spawnLight.intensity;
        spawnLight.intensity = 0f;
        spawnLight.pointLightOuterRadius = spawnLightRadius * scoreValue;

        LeanTween.value(spawnLight.gameObject, 0f, spawnLightIntensity, pulseTime)
                    .setEaseOutSine()
                    .setOnStart(() =>
                    {
                        ActivateSpawnLight(true);
                    })
                    .setOnUpdate((float f) =>
                    {
                        spawnLight.intensity = f;
                    })
                    .setOnComplete(() =>
                    {
                        // restore
                        LeanTween.value(spawnLight.gameObject, spawnLightIntensity, 0f, pulseTime)
                                    .setEaseInSine()
                                    .setOnUpdate((float f) =>
                                    {
                                        spawnLight.intensity = f;
                                    })
                                    .setOnComplete(() =>
                                    {
                                        ActivateSpawnLight(false);
                                        spawnLight.intensity = spawnLightIntensity;
                                        spawnLight.pointLightOuterRadius = spawnLightRadius;

                                        pulsingSpawnLight = false;
                                    });
                    });
    }


    public void ActivatePointLight(bool activate)
    {
        pointLight.gameObject.SetActive(activate);
    }

    public void ActivateSpawnLight(bool activate)
    {
        spawnLight.gameObject.SetActive(activate);
    }

    public void ActivateSpriteLight(bool activate, float factor = 1f)
    {
        if (factor > 0)
            spriteLight.intensity = spriteLightIntensity * factor;

        spriteLight.gameObject.SetActive(activate);
    }

    public void SetSpriteLightIntensity(float intensityFactor)
    {
        spriteLight.intensity = spriteLightIntensity * intensityFactor;
        ActivateSpriteLight(true);
    }

    // TODO: HACK
    public void InitGhostLights()
    {
        //ActivateSpriteLight(true, 0.25f);
        spriteLight.intensity = spriteLightIntensity * ghostSpriteIntensityFactor;

        pointLight.pointLightOuterRadius = pointLightRadius * 2f;
        pointLight.intensity = pointLightIntensity * ghostPointIntensityFactor;
    }

    public void SetColour(Color ballColour)
    {
        lightColour = new Color(ballColour.r, ballColour.g, ballColour.b, lightColourAlpha);

        pointLight.color = lightColour;
        spriteLight.color = lightColour;
    }

    //public void LoopPulseSpriteLight()
    //{
    //    if (loopPulsing)        // ie. 'activated'
    //        return;

    //    loopPulsing = true;
    //    spriteLight.intensity = 0f;
    //    spriteLight.gameObject.SetActive(true);

    //    LeanTween.value(spriteLight.gameObject, 0f, spriteLightIntensity, loopLightPulseTime)
    //                        .setOnUpdate((float f) => { spriteLight.intensity = f; })
    //                        .setEaseInOutSine()
    //                        .setLoopPingPong();
    //}

    //public void SinglePulsePointLight()
    //{
    //    if (singlePulsing || loopPulsing)
    //        return;

    //    singlePulsing = true;
    //    pointLight.intensity = 0f;
    //    pointLight.gameObject.SetActive(true);

    //    LeanTween.value(pointLight.gameObject, 0f, pointLightIntensity, singleLightPulseTime)
    //                        .setOnUpdate((float f) => { pointLight.intensity = f; })
    //                        .setEaseInOutSine()
    //                        .setLoopPingPong(1)
    //                        .setOnComplete(() => { singlePulsing = false; });

    //    LeanTween.value(pointLight.gameObject, pointLightRadius, pointLightRadius * singleLightPulseRadius, singleLightPulseTime)
    //                .setOnUpdate((float f) => { pointLight.pointLightOuterRadius = f; })
    //                .setEaseInOutSine()
    //                .setLoopPingPong(1)
    //                .setOnComplete(() => { singlePulsing = false; });
    //}

}
