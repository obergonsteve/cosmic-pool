using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BallColours
{
    public enum BallColour
    {
        Yellow = 0,
        Orange = 1,
        Cyan = 2,
        Pink = 3,
        Purple = 4,
        Green = 5,
    };
}
