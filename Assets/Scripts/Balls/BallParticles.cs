using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallParticles : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem grabParticles;       // looping
    [SerializeField]
    private ParticleSystem throwParticles;
    [SerializeField]
    private ParticleSystem upgradeParticles;
    [SerializeField]
    private ParticleSystem sparkles;
    [SerializeField]
    private ParticleSystem trailParticles;


    public void GrabParticles()
    {
        grabParticles.Play();
    }

    public void ThrowParticles()
    {
        grabParticles.Stop();
        throwParticles.Play();
    }

    public void UpgradeParticles()
    {
        grabParticles.Stop();
        upgradeParticles.Play();
    }

    public void PlaySparkles(Color colour)
    {
        var sparklesMain = sparkles.main;
        sparklesMain.startColor = colour;
        sparkles.Play();
    }

    public void PlayTrail(Color colour)
    {
        var trailMain = trailParticles.main;
        trailMain.startColor = colour;
        trailParticles.Play();
    }

    public void StopSparkles()
    {
        sparkles.Stop();
    }

    public void StopTrail()
    {
        trailParticles.Stop();
    }
}
