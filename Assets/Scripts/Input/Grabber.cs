using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class to enable an object (ball) to be dragged and thrown
/// in 2D space via touch / mouse.
/// Object follows raycast from touch screen position.
/// </summary>

[RequireComponent(typeof(BallController))]
[RequireComponent(typeof(Rigidbody2D))]

public class Grabber : MonoBehaviour
{
    private bool grabbed = false;     // ie. by raycast
    private Vector2 grabbedPosition;

    private float draggingSpeed = 16f;     // while grabbed

    private float throwBoost = 8f;

    private GameManager.GameState currentGameState;      // state when spawned??

    private LightController lightController;
    private Rigidbody2D ballRigidBody;
    private Projection projection;
    private BallController ballController;


    private void OnEnable()
    {
        GameEvents.OnGameStateChanged += OnGameStateChanged;
        GameEvents.OnSimTimerExpired += OnSimTimerExpired;
    }

    private void OnDisable()
    {
        GameEvents.OnGameStateChanged -= OnGameStateChanged;
        GameEvents.OnSimTimerExpired -= OnSimTimerExpired;
    }

    public void Start()
    {
        ballRigidBody = GetComponent<Rigidbody2D>();
        lightController = GetComponent<LightController>();
        projection = GetComponent<Projection>();
        ballController = GetComponent<BallController>();
    }

    private void FixedUpdate()
    {
        if (currentGameState == GameManager.GameState.GameOver)
            return;

        if (grabbed)
            Catapult(true, 0f);     // physics simulation (ghost object)
    }


    // on touch on (via raycast)
    public void GrabObject(bool stop)
    {
        if (currentGameState != GameManager.GameState.WaitingForPlayer)
            return;

        if (grabbed)
            return;

        grabbed = true;

        if (lightController != null)
        {
            //lightController.LoopPulseSpriteLight();
            lightController.ActivateSpriteLight(true);
            lightController.ActivatePointLight(true);
        }

        if (projection != null)
            projection.ActivateGhost(true);         // ghost point light

        if (stop)
            Stop();

        grabbedPosition = transform.position;
        ballController.Grabbed();

        // fire grabbed event
        GameEvents.OnGrab?.Invoke(projection, grabbedPosition);
    }

    // on touch move
    public void DragGrabbed(Vector2 toPosition)
    {
        if (!grabbed)
            return;

        // fire dragged event
        GameEvents.OnDrag?.Invoke(this);

        // move a step closer to toPosition
        float step = draggingSpeed * Time.deltaTime; // calculate distance to move
        transform.position = Vector2.MoveTowards(transform.position, toPosition, step);
    }

    // on touch off
    public void ThrowGrabbed(Vector2 originPosition, bool simulateOnly, bool catapult, float suspendTime)
    {
        if (!grabbed)
            return;

        // greater force applied with greater distance between transform.position and targetPosition
        Vector2 throwDirection = originPosition - (Vector2)transform.position;        // represents direction and speed
        Vector2 throwForce = throwDirection * throwBoost;

        if (projection != null)
            projection.SimulateThrow(throwForce);        // ghost projection

        if (lightController != null)
            lightController.ActivatePointLight(false);

        if (! simulateOnly)
        {
            grabbed = false;
            ballController.Push(throwForce, transform.position, originPosition);

            if (projection != null)
                projection.ActivateGhost(false);

            ballController.Thrown(suspendTime);    // status, particles, audio

            GameEvents.OnThrow?.Invoke(this, throwForce, catapult);
        }
    }

    public void Catapult(bool simulateOnly, float suspendTime)
    {
        ThrowGrabbed(grabbedPosition, simulateOnly, true, suspendTime);
    }

    // also fired if dragged ball hits another ball
    private void OnSimTimerExpired(float suspendTime)
    {
        Catapult(false, suspendTime);
    }

    private void Stop()
    {
        ballRigidBody.velocity = Vector2.zero;
        ballRigidBody.angularVelocity = 0f;
    }

    private void OnGameStateChanged(GameManager.GameState fromState, GameManager.GameState gameState)
    {
        currentGameState = gameState;
    }
}
