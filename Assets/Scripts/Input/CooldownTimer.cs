using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// cooldown timer after player's turn (grab or ripple)
public class CooldownTimer : MonoBehaviour
{
    //[SerializeField]
    private int cooldownTimeout = 1; 
    //private float simCooldownTime = 0f;     // to match throw or ripple simulation time

    private int timeLeft = 0;

    private Coroutine timerCoroutine;

    private void OnEnable()
    {
        //GameEvents.OnGrabTimerStart += OnGrabTimerStart;
        //GameEvents.OnSimulateThrow += OnSimulateThrow;
        //GameEvents.OnSimulateRipple += OnSimulateRipple;

        GameEvents.OnGameStateChanged += OnGameStateChanged;
    }

    private void OnDisable()
    {
        //GameEvents.OnGrabTimerStart -= OnGrabTimerStart;
        //GameEvents.OnSimulateThrow -= OnSimulateThrow;
        //GameEvents.OnSimulateRipple -= OnSimulateRipple;

        GameEvents.OnGameStateChanged -= OnGameStateChanged;
    }


    //private void OnGrabTimerStart(int timeLeft)
    //{
    //    simCooldownTime = timeLeft;
    //}

    //// overrides cooldownTime to match ripple simulation time
    //private void OnSimulateThrow(int simulationFrames)
    //{
    //    simCooldownTime = simulationFrames * Time.fixedDeltaTime;      // frames to seconds
    //}

    //// overrides cooldownTime to match throw simulation time
    //private void OnSimulateRipple(int simulationFrames)
    //{
    //    simCooldownTime = simulationFrames * Time.fixedDeltaTime;      // frames to seconds
    //}

    private void OnGameStateChanged(GameManager.GameState fromState, GameManager.GameState gameState)
    {
        if (gameState == GameManager.GameState.WaitingForGame)
            StartTimer(cooldownTimeout);
    }

    private void StartTimer(int time)
    {
        StopTimer();
        if (time > 0)
            timerCoroutine = StartCoroutine(CooldownCountdown(time));
        else
            GameEvents.OnCooldownTimerExpired?.Invoke();
    }

    private void StopTimer()
    {
        if (timerCoroutine != null)
            StopCoroutine(timerCoroutine);

        timerCoroutine = null;
    }

    private IEnumerator CooldownCountdown(int countFrom)
    {
        timeLeft = countFrom;

        //GameEvents.OnCooldownTimerStart?.Invoke(timeLeft);
        GameEvents.OnCooldownCountdown?.Invoke(timeLeft, true);

        while (timeLeft > 0)
        {
            yield return new WaitForSecondsRealtime(1f);
            timeLeft--;
            GameEvents.OnCooldownCountdown?.Invoke(timeLeft, false);
            yield return null;
        }

        timerCoroutine = null;
        GameEvents.OnCooldownTimerExpired?.Invoke();
        yield return null;
    }
}
