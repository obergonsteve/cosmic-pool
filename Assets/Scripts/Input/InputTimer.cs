//using System;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class InputTimer : MonoBehaviour
//{
//    //[SerializeField]
//    //private int touchTimeout = 10;
//    //private int timeLeft = 0;

//    [SerializeField]
//    private int beatCountTimeout = 16;      // number of beats with no player activity
//    private int beatsLeft = 0;

//    private bool timeoutPaused = false;

//    //private Coroutine timerCoroutine;

//    private void OnEnable()
//    {
//        GameEvents.OnGrab += OnGrab;
//        GameEvents.OnRippleSpawned += OnRippleSpawned;
//        GameEvents.OnThrow += OnThrow;
//        GameEvents.OnRippleActivated += OnRippleActivated;

//        //GameEvents.OnTouchTimeout += OnTouchTimeout;        // start timer again
//        GameEvents.OnBeat += OnBeat;   
//        GameEvents.OnGameStart += OnGameStart;
//        GameEvents.OnGameStateChanged += OnGameStateChanged;
//    }

//    private void OnDisable()
//    {
//        GameEvents.OnGrab -= OnGrab;
//        GameEvents.OnRippleSpawned -= OnRippleSpawned;
//        GameEvents.OnThrow -= OnThrow;
//        GameEvents.OnRippleActivated -= OnRippleActivated;

//        //GameEvents.OnTouchTimeout -= OnTouchTimeout;
//        GameEvents.OnBeat -= OnBeat;
//        GameEvents.OnGameStart -= OnGameStart;
//        GameEvents.OnGameStateChanged -= OnGameStateChanged;
//    }

//    private void OnGameStart()
//    {
//        ResetBeatCount();
//    }

//    private void OnBeat(BeatData beatData)
//    {
//        if (timeoutPaused)
//            return;

//        beatsLeft--;

//        if (beatsLeft <= 0)
//        {
//            GameEvents.OnPlayerInactive?.Invoke(beatCountTimeout);
//            ResetBeatCount();
//        }
//    }

//    private void ResetBeatCount()
//    {
//        beatsLeft = beatCountTimeout;
//    }

//    private void OnGrab(Projection grabObject, Vector2 grabPosition)
//    {
//        timeoutPaused = true;
//        ResetBeatCount();
//        //StopTimer();
//    }

//    // restart countdown on ball throw
//    private void OnThrow(Grabber throwObject, Vector3 throwForce, bool catapult)
//    {
//        ResetBeatCount();
//        timeoutPaused = false;

//        //StopTimer();
//        //StartTimer();
//    }

//    private void OnRippleSpawned(Ripple newRipple)
//    {
//        timeoutPaused = true;
//        ResetBeatCount();

//        //StopTimer();
//    }

//    // restart countdown on ripple activated
//    private void OnRippleActivated()
//    {
//        ResetBeatCount();
//        timeoutPaused = false;

//        //StopTimer();
//        //StartTimer();
//    }

//    //private IEnumerator StartCountdown()
//    //{
//    //    timeLeft = touchTimeout;

//    //    while (timeLeft > 0)
//    //    {
//    //        yield return new WaitForSecondsRealtime(1f);
//    //        timeLeft--;
//    //        yield return null;
//    //    }

//    //    GameEvents.OnTouchTimeout?.Invoke();
//    //}

//    // start countdown again
//    //private void OnTouchTimeout()
//    //{
//    //    ResetBeatCount();

//    //    //StopTimer();
//    //    //StartTimer();
//    //}

//    private void OnGameStateChanged(GameManager.GameState fromState, GameManager.GameState gameState)
//    {
//        if (gameState == GameManager.GameState.GameOver)
//        {
//            ResetBeatCount();
//            timeoutPaused = true;
//        }
//    }

//    //private void StartTimer()
//    //{
//    //    timerCoroutine = StartCoroutine(StartCountdown());
//    //}

//    //private void StopTimer()
//    //{
//    //    if (timerCoroutine != null)
//    //    {
//    //        StopCoroutine(timerCoroutine);
//    //        timerCoroutine = null;
//    //    }

//    //    timeLeft = 0;
//    //}
//}
