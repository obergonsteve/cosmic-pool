using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// script for raycating to grab, drag and throw Grabber objects
// handles swipe events (from touch input)

public class Raycaster : MonoBehaviour
{
	[SerializeField]
	private LayerMask raycastGrabLayers;			// for 'activating' objects (balls) to drag / throw on touch 'on'

	private Grabber grabbedObject = null;			// 'Grabber' object that raycast hit and activated

	private Camera mainCam;


	private void Start()
	{
		// get Camera.main once only for efficiency
		mainCam = Camera.main;
	}

	private void OnEnable()
	{
		GameEvents.OnSwipeStart += OnSwipeStart;
		GameEvents.OnSwipeMove += OnSwipeMove;
		GameEvents.OnSwipeEnd += OnSwipeEnd;
	}

    private void OnDisable()
	{
		GameEvents.OnSwipeStart -= OnSwipeStart;
		GameEvents.OnSwipeMove -= OnSwipeMove;
		GameEvents.OnSwipeEnd -= OnSwipeEnd;
	}

    // event handlers for touch on / move / off events

    private void OnSwipeStart(Vector2 startPosition, int touchCount)
	{
		GrabRaycast(startPosition);		// spawn ripple if nothing grabbed
	}

	private void OnSwipeMove(Vector2 movePosition, int touchCount)
	{
		Drag(movePosition);
	}

	private void OnSwipeEnd(Vector2 endPosition, Vector2 direction, float speed, int touchCount)
	{
		Throw(endPosition);
	}


	// raycast for target collider layer from touch screenPosition and Grab component
	// activate the hit Grabber (eg. animation)
	// spawn a ripple of nothing grabbed
	private void GrabRaycast(Vector2 screenPosition)
	{
		// raycast to detect what was touched
		RaycastHit2D hitInfo = DoRaycast(screenPosition, raycastGrabLayers);		// grabbing a ball

		if (hitInfo.transform != null)		// 'grab' the ball that was hit
		{
			//Debug.Log($"ActivateRaycast HIT: {hitInfo.transform.name}");
			grabbedObject = hitInfo.transform.GetComponent<Grabber>();
			if (grabbedObject != null)
			{
				grabbedObject.GrabObject(true);
			}
		}
        else				// spawn a ripple, activated on swipe end
		{
			Vector2 ripplePoint = mainCam.ScreenToWorldPoint(screenPosition);
			GameEvents.OnSpawnRipple?.Invoke(ripplePoint);
		}
    }

	// on touch move event handler
	// drag grabbed ball or ripple spawned on touch on
	private void Drag(Vector2 screenPosition)
	{
		Vector2 dragPoint = mainCam.ScreenToWorldPoint(screenPosition);

		if (grabbedObject == null)   // drag ghost ripple if not grabbed a ball
		{
			GameEvents.OnDragRipple?.Invoke(dragPoint);
		}
		else
		{
			grabbedObject.DragGrabbed(dragPoint);     
		}
	}

	// on touch off event handler
	// throw grabbed ball or activate ripple spawned on touch on
	private void Throw(Vector2 screenPosition)
	{
		Vector2 releasePoint = mainCam.ScreenToWorldPoint(screenPosition);

		if (grabbedObject == null)           // release (activate) ripple if not grabbed a ball
		{
			GameEvents.OnReleaseRipple?.Invoke(releasePoint);
		}
		else
		{
			Catapult();         // 'catapult' back towards grab point
		}

        grabbedObject = null;
	}

	private void Catapult()
	{
		if (grabbedObject == null)
			return;

		// 'catapult' back towards grab point
		grabbedObject.Catapult(false, 0f);
		grabbedObject = null;
	}


	private RaycastHit2D DoRaycast(Vector2 touchPosition, LayerMask layers)
	{
		// raycast to detect what was touched
		Vector2 rayOrigin = mainCam.ScreenToWorldPoint(touchPosition);
		return Physics2D.Raycast(rayOrigin, Vector2.zero, Mathf.Infinity, layers);
	}
}
