﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Handle screen touches
// Mouse clicks emulate touches
// Keeps track of beat ticks for touch timing
public class TouchInput : MonoBehaviour
{
	private Vector2 startPosition;
	private Vector2 moveDirection;
	private Vector2 endPosition;

	private long touchStartTicks;
	private long touchEndTicks;
	private float TickSpeedFactor = 100000f;        // to reduce swipe time

	private int minSwipeTime = 5;                // < is a Tap

	//private Camera mainCam;

	//private void Start()
	//{
	//	// get Camera.main once only for efficiency
	//	mainCam = Camera.main;
	//}

	private void Update()
	{
		// track a single touch
		if (Input.touchCount > 0)
		{
			Touch touch = Input.GetTouch(0);

			// handle finger movements based on TouchPhase
			switch (touch.phase)
			{
				case TouchPhase.Began:
					StartTouch(touch.position, Input.touchCount);
					break;

				case TouchPhase.Moved:
					MoveTouch(touch.position, Input.touchCount);
					break;

				case TouchPhase.Ended:
					EndTouch(touch.position, Input.touchCount);
					break;

				case TouchPhase.Stationary:         // touching but hasn't moved
					break;

				case TouchPhase.Canceled:           // system cancelled touch
					break;
			}
		}

		// emulate touch with left mouse
		else if (Input.GetMouseButtonDown(0))
		{
			StartTouch(Input.mousePosition, 1);
		}
		else if (Input.GetMouseButton(0))
		{
			MoveTouch(Input.mousePosition, 1);
		}
		else if (Input.GetMouseButtonUp(0))
		{
			EndTouch(Input.mousePosition, 1);
		}

		// right mouse == 2 finger swipe
		else if (Input.GetMouseButtonDown(1))
		{
			StartTouch(Input.mousePosition, 2);
		}
		else if (Input.GetMouseButton(1))
		{
			MoveTouch(Input.mousePosition, 2);
		}
		else if (Input.GetMouseButtonUp(1))
		{
			EndTouch(Input.mousePosition, 2);
		}
	}

	private void StartTouch(Vector2 screenPosition, int touchCount)
	{
		startPosition = screenPosition;
		touchStartTicks = DateTime.Now.Ticks;

		GameEvents.OnSwipeStart?.Invoke(startPosition, touchCount);
	}

	private void MoveTouch(Vector2 screenPosition, int touchCount)
	{
		Vector2 movePosition = screenPosition;

		if (movePosition != startPosition)
		{
			GameEvents.OnSwipeMove?.Invoke(movePosition, touchCount);
		}
	}

	private void EndTouch(Vector2 screenPosition, int touchCount)
	{
		endPosition = screenPosition;
		touchEndTicks = DateTime.Now.Ticks;
		moveDirection = (endPosition - startPosition).normalized;

		//Debug.Log($"EndTouch: touchTicks = {touchEndTicks - touchStartTicks}");

		if (touchEndTicks - touchStartTicks < (minSwipeTime * TickSpeedFactor))
		{
			// minimal movement - considered a tap!
			GameEvents.OnTap?.Invoke(endPosition, touchCount);
			GameEvents.OnSwipeEnd?.Invoke(endPosition, Vector2.zero, 0f, touchCount);
		}
		else
		{
			float moveDistance = Vector2.Distance(startPosition, endPosition);
			//float worldMoveDistance = Mathf.Abs(Vector2.Distance(worldStart, worldEnd));

			float swipeTime = (touchEndTicks - touchStartTicks) / TickSpeedFactor;
			float moveSpeed = moveDistance / swipeTime;     // speed = distance / time
															//Debug.Log($"EndTouch direction = {moveDirection} distance = {moveDistance} time = {swipeTime} speed = {moveSpeed} tapCount = {tapCount}");
			//if (moveSpeed > 0f)
			{
				GameEvents.OnSwipeEnd?.Invoke(endPosition, moveDirection, moveSpeed, touchCount);
			}
			//else
			//{
			//	GameEvents.OnTap?.Invoke(endPosition, 0);
			//}
		}
	}
}
