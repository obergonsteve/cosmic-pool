using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabTimer : MonoBehaviour
{
    private Coroutine timerCoroutine;

    private int grabTimeout = 4; 
    private int timeLeft = 0;

    private void OnEnable()
    {
        GameEvents.OnGrab += OnGrab;
        GameEvents.OnThrow += OnThrow;

        GameEvents.OnRippleSpawned += OnRippleSpawned;
        GameEvents.OnRippleActivated += OnRippleActivated;
    }

    private void OnDisable()
    {
        GameEvents.OnGrab -= OnGrab;
        GameEvents.OnThrow -= OnThrow;

        GameEvents.OnRippleSpawned -= OnRippleSpawned;
        GameEvents.OnRippleActivated -= OnRippleActivated;
    }


    private void OnGrab(Projection grabObject, Vector2 grabPosition)
    {
        StartTimer(grabTimeout);
    }

    private void OnThrow(Grabber grabObject, Vector3 throwForce, bool catapult)
    {
        StopTimer();
        GameEvents.OnSimTimerStop?.Invoke(timeLeft);
    }

    private void OnRippleSpawned(Ripple newRipple)
    {
        StartTimer(grabTimeout);
    }

    private void OnRippleActivated()
    {
        StopTimer();
        GameEvents.OnSimTimerStop?.Invoke(timeLeft);
    }

    private void StartTimer(int time)
    {
        timerCoroutine = StartCoroutine(GrabCountdown(time));
    }

    private void StopTimer()
    {
        if (timerCoroutine != null)
            StopCoroutine(timerCoroutine);

        timerCoroutine = null;
    }

    private IEnumerator GrabCountdown(int countFrom)
    {
        timeLeft = countFrom;

        //GameEvents.OnSimTimerStart?.Invoke(timeLeft);
        GameEvents.OnSimTimerCountdown?.Invoke(timeLeft, true);

        while (timeLeft > 0)
        {
            yield return new WaitForSecondsRealtime(1f);
            timeLeft--;
            GameEvents.OnSimTimerCountdown?.Invoke(timeLeft, false);
            yield return null;
        }

        GameEvents.OnSimTimerExpired?.Invoke(0f);
    }
}
