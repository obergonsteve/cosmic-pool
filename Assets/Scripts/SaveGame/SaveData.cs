﻿

// simple structure used for data saved to file
// eg. player stats

[System.Serializable]
public struct SaveData
{
    public int PlayCount;       // no. of times new game started and finished (game over)
    public int HiScore;         // own PB
}