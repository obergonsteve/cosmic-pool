using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;


// class to handle file input/output of SaveData
// responds to events from within game 

public class SaveManager : MonoBehaviour
{
	private const string gameName = "CosmicPool.dat";
	private string FilePath => Application.persistentDataPath + "/" + gameName + ".dat";

    private void OnEnable()
    {
		GameEvents.OnLoadGameData += OnLoadGameData;		// data loaded at start of game
		GameEvents.OnSaveGameData += OnSaveGameData;		// data saved on game over (or after new hi-score)
		GameEvents.OnResetGameData += OnResetGameData;		// data deleted and recreated
	}

	private void OnDisable()
	{
		GameEvents.OnLoadGameData -= OnLoadGameData;
		GameEvents.OnSaveGameData -= OnSaveGameData;
		GameEvents.OnResetGameData -= OnResetGameData;
	}


    private void OnLoadGameData(out SaveData saveData, bool newHiScore)
    {
		LoadDataFromFile(out saveData);

		// fire new hi-score event only after data finished loading from file!
		if (newHiScore)
			GameEvents.OnNewHiScore?.Invoke(saveData.HiScore, false);       // eg. for UI 
	}

    private void OnSaveGameData(SaveData saveData)
    {
		SaveDataToFile(saveData);
    }

	private void OnResetGameData()
	{
		ResetData();
	}


	// simply overwrites existing file, if present
	private void SaveDataToFile(SaveData data)
	{
		BinaryFormatter bf = new BinaryFormatter();
		FileStream stream = File.Create(FilePath);		

		bf.Serialize(stream, data);
		stream.Close();

		Debug.Log("Game data saved!");
	}

	// loads from file into given SaveData struct
	private void LoadDataFromFile(out SaveData data)
	{
		if (File.Exists(FilePath))
        {
			BinaryFormatter bf = new BinaryFormatter();
			FileStream stream = File.Open(FilePath, FileMode.Open);

			data = (SaveData)bf.Deserialize(stream);	// type cast stream 'object' to 'SaveData'
			stream.Close();

			Debug.Log("Saved game data loaded!");
		}
		else		// create new file and save empty data if file not present
		{
			data = new SaveData();
			SaveDataToFile(data);

			Debug.Log("New game data loaded!");
		}
	}

	// deletes and recreates mew data
	private void ResetData()
	{
		if (File.Exists(FilePath))
		{
			File.Delete(FilePath);
		}

		SaveDataToFile(new SaveData());
		Debug.Log("Data reset complete!");
	}
}
